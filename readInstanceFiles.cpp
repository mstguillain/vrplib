/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



#include <iostream>
#include <fstream>	// file I/O
#include <sstream>	// ostringstream
#include <cstdlib>	// rand
#include <ctime>	// time
#include <cmath>	// sqrt
#include <cstring>	// memcpy
#include <limits>   // numeric_limits
#include <iomanip>	// setw, setfill
#include <algorithm>// std::max
#include <limits> 	// numeric_limits
#include <map>	

#include "VRP_instance.h"
#include "tools.h"
#include "json.hpp"

using json = nlohmann::json;
















/* Handles instance files of Cordeau-Laporte in data/vrptw/old 	-	static instances */
VRP_instance& VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old(const char *filename) {
	int i, nRegions, nVeh, Q;
	double x, y;

	std::string line;
	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	instance_file >> i; 						// skip first integer
	instance_file >> nVeh;						// retrieve number of vehicles
	instance_file >> nRegions; nRegions++;		// retrieve instance size: the number of regions (++ for the depot) 

	std::getline(instance_file, line); // skip end of line

	instance_file >> i;			// skip int
	instance_file >> Q;			// retrieve max capacity

	// double veh_velocity = 1.0;
	int horizon = numeric_limits<int>::max();

	VRP_instance* instance = new VRP_instance(nRegions, horizon);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q);
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	// cout << "instance file to vertex : " << depot.toString() << endl;

	// DEPOTS
	instance_file >> depot.id_instance_file;
	instance_file >> x; 							// retrieve x coordinate
	instance_file >> y; 							// retrieve y coordinate
	// instance->setCoordRegion(0, x, y);
	instance->setCoordinatesVertex(depot, x, y);

	instance_file >> depot.getRequest_mutable().add_service_time;	// retrieve service duration
	instance_file >> depot.getRequest_mutable().demand;				// retrieve demand
	instance_file >> i; 							// skip int
	instance_file >> i; 							// skip int
	//instance_file >> i; 						// skip int 	---->    /!\ One int less to skip in the depot line
	instance_file >> depot.getRequest_mutable().e;				// retrieve start TW
	instance_file >> depot.getRequest_mutable().l;				// retrieve end TW

	instance->H = depot.getRequest().l;
	// cout << "   " << depot.id_instance_file << "   " << x << "   " << y << "   " << depot.request_set[0].duration << "   " << depot.request_set[0].demand << "   " << depot.request_set[0].e << "   " << depot.request_set[0].l << endl;

	// REGULAR vertices
	for (int j = 1; j < nRegions; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::REGULAR);
	
		// cout << "instance file to vertex : " << vertex.toString();
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		// instance->setCoordRegion(j, x, y);
		instance->setCoordinatesVertex(vertex, x, y);

		instance_file >> vertex.getRequest_mutable().add_service_time;	// retrieve service duration
		instance_file >> vertex.getRequest_mutable().demand;			// retrieve demand
		instance_file >> i; 							// skip int
		instance_file >> i; 							// skip int
		instance_file >> i; 							// skip int 	
		instance_file >> vertex.getRequest_mutable().e;				// retrieve start TW
		instance_file >> vertex.getRequest_mutable().l;				// retrieve end TW

		// cout << "   " << vertex.id_instance_file << "   " << x << "   " << y << "   " << vertex.requests_ts[0].duration 
		// 		<< "   " << vertex.requests_ts[0].demand << "   " << vertex.requests_ts[0].e 
		// 		<< "   " << vertex.requests_ts[0].l << endl;
	}

	instance_file.close();

	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);

	return *instance;
}



/* Instance files used for the SS-VRPTW-CR, EvoSTOC conference paper */
VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR(const char *filename, double scale) {
	int nRegions, Q, nVeh, nCustomerVertices, nWaitingVertices, horizon, nTimeSlots, orig_horizon, veh_velocity;
	double x, y;
	std::string line, word;

	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	instance_file >> word;	// skip word "nWaitingVertices:"
	instance_file >> nWaitingVertices;
	instance_file >> word;	// skip word "nCustomerVertices:"
	instance_file >> nCustomerVertices;
	nRegions = nCustomerVertices + nWaitingVertices + 1;	// +1 for the depot region
	instance_file >> word;	// skip word "Horizon:"
	instance_file >> orig_horizon; horizon = ceil(orig_horizon / scale);
	instance_file >> word;	// skip word "nTimeSlots:"
	instance_file >> nTimeSlots;	_ASSERT_(nTimeSlots <= horizon, "scale too small for the number of time slots !");
	instance_file >> word; // skip word "nVehicles:"
	instance_file >> nVeh;
	instance_file >> word; // skip word "vehicleCapacity:"
	instance_file >> Q;
	instance_file >> word; // skip word "vehicleVelocity:"
	instance_file >> veh_velocity;		veh_velocity *= scale;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line "Arguments: ..."
	std::getline(instance_file, line); // skip line	"TS: ..."
	std::getline(instance_file, line); // skip line "Time: ..."
	std::getline(instance_file, line); // skip line

	// Q = 0;
	// nVeh = 1;
	// cout << filename << endl;
	VRP_instance* instance = new VRP_instance(nRegions, horizon, nTimeSlots, orig_horizon, scale);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q, veh_velocity);
	// instance->_vehVelocity = veh_velocity;


	/* DEPOT and WAITING VERTICES */
	// depot
	std::getline(instance_file, line); // skip line
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	instance_file >> depot.id_instance_file;
	instance_file >> x; 							// retrieve x coordinate
	instance_file >> y; 							// retrieve y coordinate
	// instance->setCoordRegion(0, x, y);
	instance->setCoordinatesVertex(depot, x, y);
	depot.getRequest_mutable().l = horizon;
	std::getline(instance_file, line); // skip end of line
	// waitings	
	for (int j = 1; j <= nWaitingVertices; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::WAITING);
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		// instance->setCoordRegion(j, x, y);
		instance->setCoordinatesVertex(vertex, x, y);
		std::getline(instance_file, line); // skip end of line
	}


	
	/* CUSTOMER REGIONS */
	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line
	int nReq = 0;
	for (int j = nWaitingVertices+1; j <= nWaitingVertices+nCustomerVertices; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::RANDOM_CUSTOMER);
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		// instance->setCoordRegion(j, x, y);
		instance->setCoordinatesVertex(vertex, x, y);

		for (int ts=1; ts<=nTimeSlots; ts++) {
			instance_file >> vertex.getRequest_mutable(ts).p;
			if (vertex.getRequest(ts).p > 0) nReq++;
		}

		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line


	/* REQUESTS ATTRIBUTES */
	instance_file >> word;	// skip word "nRequests"
	int nRequests;
	instance_file >> nRequests;
	_ASSERT_(nRequests == nReq, "");
	instance->nPotentialRequests = nRequests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i=0; i<nReq; i++) {
		int req_id, region, ts;
		instance_file >> req_id;
		instance_file >> region;
		instance_file >> ts;					_ASSERT_(ts <= nTimeSlots, "ts=" << ts);
		VRP_request& req = instance->getVertexFromRegion_mutable(VRP_vertex::RANDOM_CUSTOMER, region)->getRequest_mutable(ts);
		req.id_instance_file = req_id;
		req.id = i+1;							_ASSERT_(req.id <= nRequests, "");
		req.revealTS = ts;
		instance_file >> req.revealTime; 
		req.revealTime_min = req.revealTime;
		req.revealTime_max = req.revealTime;
		instance_file >> req.demand;
		// req.demand = 0;
		instance_file >> req.add_service_time; 
		instance_file >> req.e; 
		instance_file >> req.l; 
		// req.revealTime = instance->ts2time(ts);

		req.revealTime 		= ceil(req.revealTime / scale);
		req.revealTime_min 	= ceil(req.revealTime_min / scale);
		req.revealTime_max 	= max(req.revealTime_min, ceil(req.revealTime_max / scale));
		req.add_service_time= ceil(req.add_service_time / scale);
		req.e 				= ceil(req.e / scale);
		req.l 				= max(req.e, ceil(req.l / scale));
		
		std::getline(instance_file, line); // skip end of line
	}




	instance_file.close();

	instance->setIntegerTravelTimes();
	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);		// integer_travel_times = true ---> ceil each distance to integer

	// cout << "Warning: readInstanceFile_SSVRPTW_CR() has hardcoded modifications" << endl;

	return *instance;
}














/* Instance files used for the DS-VRPTW, from Bent & Van Hentenryck */
// WORK IN PROGRESS !
VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(const char *filename) {

	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	int nCustomerRegions, Q, nVeh, horizon = 240, nTimeSlots = 3;
	double x, y;
	std::string line, word;

	instance_file >> word; 				// skip word "Customers:"
	instance_file >> nCustomerRegions;
	instance_file >> word; 				// skip word "Capacity:"
	instance_file >> Q;
	instance_file >> word; 				// skip word "Time"
	instance_file >> word; 				// skip word "Bins"
	instance_file >> word; 				// skip number
	instance_file >> word; 				// skip word "Vehicles"
	instance_file >> nVeh;
	std::getline(instance_file, line); 	// skip end of line

	std::getline(instance_file, line); 	// skip line
	std::getline(instance_file, line); 	// skip line "Cust   X     Y    [...]"


							// 	(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(1+nCustomerRegions, horizon, nTimeSlots, horizon);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q);


	// DEPOT
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	instance->addVertex(0, VRP_vertex::WAITING, -1);	// also add a waiting vertex for the same region, it could be useful
	instance_file >> word; 								// skip word "D"
	depot.id_instance_file = -1;
	instance_file >> x; 								// retrieve x coordinate
	instance_file >> y; 								// retrieve y coordinate
	// instance->setCoordRegion(0, x, y);
	instance->setCoordinatesVertex(depot, x, y);

	instance_file >> depot.getRequest_mutable().demand;		// retrieve demand
	int e, l;
	instance_file >> e; e++;
	depot.getRequest_mutable().e = e;						// retrieve start TW
	instance_file >> l; l++;
	depot.getRequest_mutable().l = l;						// retrieve end TW
	_ASSERT_(l - e == 240, "");
	instance_file >> depot.getRequest_mutable().add_service_time;	// retrieve service duration
	std::getline(instance_file, line); 								// skip end of line

	// CUSTOMER REGIONS
	for (int i=1; i <= nCustomerRegions; i++) {
		int id;
		instance_file >> id;
		VRP_vertex& vertex = instance->addVertex(i, VRP_vertex::REGULAR_ONLINE, id);
		instance_file >> x; 								// retrieve x coordinate
		instance_file >> y; 								// retrieve y coordinate
		// instance->setCoordRegion(vertex.id_instance_file + 1, x, y);
		instance->setCoordinatesVertex(vertex, x, y);

		instance->addVertex(vertex.id_instance_file + 1, VRP_vertex::WAITING, vertex.id_instance_file);	// also add a waiting vertex for the same region, it could be useful

		int demand, e, l, duration;
		instance_file >> demand;		// retrieve demand
		instance_file >> e; e++;		// retrieve start TW
		instance_file >> l;	l++;		// retrieve end TW
		instance_file >> duration;		// retrieve service duration

		for (int ts = 0; ts <= nTimeSlots; ts++) {
			vertex.getRequest_mutable(ts).demand 			= demand;
			vertex.getRequest_mutable(ts).e 				= e;
			vertex.getRequest_mutable(ts).l 				= l;
			vertex.getRequest_mutable(ts).add_service_time 	= duration;

			vertex.getRequest_mutable(ts).revealTS 		 = ts;
			vertex.getRequest_mutable(ts).revealTime_min = ((ts-1) * 80 + 1 ) * (ts > 0);
			if (ts == 0) vertex.getRequest_mutable(ts).revealTime_max = 0;
			// revealTime_max is defined below, because it depends on the time needed to reach the request location from the depot


			instance_file >> vertex.getRequest_mutable(ts).p;		// retrieve request probability
			vertex.getRequest_mutable(ts).appeared = false;	
		}

		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line

	// KNOWN REQUESTS
	instance_file >> word; 				// skip word "Known"
	instance_file >> word; 				// skip word "Requests"
	int n_deterministic_requests;
	instance_file >> n_deterministic_requests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i = 0; i < n_deterministic_requests; i++) {
		int region; 
		instance_file >> region; region++;
		instance->set_OnlineRequest_asAppeared(region, 0);
		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line


	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);


	for (int i=1; i <= nCustomerRegions; i++) {
		VRP_vertex& depot = *instance->getVertexFromRegion_mutable(VRP_vertex::DEPOT, 0);
		VRP_vertex& vertex = *instance->getVertexFromRegion_mutable(VRP_vertex::REGULAR_ONLINE, i);
		for (int ts = 1; ts <= nTimeSlots; ts++) {
			int latest_dep_time = horizon - veh_type.travelTime(depot, vertex) + vertex.getRequest(ts).add_service_time + veh_type.travelTime(vertex, depot);
			vertex.getRequest_mutable(ts).revealTime_max = min(latest_dep_time, (int) vertex.getRequest_mutable(ts).revealTime_min + 79) ;
		}
	}




	// ONLINE REQUESTS
	instance_file >> word; 				// skip word "Unknown"
	instance_file >> word; 				// skip word "Requests"
	int n_online_requests;
	instance_file >> n_online_requests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i = 0; i < n_online_requests; i++) {
		int region, reveal_time, time_slot;
		instance_file >> region; region++;
		instance_file >> reveal_time; reveal_time++;
		if (reveal_time <= 80) time_slot = 1;
		else if (reveal_time <= 160) time_slot = 2;
		else {_ASSERT_(reveal_time <= 240, ""); time_slot = 3;}
		
		instance->addAppearingOnlineRequest(region, time_slot, reveal_time);
		
		std::getline(instance_file, line); // skip end of line
	}


	instance_file.close();	


	return *instance;
}
























/* Instance files used for the DS-VRPTW, from Rizzo & Saint-Guillain */
VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Rizzo(const char *oc_directory, int scenario_number, bool use_lockers) {
	const int NB_TU_PER_HOUR = 60;

	/************ open all instance files *************/
	
	string oc_dir_path(oc_directory);
	if (oc_dir_path[oc_dir_path.size()-1] == '/') oc_dir_path = oc_dir_path.substr(0, oc_dir_path.size()-1);
	// string dir_name = oc_dir_path.substr(oc_dir_path.rfind('/') + 1);

	std::stringstream ss;
	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-behaviors.txt";
	ss.str(""); 
	if (use_lockers)
		ss << oc_dir_path << "/lockers-yes/behaviors.txt";
	else 
		ss << oc_dir_path << "/lockers-no/behaviors.txt";
	std::ifstream cust_behaviors; cust_behaviors.open(ss.str());
	if (cust_behaviors.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// // ss.str(""); ss << oc_dir_path << "/" << dir_name << "-network-distances.txt";
	ss.str(""); ss << oc_dir_path << "/../distances.txt";
	std::ifstream travel_distances; travel_distances.open(ss.str());
	if (travel_distances.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// // ss.str(""); ss << oc_dir_path << "/" << dir_name << "-network-distances.txt";
	// ss.str(""); ss << oc_dir_path << "/../locations.txt";
	// std::ifstream locations; locations.open(ss.str());
	// if (locations.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-times.txt";
	ss.str(""); ss << oc_dir_path << "/../travel-times.txt";
	std::ifstream travel_times; travel_times.open(ss.str());
	if (travel_times.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }

	ss.str(""); ss << oc_dir_path << "/../service-times.txt";
	std::ifstream service_times; service_times.open(ss.str());
	if (service_times.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }

	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-instances/" << dir_name << "-IN-" << setfill('0') << setw(2) << scenario_number << ".txt";
	ss.str(""); ss << oc_dir_path << "/instances/IN-" << setfill('0') << setw(2) << scenario_number << ".txt";
	std::ifstream scenario_file; scenario_file.open(ss.str());
	if (scenario_file.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }



	/************ retrieve horizon and graph specifications from customers-behaviors.txt *************/
	bool instanceID_in_operational_context[MAX_ID];
	for (int i=0; i<MAX_ID; i++) instanceID_in_operational_context[i] = false;
	int nCustomerRegions, horizon, nTimeSlots;
	std::string line, word;

	cust_behaviors >> word;		// skip word NODES:
	cust_behaviors >> nCustomerRegions;
	std::getline(cust_behaviors, line); // skip end of line
	cust_behaviors >> word;	cust_behaviors >> word;		// skip words TIME HORIZON:
	double horizon_nb_hours, horizon_start_hour; 
	cust_behaviors >> horizon_start_hour;
	cust_behaviors >> horizon_nb_hours;
	std::getline(cust_behaviors, line); // skip end of line
	for (int i=0; i<4; i++) cust_behaviors >> word;		// skip words TIME BUCKETS IN HORIZON:
	cust_behaviors >> nTimeSlots;

	horizon = horizon_nb_hours * NB_TU_PER_HOUR;	// ==> one time unit per minute
	// cout << "Horizon: " << horizon_start_hour << " to " << horizon_start_hour + horizon_nb_hours << "(" << horizon << " TU)" << endl;
									// 	(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(2+nCustomerRegions, horizon, nTimeSlots, horizon);	// ==> 2 depots (otherwise: 1+nCustomerRegions)
	
	/****  vehicles  ****/
	VRP_VehicleType& veh_type_van = instance->addVehicleType(string("van"), 700);
	VRP_VehicleType& veh_type_bike = instance->addVehicleType(string("bike"), 70);
	// cout << instance->toStringVehicleTypes() << endl;

	/**** time slots ****/
	std::vector<int> ts_start(nTimeSlots+1, -1), ts_end(nTimeSlots+1, -1);
	for (int ts=1; ts <= nTimeSlots; ts++) {
		int ts_start_hour, ts_len;
		cust_behaviors >> ts_start_hour;
		ts_start[ts] = (ts_start_hour - horizon_start_hour) * NB_TU_PER_HOUR + 1;
		cust_behaviors >> ts_len;
		ts_end[ts] = ts_start[ts] + NB_TU_PER_HOUR*ts_len -1;
		// cout << "Time Slot: " << ts_start[ts] << " -> " << ts_end[ts] << endl;
		cust_behaviors >> word;		// skip word [09:00-17:00]
	} std::getline(cust_behaviors, line); // skip end of line
	ts_start[0] = 0;
	ts_end[0] = 0;

	/************ network locations & customers behaviors *************/
	// DEPOT
	instance->addVertex(0, VRP_vertex::DEPOT, 1);
	instance->addVertex(1, VRP_vertex::DEPOT, 2);
	// instance->addVertex(0, VRP_vertex::UNLOAD_DEPOT, 1);// add a depot where vehicle can unload during their route, if allowed
	// instance->addVertex(1, VRP_vertex::UNLOAD_DEPOT, 2);//
	instance->addVertex(0, VRP_vertex::WAITING, 1);		// also add a waiting vertex for the same region, it could be useful
	instance->addVertex(1, VRP_vertex::WAITING, 2);		// 
	instanceID_in_operational_context[1] = true;
	instanceID_in_operational_context[2] = true;
	
	for (int i=0; i<4; i++) std::getline(cust_behaviors, line); // skip 4 lines

	for (int region = 2; region <= nCustomerRegions+1; region++) {		// ==> 2 depots (otherwise: (int region = 1; region <= nCustomerRegions; region++) )
		int id_instance_file, demand, e, l; int offline;
		cust_behaviors >> id_instance_file; 
		cust_behaviors >> word; 		// skip TYPE
		cust_behaviors >> demand;
		cust_behaviors >> e; e = (e - horizon_start_hour) * NB_TU_PER_HOUR + 1;
		cust_behaviors >> l; l = e + NB_TU_PER_HOUR*l -1;
		cust_behaviors >> word;		// skip word [09:00-17:00]
		cust_behaviors >> offline;
		VRP_vertex& vertex = instance->addVertex(region, VRP_vertex::REGULAR_ONLINE, id_instance_file);
		instanceID_in_operational_context[id_instance_file] = true;
		instance->addVertex(region, VRP_vertex::WAITING, id_instance_file);	// also add a waiting vertex for the same region, it could be useful
		// std::cout << "Customer vertex: " << region << "(" << id_instance_file << ")  " << demand << "  " << e << "  " << l << "   OFFLINE:" << offline;

		for (int ts = 0; ts <= nTimeSlots; ts++) {		
			VRP_request& req = vertex.getRequest_mutable(ts);
			req.demand 		= demand;
			req.e 			= e;
			req.l 			= l;
			req.add_service_time = 0;
			req.revealTS 		 = ts;
			req.revealTime_min = ts_start[ts];
			req.revealTime_max = ts_end[ts];
			if (ts > 0) {
				cust_behaviors >> req.p; req.p *= 100;		// retrieve request probability (stored in %)
				req.appeared = false;	
				// std::cout << "     " << req.p;
			} else 
				req.appeared = offline;
		}
		// std::cout << endl;


		std::getline(cust_behaviors, line); // skip end of line
	}
	cust_behaviors.close();


	// cout << instance->toStringVehicleTypes() << endl;

	/************ travel times *************/
	int nPairs;
	travel_times >> word;		// skip word PAIRS:
	travel_times >> nPairs;
	std::getline(travel_times, line); // skip end of line
	for (int i=0; i<5; i++) std::getline(travel_times, line); // skip 5 lines
	for (int pair=1; pair <= nPairs; pair++) {
		int id1, id2, travel_time_van_seconds, travel_time_bike_seconds;
		travel_times >> id1;	
		travel_times >> id2;	
		// std::cout << "(" << id1 << "<" << instanceID_in_operational_context[id1] << ">" << "," << id2 << "<" << instanceID_in_operational_context[id2] << ">" << ")";
		if (instanceID_in_operational_context[id1] && instanceID_in_operational_context[id2]) {
			travel_times >> travel_time_van_seconds;
			travel_times >> travel_time_bike_seconds;
			VRP_vertex::VertexType vertex_type_1 = VRP_vertex::REGULAR_ONLINE, vertex_type_2 = VRP_vertex::REGULAR_ONLINE;
			if (id1 <= 2) vertex_type_1 = VRP_vertex::DEPOT;
			if (id2 <= 2) vertex_type_2 = VRP_vertex::DEPOT;
			const VRP_vertex& v1 = * instance->getVertexFromInstanceFileID(vertex_type_1, id1), v2 = * instance->getVertexFromInstanceFileID(vertex_type_2, id2);
			veh_type_van.setTravelTime(v1.getRegion(), v2.getRegion(), (double) travel_time_van_seconds / 60.0);
			veh_type_bike.setTravelTime(v1.getRegion(), v2.getRegion(), (double) travel_time_bike_seconds / 60.0);
			// std::cout << "Travel time between vertex " << v1.toString() << " and " << v2.toString() << ": " << (double) travel_time_van_seconds / 60.0 << " minutes." << endl;
		}
		std::getline(travel_times, line); // skip end of line
	}
	travel_times.close();


	/************ travel distances *************/
	travel_distances >> word;		// skip word PAIRS:
	travel_distances >> nPairs;
	std::getline(travel_distances, line); // skip end of line
	for (int i=0; i<3; i++) std::getline(travel_distances, line); // skip 3 lines
	for (int pair=1; pair <= nPairs; pair++) {
		int id1, id2, travel_dist_van_meters, travel_dist_bike_meters;
		travel_distances >> id1;	
		travel_distances >> id2;	
		// std::cout << "(" << id1 << "<" << instanceID_in_operational_context[id1] << ">" << "," << id2 << "<" << instanceID_in_operational_context[id2] << ">" << ")";
		if (instanceID_in_operational_context[id1] && instanceID_in_operational_context[id2]) {
			travel_distances >> travel_dist_van_meters;
			// travel_distances >> travel_dist_bike_meters;
			travel_dist_bike_meters = travel_dist_van_meters;
			VRP_vertex::VertexType vertex_type_1 = VRP_vertex::REGULAR_ONLINE, vertex_type_2 = VRP_vertex::REGULAR_ONLINE;
			if (id1 <= 2) vertex_type_1 = VRP_vertex::DEPOT;
			if (id2 <= 2) vertex_type_2 = VRP_vertex::DEPOT;
			const VRP_vertex& v1 = * instance->getVertexFromInstanceFileID(vertex_type_1, id1), v2 = * instance->getVertexFromInstanceFileID(vertex_type_2, id2);
			veh_type_van.setTravelDistance(v1.getRegion(), v2.getRegion(), (double) travel_dist_van_meters / 1000);
			veh_type_bike.setTravelDistance(v1.getRegion(), v2.getRegion(), (double) travel_dist_bike_meters / 1000);
			// std::cout << "Travel time between vertex " << v1.toString() << " and " << v2.toString() << ": " << (double) travel_time_van_seconds / 60.0 << " minutes." << endl;
		}
		std::getline(travel_distances, line); // skip end of line
	}
	travel_distances.close();



	/************ service times *************/
	int nNodes;
	service_times >> word;		// skip word NODES:
	service_times >> nNodes;
	std::getline(service_times, line); // skip end of line
	for (int i=0; i<5; i++) std::getline(service_times, line); // skip 5 lines
	for (int node=1; node <= nNodes; node++) {
		int id, service_time_van_seconds, service_time_bike_seconds;
		service_times >> id;	
		// std::cout << id << "<" << instanceID_in_operational_context[id] << ">";
		if (instanceID_in_operational_context[id]) {
			service_times >> word;		// skip TYPE
			service_times >> service_time_van_seconds;
			service_times >> service_time_bike_seconds;
			VRP_vertex::VertexType vertex_type = VRP_vertex::REGULAR_ONLINE;
			if (id <= 2) vertex_type = VRP_vertex::DEPOT;
			const VRP_vertex& v = * instance->getVertexFromInstanceFileID(vertex_type, id);
			veh_type_van.setServiceTime(v.getRegion(), (double) service_time_van_seconds / 60.0);
			veh_type_bike.setServiceTime(v.getRegion(), (double) service_time_bike_seconds / 60.0);
			// std::cout << "Service time at " << v.toString() << ": " << (double) service_time_van_seconds / 60.0 << "(van):" << (double) service_time_bike_seconds / 60.0 << "(bike) minutes." << endl;
		}
		std::getline(service_times, line); // skip end of line
	}
	service_times.close();

	// cout << instance->toStringVehicleTypes() << endl;

	/************ scenario instance *************/
	int n_requests;
	scenario_file >> word; scenario_file >> word;		// skip words ONLINE REQUESTS:
	scenario_file >> n_requests;
	std::getline(scenario_file, line); // ski end of line
	for (int i=0; i<6; i++) std::getline(scenario_file, line); // skip 6 lines
	for (int i = 0; i < n_requests; i++) {
		int id_instance_file, ts;
		std::string reveal_time_str;
		scenario_file >> id_instance_file;	
		scenario_file >> word; 	// skip demand
		scenario_file >> word; scenario_file >> word; scenario_file >> word;  // skip TW
		scenario_file >> reveal_time_str; 
		scenario_file >> ts;

		int reveal_time = (stoi(split<string>(reveal_time_str, ':')[0]) - horizon_start_hour)*NB_TU_PER_HOUR + stoi(split<string>(reveal_time_str, ':')[1]) + 1;		
		int region = instance->getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, id_instance_file)->getRegion();
		instance->addAppearingOnlineRequest(region, ts, reveal_time);
		// cout << "Online req: " << id_instance_file << " internal_id: " << instance->getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, id_instance_file)->getRegion() << "   revealTime=" << reveal_time << "   (ts " << ts << ")" << endl;

		std::getline(scenario_file, line); // skip end of line
	}
	scenario_file.close();	

	// cout << instance->toStringVehicleTypes() << endl;

	return *instance;
}








VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, json j_scenario, int vehicle_capacity) {
	VRP_instance& instance = readInstanceFile_DS_VRPTW__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity);

	for (auto j_req : j_scenario["Requests"]) {
		const VRP_vertex& vertex = *instance.getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, (int) j_req["Node"]);
		if ((int) j_req["TimeSlot"] <= 0) {   // if OFFLINE request
			instance.set_OnlineRequest_asAppeared(vertex.getRequest(0));
		} else {
			// cout << "    r:" << vertex.getRegion() << "      ts:" << (int) j_req["TimeSlot"] << "    rt:" << (int) j_req["RevealTime"] + 1 << endl;	
			instance.addAppearingOnlineRequest(vertex.getRegion(), (int) j_req["TimeSlot"], (int) j_req["RevealTime"] );
		}
	}
	return instance;
}



VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity) {
	VRP_instance& instance = readInstanceFile_SSDS_VRPTW__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity, "DS", VRP_vertex::REGULAR_ONLINE, 1.0);

	return instance;
}





/* Instance files used for the SS-VRPTW-CR, JSON format defined in Candaele's master thesis
	- Static distances ! (non time dependent) 
	- Multiple vehicle types (not implemented yet in the solver)

	ID's are NOT reassigned from 0 (first depot).  ===> not compatible with SIP models 
*/
// WORK IN PROGRESS !
VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity, double scale, string type) {	
	VRP_instance& instance = readInstanceFile_SSDS_VRPTW__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity, type, VRP_vertex::RANDOM_CUSTOMER, scale);
	instance.setIntegerTravelTimes();
	return instance;
}

VRP_instance& VRP_instance_file::readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(json j_carrier, json j_graph, json j_customers, json j_scenario, int vehicle_capacity, double scale) {	
	VRP_instance& instance = readInstanceFile_SSDS_VRPTW__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity, "SS-DS", VRP_vertex::REGULAR_ONLINE, scale);
	
	for (auto j_req : j_scenario["Requests"]) {
		const VRP_vertex& vertex = *instance.getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, (int) j_req["Node"]);
		if ((int) j_req["TimeSlot"] <= 0) {   // if OFFLINE request
			instance.set_OnlineRequest_asAppeared(vertex.getRequest(0));
		} else {
			// cout << "    r:" << vertex.getRegion() << "      ts:" << (int) j_req["TimeSlot"] << "    rt:" << (int) j_req["RevealTime"] + 1 << endl;	
			instance.addAppearingOnlineRequest(vertex.getRegion(), (int) j_req["TimeSlot"], ceil((int) j_req["RevealTime"]  / scale) );
		}
	}

	// instance.setIntegerTravelTimes();
	return instance;
}












// WORK IN PROGRESS !
VRP_instance& VRP_instance_file::readInstanceFile_SSDS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity, string type, VRP_vertex::VertexType requestVertexType, double scale) {
	_ASSERT_(scale >= 1, scale);
	if (scale > 1) _ASSERT_(type == "SS" or type == "SS-DS", type);
	
	// _ASSERT_(j_carrier["TimeSlotsNumber"] == j_customers["TimeSlotsNumber"], j_carrier["TimeSlotsNumber"] << " ≠ " << j_customers["TimeSlotsNumber"]);
	_ASSERT_(not j_customers["TimeSlotsNumber"].is_null(), "");

	_ASSERT_(not j_graph["Nodes"].is_null(),"");
	int nNodes = j_graph["Nodes"].size();
	
	int orig_horizon = j_customers["HorizonSize"];
	// int day_duration_hours  = 8;
	// int day_duration_sec	= day_duration_hours * 3600;			// Minutes
	// int nbsec_per_timeunit 	= day_duration_sec / horizon;	

	int nbsec_per_timeunit = (int) j_customers["RealDurationPerTimeUnit"];
	if (j_customers["RealTimeUnit"] == "Minut")
		nbsec_per_timeunit *= 60;
	else if (j_customers["RealTimeUnit"] == "Hour")
		nbsec_per_timeunit *= 3600;
	else _ASSERT_(j_customers["RealTimeUnit"] == "Second", "Not implemented: " << j_customers["RealTimeUnit"]);


	int horizon_scaled = ceil(orig_horizon / scale);
							// 	(int nRegions, int horizon, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	// int nTimeSlots = j_customers["TimeSlotsNumber"];
	int nTimeSlots = j_customers["PotentialRequests"][0]["ArrivalProbability"].size() - ((int) j_customers["TimeSlots"][0] < 0); // if the first time slot is the offline TS, then nTimeSlots must be of -1 (VRP_instance only wants to know the # of ONLINE time slots, excluding the offline one)
	VRP_instance* instance = new VRP_instance(nNodes, horizon_scaled, nTimeSlots, orig_horizon, scale);


	vector<int> instance_regions;
	for (auto it = j_graph["Nodes"].begin(); it != j_graph["Nodes"].end(); ++it)
	{
		int instance_id = stoi(it.key());
		auto node = it.value();

		VRP_vertex *v = nullptr;
		for (string ntype : node["NodeType"]) {
			if (ntype == "Depot")
				v = & instance->addVertex(VRP_vertex::DEPOT, instance_id);
			else if (ntype == "WaitingPoint")
				v = & instance->addVertex(VRP_vertex::WAITING, instance_id);
			else if (ntype == "Customer")
				v = & instance->addVertex(requestVertexType, instance_id);
			else 
				_ASSERT_(false, ntype);
		}
		// cout << v->getType() << " vertex at instance_id= " << instance_id << "  region_id= " << v->getRegion() << endl;
		instance_regions.push_back(v->getIdInstance());
		// _ASSERT_(not node["GpsCoord"].is_null() or not node["MapCoord"].is_null(),"");
		if (not node["MapCoord"].is_null())
			instance->setCoordinatesVertex(*v, node["MapCoord"]["X"], node["MapCoord"]["Y"]);
			// v->setCoordinates(node["MapCoord"]["X"], node["MapCoord"]["Y"]);
		if (not node["GpsCoord"].is_null())
			instance->setCoordinatesVertex(*v, node["GpsCoord"][0], node["GpsCoord"][1]);
			// v->setCoordinates(node["GpsCoord"][0], node["GpsCoord"][1]);
	}

	_ASSERT_((int) j_customers["TimeSlots"][0] < 0, j_customers["TimeSlots"]);

	double tw_doubled = false;
	int i = 1;
	int i_ts = 0;
	for (auto req_info : j_customers["PotentialRequests"]) {
		_ASSERT_(req_info["TimeWindow"]["TWType"] == "absolute", "not implemented yet");
		i_ts = i_ts % (int) j_customers["TimeSlotsNumber"] + 1;
		// cout << req_info << endl;
		int region = req_info["Node"];

		_ASSERT_((int) j_customers["TimeSlots"][0] < 0 && (int) j_customers["TimeSlots"][1] >= 0, "Timeslots: " << j_customers["TimeSlots"]);
		

		bool H_starts_at_zero = (int) j_customers["TimeSlots"][ ((int) j_customers["TimeSlots"][0] < 0) ] == 0;
		int ts = 0;
		for (double prob_ : req_info["ArrivalProbability"]) {
			// _ASSERT_(prob_ == abs(prob_), "not implemented yet")
			// _ASSERT_(prob_ <= 1.0, prob_);
			if (prob_ != floor(prob_))
				_ASSERT_(prob_ <= 1.0, prob_);		// if it is a round number, then it is likely that it is expressed in % ... 
			if (prob_ >= 1.0)
				prob_ /= 100.0;

			double prob = prob_;

			int ts_start = (int) j_customers["TimeSlots"][ts];
			int ts_end = (int) j_customers["TimeSlots"][ts] + orig_horizon / nTimeSlots - 1;

			if (type == "DS") {
				// cout << j_customers["TimeSlots"] << "   ts_start:" << ts_start << "   ts_end:" << ts_end << "  orig_horizon:" << orig_horizon << endl;
				if (ts > 0)
					_ASSERT_(ts_start >= 0 and ts_end < orig_horizon, j_customers["TimeSlots"] << "   ts_start:" << ts_start << "   ts_end:" << ts_end << "  orig_horizon:" << orig_horizon);
				else {
					_ASSERT_(ts_start == -1, ts_start);
					ts_end = -1;
				}
			}

			if (prob > 0 or (type == "SS-DS" and i_ts == ts)) {
				// cout << "Potential req at node " << req_info["Node"] << " during TS " << ts << endl;
				VRP_request& req = instance->getVertexFromInstanceRegion_mutable(requestVertexType, region)->getRequest_mutable(ts);
				req.id = i;
				_ASSERT_(not req_info["RequestId"].is_null(),"");

				req.p 					= prob;

				req.id_instance_file 	= req_info["RequestId"];
				req.revealTS 			= ts;
				req.e 					= (int) req_info["TimeWindow"]["start"];
				req.l 					= (int) req_info["TimeWindow"]["end"];
				if (j_customers["Name"] == "Police" and type == "SS-DS")
					req.l += 60;
					// req.l = min(orig_horizon-1, (int) req.l + 1000);

				// req.l 					+= req.l - req.e + 1; tw_doubled = true;  					// TW x2 !

				req.revealTime 			= -42;
				
				if (type == "DS" or type == "SS-DS") {
					// req.e += H_starts_at_zero;
					// req.l += H_starts_at_zero;
					// req.revealTime 		= req.e  + H_starts_at_zero;
					// req.revealTime_min 	= req.e  + H_starts_at_zero;
					// req.revealTime_max 	= req.e  + H_starts_at_zero;


					req.revealTime_min 	= ts_start - (not H_starts_at_zero);
					req.revealTime_max 	= min(req.l, (double) ts_end - (not H_starts_at_zero));	
					req.revealTime 		= ceil(req.revealTime_min + (req.revealTime_max - req.revealTime_min) / 2.0);
					// req.revealTime 		= req.revealTime_min;// +1;
					// req.revealTime 		= req.revealTime_max;
				}
				if (type == "SS") {
					req.e += H_starts_at_zero;
					req.l += H_starts_at_zero;
					req.revealTime 		= req.e  + H_starts_at_zero;
					req.revealTime_min 	= req.e  + H_starts_at_zero;
					req.revealTime_max 	= req.e  + H_starts_at_zero;
				}
				
				req.demand 				= req_info["Demand"];			if (vehicle_capacity == 0) req.demand = 0;
				req.add_service_time 	= req_info["ServiceDuration"];
				if (j_customers["Name"] == "Police" and type == "SS-DS")
					req.add_service_time = floor(120 / scale);
				// req.add_service_time = 0;

				// req.l = req.e + 20 -1;
				// cout << "Adding request " << req.toString(true) << endl;
				if (type == "DS" or type == "SS-DS") 
					req.appeared = false;

				if (scale > 1) {
					req.revealTime			= ceil(req.revealTime / scale);
					req.revealTime_min 		= ceil(req.revealTime_min / scale);
					req.revealTime_max 		= max(req.revealTime_min, ceil(req.revealTime_max / scale));
					req.add_service_time 	= ceil(req.add_service_time / scale);
					req.e 					= ceil(req.e / scale);
					req.l 					= max(req.e, floor(req.l / scale));
				}
			}
			ts++;
		} //_ASSERT_(ts == nTimeSlots+1, ts << " ≠ " << nTimeSlots+1);
		
		i++;
	}
	instance->nPotentialRequests = i-1;

	if (vehicle_capacity == 0) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSDS_VRPTW__Candaele :: demands hardcoded to zero !!" << outputMisc::resetColor() << endl << endl;
	if (tw_doubled) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSDS_VRPTW__Candaele :: TW doubled !!" << outputMisc::resetColor() << endl;

	if (not j_carrier["Vehicles"].is_null())
		instance->nVehicles = j_carrier["Vehicles"].size();

	// int nVertices = 255;
	for (auto veh_type_json : j_carrier["VehicleTypes"]) {		// for each vehicle type
		int capacity = veh_type_json["Capacity"];
		if (vehicle_capacity >= 0)
			capacity = vehicle_capacity;
		VRP_VehicleType& veh_type = instance->addVehicleType(veh_type_json["VehicleType"], capacity);
		// cout << "Added vehicle: " << veh_type.getName() << " of capacity: " << veh_type.getCapacity() << endl;

		if (j_carrier["FileType"].is_null()) {		// NOT for BENT (wrong data in Carrier file)
			
			_ASSERT_(not j_carrier["Unit"].is_null(), "");
			_ASSERT_(j_carrier["Unit"] == "Second", j_carrier["Unit"]);
			_ASSERT_(not j_carrier["Data"].is_null(), "Carrier JSON file must at least provide travel times !");
			bool time_dependent = not j_carrier["TimeDependent"].is_null() and j_carrier["TimeDependent"];
			if (time_dependent) 
				cout << "TIME-DEPENDENT" << endl;		
			for (auto veh_data_json : j_carrier["Data"]) {	// iterate over the vehicles / time slots
				_ASSERT_(not veh_data_json["Vehicle"].is_null(), "");
				string s_veh_type = veh_data_json["Vehicle"];
				// cout << s_veh_type << endl;
				if (s_veh_type == veh_type.getName()) {
					// cout << "\tTravel times of vehicle: " << veh_type.getName() << " for time slots: " << veh_data_json["TimeSlot"] << endl;
					
					// the list of time slots for which the travel times apply is in 	veh_data_json["TimeSlot"]
					// _ASSERT_((int) veh_data_json["TimeSlot"].size() == nTimeSlots, "Time dependent matrices are not implemented yet !");
					// _ASSERT_((int) veh_data_json["VehTravelTimes"].size() == nVertices, veh_data_json["VehTravelTimes"].size() << " ≠ " << nVertices);
					// _ASSERT_((int) veh_data_json["VehTravelTimes"][0].size() == nVertices, veh_data_json["VehTravelTimes"][0].size() << " ≠ " << nVertices);
					


					// Retrieve TRAVEL TIMES
					_ASSERT_(not veh_data_json["VehTravelTimes"].is_null(), "");
					for (int ir1 : instance_regions) {
						int r1 = instance->instanceRegionIDtoRegion(ir1);
						for (int ir2 : instance_regions) {
							int r2 = instance->instanceRegionIDtoRegion(ir2);
							
							double t = (double) veh_data_json["VehTravelTimes"][ir1][ir2] / nbsec_per_timeunit * scale;
							
							// if (ir1 == 7 and ir2 == 99)	cout << "\tTravel times of vehicle: " << veh_type.getName() << " for time slots: " << veh_data_json["FromTimeSlot"] << "  between " << r1 << "(" << ir1 << ")" << " -> " << r2 << "(" << ir2 << ")" << " : " << t  << "   (scale= " << scale << ")" << endl;
							
							if (not time_dependent) {
								veh_type.setTravelTime(r1, r2, t);		
								if (veh_data_json["VehDistances"].is_null())
									veh_type.setTravelDistance(r1, r2, t);	
							}
							else {
								int fromTU = ((int) veh_data_json["FromTimeSlot"] - (int) j_customers["HorizonStartsAt"]) / scale;
								int toTU = ((int) veh_data_json["ToTimeSlot"] - (int) j_customers["HorizonStartsAt"]) / scale;

								_ASSERT_(fromTU == floor(fromTU) and toTU == floor(toTU), "");
								
								if (fromTU < horizon_scaled or fromTU > toTU) {
									if (fromTU > toTU)
										fromTU = 0;
									veh_type.setTravelTime(r1, r2, t, fromTU, min(horizon_scaled-1, toTU));	
								}
							}
								
						}
					}

					// Retrieve DISTANCES
					if (not veh_data_json["VehDistances"].is_null()) {
						for (int ir1 : instance_regions) {
							int r1 = instance->instanceRegionIDtoRegion(ir1);
							for (int ir2 : instance_regions) {
								int r2 = instance->instanceRegionIDtoRegion(ir2);
								double d = (double) veh_data_json["VehDistances"][ir1][ir2] / 1000.0;
								if (not time_dependent)
									veh_type.setTravelDistance(r1, r2, d);		
								else{
									int fromTU = ((int) veh_data_json["FromTimeSlot"] - (int) j_customers["HorizonStartsAt"]) / scale;
									int toTU = ((int) veh_data_json["ToTimeSlot"] -  (int) j_customers["HorizonStartsAt"]) / scale;
									
									_ASSERT_(fromTU == floor(fromTU) and toTU == floor(toTU), "");

									if (fromTU < horizon_scaled or fromTU > toTU) {
										if (fromTU > toTU)
											fromTU = 0;
										veh_type.setTravelDistance(r1, r2, d, fromTU, min(horizon_scaled-1, toTU));	
									}
		
								}
							}
						}
					}
					else {
						// cout << "\tNo distances info provided." << endl;

					}

					// Retrieve SERVICE TIMES
					if (! veh_data_json["VehServiceTimes"].is_null()) {
						_ASSERT_(false, "Not implemented yet.");
					}
					else {
						// cout << "\tNo service times info provided." << endl;
					}


				}

				if (time_dependent)
					veh_type.computeAverageDistances();

			}
		}
		else		// FOR BENT only (wrong data in Carrier file)
			veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY, true);				
	}

	return *instance;
}



VRP_instance& VRP_instance_file::readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(string carrier_filename, string graph_filename, string customers_filename, string scenario_filename, int vehicle_capacity, double scale) {
	std::ifstream carrier_file, graph_file, customers_file, scenario_file;
	carrier_file.open(carrier_filename);
	if (carrier_file.fail()) {
		std::cerr << endl << " Error: Unable to open carrier file " << carrier_filename << endl;
		exit (8);
	}
	graph_file.open(graph_filename);
	if (graph_file.fail()) {
		std::cerr << endl << " Error: Unable to open graph file " << graph_filename << endl;
		exit (8);
	}
	customers_file.open(customers_filename);
	if (customers_file.fail()) {
		std::cerr << endl << " Error: Unable to open customers file " << customers_filename << endl;
		exit (8);
	}
	
	scenario_file.open(scenario_filename);
	if (scenario_file.fail()) {
		std::cerr << endl << " Error: Unable to open scenario file " << scenario_filename << endl;
		exit (8);
	}
	
	json j_carrier, j_graph, j_customers, j_scenario;
	carrier_file >> j_carrier;		// read JSON files
	graph_file >> j_graph;		
	customers_file >> j_customers;		
	scenario_file >> j_scenario;
	

	carrier_file.close();	
	graph_file.close();	
	customers_file.close();	
	scenario_file.close();

	return readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(j_carrier, j_graph, j_customers, j_scenario, vehicle_capacity, scale);
}




VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(string carrier_filename, string graph_filename, string customers_filename, int vehicle_capacity, double scale, string type) {
	std::ifstream carrier_file, graph_file, customers_file;
	carrier_file.open(carrier_filename);
	if (carrier_file.fail()) {
		std::cerr << endl << " Error: Unable to open carrier file " << carrier_filename << endl;
		exit (8);
	}
	graph_file.open(graph_filename);
	if (graph_file.fail()) {
		std::cerr << endl << " Error: Unable to open graph file " << graph_filename << endl;
		exit (8);
	}
	customers_file.open(customers_filename);
	if (customers_file.fail()) {
		std::cerr << endl << " Error: Unable to open customers file " << customers_filename << endl;
		exit (8);
	}
	
	json j_carrier, j_graph, j_customers;
	carrier_file >> j_carrier;		// read JSON files
	graph_file >> j_graph;		
	customers_file >> j_customers;		
	

	carrier_file.close();	
	graph_file.close();	
	customers_file.close();	

	return readInstanceFile_SSVRPTW_CR__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity, scale, type);
}




VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(string carrier_filename, string graph_filename, string customers_filename, int vehicle_capacity) {
	std::ifstream carrier_file, graph_file, customers_file;
	carrier_file.open(carrier_filename);
	if (carrier_file.fail()) {
		std::cerr << endl << " Error: Unable to open carrier file " << carrier_filename << endl;
		exit (8);
	}
	graph_file.open(graph_filename);
	if (graph_file.fail()) {
		std::cerr << endl << " Error: Unable to open graph file " << graph_filename << endl;
		exit (8);
	}
	customers_file.open(customers_filename);
	if (customers_file.fail()) {
		std::cerr << endl << " Error: Unable to open customers file " << customers_filename << endl;
		exit (8);
	}
	
	json j_carrier, j_graph, j_customers;
	carrier_file >> j_carrier;		// read JSON files
	graph_file >> j_graph;		
	customers_file >> j_customers;		
	

	carrier_file.close();	
	graph_file.close();	
	customers_file.close();	

	return readInstanceFile_DS_VRPTW__Candaele(j_carrier, j_graph, j_customers, vehicle_capacity);
}

VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Candaele(string carrier_filename, string graph_filename, string customers_filename, string scenario_filename, int vehicle_capacity) {
	std::ifstream carrier_file, graph_file, customers_file, scenario_file;
	carrier_file.open(carrier_filename);
	if (carrier_file.fail()) {
		std::cerr << endl << " Error: Unable to open carrier file " << carrier_filename << endl;
		exit (8);
	}
	graph_file.open(graph_filename);
	if (graph_file.fail()) {
		std::cerr << endl << " Error: Unable to open graph file " << graph_filename << endl;
		exit (8);
	}
	customers_file.open(customers_filename);
	if (customers_file.fail()) {
		std::cerr << endl << " Error: Unable to open customers file " << customers_filename << endl;
		exit (8);
	}

	scenario_file.open(scenario_filename);
	if (scenario_file.fail()) {
		std::cerr << endl << " Error: Unable to open scenario file " << scenario_filename << endl;
		exit (8);
	}
	
	json j_carrier, j_graph, j_customers, j_scenario;
	carrier_file >> j_carrier;		// read JSON files
	graph_file >> j_graph;		
	customers_file >> j_customers;		
	scenario_file >> j_scenario;
	

	carrier_file.close();	
	graph_file.close();	
	customers_file.close();	
	scenario_file.close();

	return readInstanceFile_DS_VRPTW__Candaele(j_carrier, j_graph, j_customers, j_scenario, vehicle_capacity);
}







/* Instance files used for the SS-VRPTW-CR, journal paper, part 1: models   --- Optimod Lyon instances */
VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(const char *filename_distances, const char *filename_instance, int vehicle_capacity, double scale) {
	int nRegions_total, nRegions, nWaitingVertices, horizon, orig_horizon, nTimeSlots;
	int day_duration_hours;//, waiting_time_step;
	std::string line, word;

	
	

	/* read distance matrix file */
	std::ifstream distances_file;
	distances_file.open(filename_distances);
	if (distances_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename_distances << endl;
		exit (8);
	}

	distances_file >> word; distances_file >> word; distances_file >> word;	// skip words "Number of vertices:"
	distances_file >> nRegions_total;
	std::getline(distances_file, line); // skip end of line
	std::getline(distances_file, line); // skip line "Average travel times ..."

	vector< vector<double> > dist;
	for (int v1=0; v1 < nRegions_total; v1++) 
		dist.push_back(vector<double>(nRegions_total, 0.0));

	for (int i=0; i < nRegions_total; i++) {
		distances_file >> word; distances_file >> word;	// skip words "i :"
		for (int j = 0; j < nRegions_total; j++) {
			distances_file >> dist[i][j];
		}
		std::getline(distances_file, line); // skip end of line
	}

	distances_file.close();





	/* read instance file */
	std::ifstream instance_file;
	instance_file.open(filename_instance);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename_instance << endl;
		exit (8);
	}

	std::getline(instance_file, line);	// skip line "INSTANCE ATTRIBUTES""
	instance_file >> word; 				// skip word "#CustomerRegions:"
	instance_file >> nRegions;
	instance_file >> word; 				// skip word "#WaitingVertices:"
	instance_file >> nWaitingVertices;
	// instance_file >> word; 				// skip word "VehicleCapacity:"
	// instance_file >> Q;
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; 				// skip word "DayDuration:"
	instance_file >> day_duration_hours;
	instance_file >> word; 				// skip word "HorizonSize:"
	instance_file >> orig_horizon;
	instance_file >> word; 				// skip word "#TimeSlots:"
	instance_file >> nTimeSlots;
	std::getline(instance_file, line); // skip end of line

	horizon = ceil(orig_horizon / scale);
	

	int day_duration_sec	= day_duration_hours * 3600;
	int nbsec_per_timeunit 	= day_duration_sec / horizon;	

							// 	(int nRegions, int horizon, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(1+nWaitingVertices+nRegions, horizon, nTimeSlots, orig_horizon, scale);
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), vehicle_capacity);
	bool zero_demands = (vehicle_capacity == 0);

	std::getline(instance_file, line);	
	std::getline(instance_file, line);	// skip line "INSTANCE VERTICES"
	instance_file >> word; 				// skip word "Depot:"
	int depot_instance_id;
	instance_file >> depot_instance_id;
	instance->addVertex(0, VRP_vertex::DEPOT, depot_instance_id);
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; instance_file >> word; 	// skip words "Customer vertices:"
	int * instanceID_to_id = new int[nRegions_total];
	for (int i=nWaitingVertices+1; i <= nWaitingVertices+nRegions; i++) {
		int region_id;
		instance_file >> region_id;
		instance->addVertex(i, VRP_vertex::RANDOM_CUSTOMER, region_id);
		instanceID_to_id[region_id] = i;
	}
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; instance_file >> word; 	// skip words "Waiting vertices:"
	for (int i=1; i <= nWaitingVertices; i++) {
		int region_id;
		instance_file >> region_id;
		instance->addVertex(i, VRP_vertex::WAITING, region_id);
	}
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip empty line
	std::getline(instance_file, line);	// skip line "POTENTIAL REQUESTS"
	int nRequests;
	instance_file >> word; 				// skip word "#PotentialRequests:"
	instance_file >> nRequests;
	instance->nPotentialRequests = nRequests;

	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line);	// skip line "(everything has been ..."
	std::getline(instance_file, line);	// skip line 
	bool tw_doubled = false, tw_x3 = false;
	for (int i=0; i < nRequests; i++) {
		int region, ts, revealTime, prob, duration, demand, e, l;
		instance_file >> region;
		instance_file >> ts;			_ASSERT_(ts <= nTimeSlots, "ts=" << ts);
		instance_file >> revealTime;
		instance_file >> prob;
		instance_file >> duration;
		instance_file >> demand;
		instance_file >> e;
		instance_file >> l;

		VRP_request& req = instance->getVertexFromRegion_mutable(VRP_vertex::RANDOM_CUSTOMER, instanceID_to_id[region])->getRequest_mutable(ts);
		req.id = i+1;
		req.id_instance_file = req.id;
		req.revealTS = ts;
		req.revealTime = revealTime;
		req.revealTime_min = req.revealTime;
		req.revealTime_max = req.revealTime;
		req.p = prob;
		req.demand = demand;			if (zero_demands) req.demand = 0;
		req.add_service_time = duration;
		req.e = e;
		req.l = l;
										// req.l += max(0, l - e); tw_doubled = true; // TW doubled !
										// req.l += 2 * max(0, l - e); tw_x3 = true; // TW tripled !

		req.revealTime		= ceil(req.revealTime / scale);
		req.revealTime_min 	= ceil(req.revealTime_min / scale);
		req.revealTime_max 	= max(req.revealTime_min, ceil(req.revealTime_max / scale));
		req.add_service_time 	= ceil(req.add_service_time / scale);
		req.e 				= ceil(req.e / scale);
		req.l 				= max(req.e, floor(req.l / scale));

		std::getline(instance_file, line); // skip end of line
		// cout << req.toString(true) << endl << flush;
	}
	if (zero_demands) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: demands hardcoded to zero !!" << outputMisc::resetColor() << endl << endl;
	if (tw_doubled) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: all TW doubled !!" << outputMisc::resetColor() << endl << endl;
	if (tw_x3) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: all TW tripled !!" << outputMisc::resetColor() << endl << endl;

	instance_file.close();


	// Set all the travel times between each couple of vertices added to the instance

	instance->setIntegerTravelTimes();

	for (int v1=0; v1 < 1+nWaitingVertices+nRegions; v1++) {
		for (int v2=0; v2 < 1+nWaitingVertices+nRegions; v2++) {
			const VRP_vertex *v_1 = nullptr, *v_2 = nullptr;

			if (v1 == 0) v_1 = instance->getVertexFromRegion(VRP_vertex::DEPOT, v1);
			if (0 < v1 && v1 <= nWaitingVertices) v_1 = instance->getVertexFromRegion(VRP_vertex::WAITING, v1);
			if (nWaitingVertices < v1 && v1 <= nWaitingVertices+nRegions) v_1 = instance->getVertexFromRegion(VRP_vertex::RANDOM_CUSTOMER, v1);


			if (v2 == 0) v_2 = instance->getVertexFromRegion(VRP_vertex::DEPOT, v2);
			if (0 < v2 && v2 <= nWaitingVertices) v_2 = instance->getVertexFromRegion(VRP_vertex::WAITING, v2);
			if (nWaitingVertices < v2 && v2 <= nWaitingVertices+nRegions) v_2 = instance->getVertexFromRegion(VRP_vertex::RANDOM_CUSTOMER, v2);

			_ASSERT_(v_1->getIdInstance() >= 0 && v_1->getIdInstance() <= nRegions_total, "argh");
			veh_type.setTravelDistance(v1, v2, dist[v_1->getIdInstance()][v_2->getIdInstance()]/nbsec_per_timeunit);

		}
	}


	return *instance;
}






























/* SS-VRP-D instance files in benchmarks/ss-vrp-d/discrete distrib/local  */
VRP_instance& VRP_instance_file::readInstanceFile_SSVRP_D__1(const char *filename, int vehicle_capacity) {
	int nCustomers, maxDemand;
	double x, y;
	std::string line, word;

	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line

	instance_file >> word;	// skip word "#Customers:"
	instance_file >> nCustomers;
	instance_file >> word;	// skip word "MaxDemand:"
	instance_file >> maxDemand;
	
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line

	VRP_instance* instance = new VRP_instance(nCustomers+1);

	/* DEPOT and WAITING VERTICES */
	// depot
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	instance_file >> depot.id_instance_file;
	instance_file >> x; 							// retrieve x coordinate
	instance_file >> y; 							// retrieve y coordinate
	// instance->setCoordRegion(0, x, y);
	instance->setCoordinatesVertex(depot, x, y);
	std::getline(instance_file, line); // skip end of line

	
	/* CUSTOMER REGIONS */
	for (int i = 1; i <= nCustomers; i++) {
		VRP_vertex& vertex = instance->addVertex(i, VRP_vertex::RANDOM_DEMAND, i, maxDemand);
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		// instance->setCoordRegion(i, x, y);
		instance->setCoordinatesVertex(vertex, x, y);

		instance_file >> word;							// skip mu
		instance_file >> word;							// skip sigma		

		int sum_p = 0;
		for (int q = 0; q <= maxDemand; q++) {
			int p;
			instance_file >> p;							// retrieve probability
			instance->setDiscreteDemandProb(i, q, (double)p/100);
			sum_p += p;
		} _ASSERT_(sum_p == 100, sum_p);


		std::getline(instance_file, line); // skip end of line
	}
	
	instance_file.close();

	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), vehicle_capacity);
	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);

	return *instance;
}





