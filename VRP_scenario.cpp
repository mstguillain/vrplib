/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_scenario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_scenario.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#include <iostream>
#include <cstdlib>	// rand	
#include <limits>
#include <vector>
#include <algorithm>
#include <random>
// #include <cmath>
#include "VRP_instance.h"
#include "VRP_scenario.h"
#include "VRP_solution.h"
#include "tools.h"

#include "json.hpp"

using namespace std;



// Scenario_SS_DS_VRPTW_CR::Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool) : I(instance) {
Scenario_SS_DS_VRPTW_CR::Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_type vrp_type, int currentTime) : I(instance) {
	ASSERT(instance.getHorizon() < numeric_limits<int>::max(), "horizon size isn't set !");
	sol_type = vrp_type;
	reSampleScenario(currentTime);
	// this->pool_volatile = pool;
}

Scenario_SS_DS_VRPTW_CR::Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, json j_scenario, VRP_type vrp_type, int currentTime) : I(instance) {
	ASSERT(instance.getHorizon() < numeric_limits<int>::max(), "horizon size isn't set !");
	_ASSERT_(currentTime == -1, "not implemented yet");

	sol_type = vrp_type;
	fixed_scenario = true;
	
	sampled_requests.resize(I.getNumberRegions());
	for (auto& vec : sampled_requests)
		vec.assign(I.getHorizon()+1, NULL);

	int nb_TU_per_TS = I.getHorizon() / I.getNumberTimeSlots();


	sampled_requests_sequence.clear();

	for (json j_req : j_scenario) {
		if ((int) j_req["time_unit"] >= I.getHorizon()) continue;

		int region = I.instanceRegionIDtoRegion(j_req["node"]);
		int ts = floor((int) j_req["time_unit"] / nb_TU_per_TS) + 1;
		_ASSERT_(ts < 1000, ts);
		const VRP_request& r = I.getVertexFromRegion(VRP_vertex::RANDOM_CUSTOMER, region)->getRequest(ts);

		sampled_requests[region][(int) j_req["time_unit"]] = &r;

		VRP_instance::RequestAttributes req_attr;
		req_attr.vertex = & r.getVertex();
		req_attr.request = & r;
		req_attr.reveal_time = (int) j_req["time_unit"];
		req_attr.time_slot = ts;
		_ASSERT_(req_attr.reveal_time <= r.l, req_attr.reveal_time << " > " << r.l);
		_ASSERT_(r.l <= 10000, endl << j_req["node"] << " -> " << region << endl 
									<< r.toStringRealVertexNumbers(true) << "   id: " << r.id_instance_file << endl 
									<< j_req << endl 
									<< req_attr.toString() << endl 
									<< ts << " " << req_attr.time_slot << endl);
		sampled_requests_sequence.push_back(req_attr);
	}

	sampled_time = currentTime;
}

Scenario_SS_DS_VRPTW_CR::~Scenario_SS_DS_VRPTW_CR() {
	// if (s_dsvrptw_copy)
	// 	delete s_dsvrptw_copy;
}

void Scenario_SS_DS_VRPTW_CR::updateScenario(double currentTime) {
	_ASSERT_(not fixed_scenario, "");
	if (sol_type == SS_VRPTW_CR) _ASSERT_(false, "not intended to be called with a SS-VRPTW-CR     currentTime=" << currentTime);
	_ASSERT_(false, "not implemented yet");
	UNUSED(currentTime);
}
void Scenario_SS_DS_VRPTW_CR::reSampleScenario(double currentTime) {
	_ASSERT_(not fixed_scenario, "");
	if (sol_type == SS_VRPTW_CR) _ASSERT_(currentTime == -1, "not intended to be called with currentTime ≥ 0, in class Scenario_SS_DS_VRPTW_CR     currentTime=" << currentTime);

	currentTime = floor(currentTime);
	sampled_requests.resize(I.getNumberRegions());
	for (auto& vec : sampled_requests)
		vec.assign(I.getHorizon()+1, NULL);

	const set<VRP_vertex *>* v_set;
	switch(sol_type) {
		case SS_VRPTW_CR:
			v_set = & I.getRandomCustomerVertices(); break;
		case DS_VRPTW:
			v_set = & I.getOnlineVertices(); break;
		default: _ASSERT_(false, "");
	}

	for (int ts = 0; ts <= I.getNumberTimeSlots(); ts++) {
		for (const VRP_vertex* pvertex : *v_set) {
			_ASSERT_(pvertex != nullptr, "");

			const VRP_request& r = pvertex->getRequest(ts);
			_ASSERT_(r.p <= 1.0, "");
			if (r.p <= 0) continue;
			
			// cout << endl << "TS: " << ts << "   vertex=" << *pvertex << " ...";
			if (currentTime >= 0 && I.revealedAtTime(r, currentTime)) continue;
			// cout << " sampling... ";

			if (sol_type == SS_VRPTW_CR)
				ASSERT(r.revealTime >= 0, "argh");
			ASSERT(r.revealTime_min >= 0 && r.revealTime_max <= I.getHorizon(), "r.revealTime_min: " << r.revealTime_min << "    r.revealTime_max: " << r.revealTime_max);
			ASSERT((int)sampled_requests.size() >= pvertex->getRegion(), sampled_requests.size() << "<" << pvertex->getRegion());

			if(rand()%100000 +1 <= r.p * 100000) {
				int rtime = max(r.revealTime_min, currentTime+1) + (rand() % (int)(r.revealTime_max - max(r.revealTime_min, currentTime+1) + 1));
				// _ASSERT_(r.revealTime_min <= rtime <= r.revealTime_max, "");
				sampled_requests[pvertex->getRegion()][rtime] = &r;
				// cout << " Sampled " << r.revealTime_min << " ≤ " << rtime << " ≤ " << r.revealTime_max << " !" << endl;
			}
			// if(rand()%100 +1 <= r.p)
				// sampled_requests[pvertex->getRegion()][r.revealTime] = &r;
		}
	}


	sampled_requests_sequence.clear();

	for (int t = 0; t <= I.getHorizon(); t++) {
		vector<const VRP_request*> requests_shuffled;
		for (const VRP_vertex* pvertex : *v_set)
			if (sampled_requests[pvertex->getRegion()][t]) 
				requests_shuffled.push_back(sampled_requests[pvertex->getRegion()][t]);

		shuffle(begin(requests_shuffled), end(requests_shuffled), default_random_engine{});
		for (const VRP_request* r : requests_shuffled) {
			VRP_instance::RequestAttributes req_attr;
			req_attr.vertex = & r->getVertex();
			req_attr.request = r;
			req_attr.reveal_time = t;
			req_attr.time_slot = r->revealTS;
			sampled_requests_sequence.push_back(req_attr);
		}
			
	}

	sampled_time = currentTime;
}		



bool Scenario_SS_DS_VRPTW_CR::sampled(const VRP_request& r) const {
	return sampled_requests[r.getVertex().getRegion()][r.revealTime];
}

const VRP_request* Scenario_SS_DS_VRPTW_CR::sampled(int region, int time_unit) const {
	ASSERT(time_unit > 0 && time_unit <= I.getHorizon(), "argh");
	return sampled_requests[region][time_unit];
}

SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution(const Solution_SS_VRPTW_CR& solution, RecourseStrategy strategy, bool debug, bool verbose, int scenarioHorizon) {
	switch (strategy) {
		case R_INFTY:
			return evalSolution_R_INFTY_CAPA(solution, false, debug, verbose, scenarioHorizon);
		case R_CAPA:
			return evalSolution_R_INFTY_CAPA(solution, true, debug, verbose, scenarioHorizon);
		case R_CAPA_PLUS:
			return evalSolution_R_CAPA_PLUS(solution, false, debug, verbose, scenarioHorizon);
		case R_BASIC_INFTY:
			return evalSolution_R_BASIC_INFTY_CAPA(solution, false, debug, verbose, scenarioHorizon);
		case R_BASIC_CAPA:
			return evalSolution_R_BASIC_INFTY_CAPA(solution, true, debug, verbose, scenarioHorizon);
		case R_SSVRP_R:
			return evalSolution_R_SSVRP_R(solution, true, debug, verbose, scenarioHorizon);
		case R_SSVRP_R_BASIC:
			return evalSolution_R_SSVRP_R(solution, false, debug, verbose, scenarioHorizon);
		default: _ASSERT_(false, "");
	}
	SimulationResults r;
	return r;
}



SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution(const Solution_DS_VRPTW& solution, RecourseStrategy strategy, bool debug, bool verbose, int scenarioHorizon) {
	switch (strategy) {
		case R_GSA:
			return evalSolution_R_GSA(solution, verbose, scenarioHorizon);
		default: _ASSERT_(false, "");
	}
	SimulationResults r;
	return r;
}

SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution_R_BASIC_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon) const {
	(void) debug;
	_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const set<VRP_request> & requests_ordered = solution.getOrderedRequests();



	if (verbose) cout << "Scenario: evalSolution_R_BASIC_INFTY_CAPA() called" << endl;
	int nb_rejects = 0;

	// double current_time = 1;
	struct VehicleState {
		int id;
		double available_time = 1;
		int current_load = 0;
		const VRP_vertex *current_vertex = NULL;
	};
	vector<VehicleState> vehicles(solution.getNumberRoutes());

	// cout << "Using " << vehicles.size() << " vehicles";

	const VRP_vertex *depot = I.getVertexFromRegion(VRP_vertex::DEPOT, 0);
	int i = 1;
	for (VehicleState & veh : vehicles) {
		veh.id = i++;
		veh.current_vertex = depot;
	}

	int nb_reveals = 0;
	for (const VRP_request& r : requests_ordered) {
		if (sampled(r)) {
			nb_reveals++;
			for (VehicleState & veh : vehicles)
				veh.available_time = max(r.revealTime, veh.available_time);

			if (verbose) cout << endl;
			if (verbose) for (VehicleState & veh : vehicles) cout << "Vehicle " << veh.id << " at vertex " << veh.current_vertex->toString() << " : avail_time = " << veh.available_time << endl;
			if (verbose) cout << endl << "Request " << r.toString() << " appeared ! \t\t";
			// FIND THE CLOSEST VEHICLE TO THE REQUEST THAT IS ABLE TO SATISFY IT
			VehicleState * closest_veh = NULL;
			long min_travelTime = numeric_limits<int>::max();
			int min_current_load = 0;
			for (VehicleState & veh : vehicles) {
				const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(veh.id);

				double travel_time = veh_type.travelTime(*veh.current_vertex, r.getVertex(), veh.available_time, true);
				double end_service_req = max(r.e, veh.available_time + travel_time) + veh_type.totalServiceTime(r);
				double return_to_depot_time = end_service_req + veh_type.travelTime(r.getVertex(), *depot, end_service_req, true);
				ASSERT(veh_type.getCapacity() <= 1000, "argh");
				if (		   veh.available_time + travel_time <= r.l
							&& return_to_depot_time <= I.getHorizon() 
							&& (!capacity || veh.current_load + r.demand <= veh_type.getCapacity()) 
							&& (long)(travel_time * 10000 + veh.current_load) < (long)(min_travelTime * 10000 + min_current_load) ) {
							// && travel_time < min_travelTime) {
					min_travelTime = travel_time;
					min_current_load = veh.current_load;
					closest_veh = &veh;
				}
				if (verbose) cout << "VEH " << veh.id << ": avail=" << veh.available_time << "  travel=" << travel_time << "  return_depot=" << return_to_depot_time << "  ";  
			}
			if (verbose) cout << endl;
			if (closest_veh == NULL) {
				nb_rejects++;
				if (verbose) cout << "REJECTED" << endl;
			}
			else {
				const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(closest_veh->id);
				double tt = veh_type.travelTime(*closest_veh->current_vertex, r.getVertex(), closest_veh->available_time, true);
				if (verbose) cout << "ACCEPTED -> vehicle " << closest_veh->id << " (at travel time " << tt << ").";
				closest_veh->available_time = max(r.e, closest_veh->available_time + tt + veh_type.totalServiceTime(r));
				closest_veh->current_load += r.demand;
				closest_veh->current_vertex = & r.getVertex();
				if (verbose) cout << "  Avail. time: " << closest_veh->available_time << endl;
			}
		}
	}

	for (VehicleState & veh : vehicles) {
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(veh.id);
		veh.available_time += veh_type.travelTime(*veh.current_vertex, *depot, veh.available_time, true);
		// ASSERT(veh.available_time <= I.getHorizon(), "time=" << veh.available_time);
		ASSERT(!capacity || veh.current_load <= veh_type.getCapacity(), "load=" << veh.current_load);
	}

	if (verbose) cout << "# Rejections: " << nb_rejects << " / " << nb_reveals << endl;

	SimulationResults r;
	r.nb_rejected = nb_rejects;
	// r.nb_serviced = n_serviced;
	// r.nb_appeared = n_sampled;
	// r.sum_delay = sum_delay;
	return r;
}

SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution_R_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon) const {
	_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment();    // depends directly on the current first stage solution : [i][j][k] = r ==> request r is the k'th scheduled potential request of the j'th visited waiting vertex of route i
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	if (verbose || debug) out << "Scenario: evalSolution_R_INFTY_CAPA() called" << endl;
	int nb_rejects = 0;
	int n_sampled = 0;
	int n_serviced = 0;
	double sum_delay = 0.0;

	// ITERATING OVER ROUTES
	for (int route=1; route <= solution.getNumberRoutes(); route++) {
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(route);

		if (verbose || debug) out << "<-- ROUTE " << route << " -->" << endl;

		double current_time = 1;
		int current_load = 0;

		// ITERATING OVER PLANNED WAITING VERTICES
		Solution_SS_VRPTW_CR::SolutionElement current_w, prev_w;
		for (int wait_pos=1; wait_pos <= solution.getRouteSize(route)-1; wait_pos++) {
			solution.getSolutionElementAtPosition(&prev_w, route, wait_pos-1);
			solution.getSolutionElementAtPosition(&current_w, route, wait_pos);
			const VRP_vertex& waiting_vertex = solution.getVertexAtElement(current_w);
			ASSERT(waiting_vertex.getType() == VRP_vertex::WAITING, "");

			if (verbose || debug) out << "t=" << current_time;
			double tt = veh_type.travelTime(solution.getVertexAtElement(prev_w), waiting_vertex, current_time, true);
			if (verbose || debug) out << "[" << current_time << "]\ttravel from " << solution.getVertexAtElement(prev_w).toString() << " to " << waiting_vertex.toString() << "(" << tt << "); current_time=" << current_time + tt << endl;
			current_time += tt;
			
			
			ASSERT(current_time <= solution.getArrivalTimeAtElement(current_w), endl << solution.toString(true) << "vertex: " << waiting_vertex.toString() << "  current_time=" << current_time << "   arrivalTime: " << solution.getArrivalTimeAtElement(current_w) << endl << out.str() );
			current_time = max(current_time, solution.getArrivalTimeAtElement(current_w));
			

			// ITERATING OVER POTENTIAL REQUESTS
			for (const VRP_request& r : requests_ordered_per_waiting_locations[route][wait_pos]) {
				if (verbose || debug) out << "t=" << current_time << "\t\twaiting for " << r.toString() << " to reveal (" << r.revealTime << ");";
				
				// if (max(current_time, r.revealTime) <= solution.t_max(r, current_w) && (!capacity || current_load + r.demand <= I.getVehicleCapacity())) 		// if it is satisfiable from now and when revealed
					current_time = max(current_time, r.revealTime); 		// waiting until the requests reveals
					if (verbose || debug) out << "  current_time=" << current_time << endl;

				if (sampled(r)) {											// if the request reveals to be present
					n_sampled++;
					if (verbose || debug) out << "t=" << current_time << "\t\t[sampled]" << endl;
					if (current_time <= solution.t_max(r, current_w) && (!capacity || current_load + r.demand <= veh_type.getCapacity())) {		// if it is satisfeasible
						if (verbose || debug) out << "t=" << current_time << "\t\t\tACCEPTED !  ";
						current_time = max(current_time, solution.t_min(r, current_w));		// wait until t_min and satisfy it
						if (verbose || debug) out << "\t\t\twaiting for t_min; current_time=" << current_time << endl;
						if (verbose || debug) out << "t=" << current_time;


						current_time += veh_type.travelTime(waiting_vertex, r.getVertex(), current_time, true);

						n_serviced++;
						sum_delay += current_time - max(r.e, (double) r.revealTime);
						// cout << "delay: " << current_time - r.e << endl;

						current_time += veh_type.totalServiceTime(r); 
						current_time += veh_type.travelTime(r.getVertex(), waiting_vertex, current_time, true);
						current_load += r.demand;
						if (verbose || debug) out << "\t\t\tsatisfy " << r.toString() << "; current_time=" << current_time << endl;
					}
					else {														// if NOT satisfeasible
						if (verbose || debug) out << "t=" << current_time << "\t\t\tREJECTED !" << endl;
						nb_rejects ++;
					}
				} 

				// pool_volatile->hProbas_Q[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
			}
		}
		ASSERT(current_time <= I.getHorizon(), "current_time=" << current_time << "   horizon=" << I.getHorizon() << endl << out.str());
		if (capacity) ASSERT(current_load <= veh_type.getCapacity(), "current_load=" << current_load << "   capacity=" << veh_type.getCapacity());
	}

	int n_unassigned_sampled = 0;
	for (auto& r : solution.getUnassignedRequests()) {
		if (sampled(r)) {
			if (verbose) out << "unassigned request " << r.toString(true) << " sampled" << endl;
			nb_rejects ++;
			n_unassigned_sampled ++;
		}
	}
	if (verbose) out << "Sampled unassigned requests: " << n_unassigned_sampled << "/" << solution.getUnassignedRequests().size() << endl;

	if (verbose) cout << out.str();

	SimulationResults r;
	r.nb_rejected = nb_rejects;
	r.nb_serviced = n_serviced;
	r.nb_appeared = n_sampled;
	r.sum_delay = sum_delay;
	if (n_serviced > 0)
		r.avg_delay = (double) sum_delay / (double) n_serviced;

	// cout << "Scenario: evalSolution_R_INFTY_CAPA() called : " << r.eval << " \t " << r.avg_delay << endl;

	return r;
}


SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution_R_CAPA_PLUS(const Solution_SS_VRPTW_CR& solution, bool accept_all, bool debug, bool verbose, int scenarioHorizon) const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment();    // depends directly on the current first stage solution : [i][j][k] = r ==> request r is the k'th scheduled potential request of the j'th visited waiting vertex of route i

	if (verbose) cout << "Scenario: evalSolution_R_CAPA_PLUS() called" << endl;
	int nb_rejects = 0;
	int last_request_id = -1;
	UNUSED(last_request_id);

	// ITERATING OVER ROUTES
	for (int route=1; route <= solution.getNumberRoutes(); route++) {
		if (verbose) cout << "<-- ROUTE " << route << " -->" << endl;
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(route);

		double current_time = 1;
		int current_load = 0;


		Solution_SS_VRPTW_CR::SolutionElement current_w, next_w, depot;
		solution.getSolutionElementAtPosition(&depot, route, 0);
		const VRP_vertex* current_vertex = & solution.getVertexAtElement(depot);

		// ITERATING OVER PLANNED WAITING VERTICES
		for (int wait_pos=1; wait_pos <= solution.getRouteSize(route)-1; wait_pos++) {

			solution.getSolutionElementAtPosition(&current_w, route, wait_pos);
			solution.getSolutionElementAtPosition(&next_w, route, wait_pos+1);
			const VRP_vertex& waiting_vertex = solution.getVertexAtElement(current_w);

			if (verbose || debug) out << "t=" << current_time;
			double tt = veh_type.travelTime(*current_vertex, waiting_vertex, current_time, true);
			current_time += tt;
			if (verbose || debug) out << "\ttravel from " << current_vertex->toString() << " to " << waiting_vertex.toString() << "(" << tt << "); current_time=" << current_time << endl;
			
			ASSERT(current_time <= solution.getArrivalTimeAtElement(current_w), "current_time=" << current_time);
			current_time = max(current_time, solution.getArrivalTimeAtElement(current_w));
			
			current_vertex = & waiting_vertex;			

			// ITERATING OVER POTENTIAL REQUESTS
			auto& req_vector = requests_ordered_per_waiting_locations[route][wait_pos];
			for (auto it = req_vector.cbegin(); it != req_vector.cend(); it++) {
				const VRP_request& r = *it;
				
				if (verbose || debug) out << "t=" << current_time << "\t\twaiting for " << r.toString() << " to reveal (at t:" << r.revealTime << ")" << "\tcurrent_vertex=" << current_vertex->toString();
				// if (max(current_time, r.revealTime) <= solution.t_max_Rplus(r, *current_vertex, current_w) && current_load + r.demand <= I.getVehicleCapacity()) { // IF it is satisfiable from now and when sampled
					current_time = max(current_time, r.revealTime); 																								// waiting until the requests reveals
					if (verbose || debug) out << "  current_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;
				// }

				if (sampled(r)) {		
					if (verbose || debug) out << "t=" << current_time << "\t\t[sampled]";																														// IF the request reveals to be present
					if (accept_all or (current_time <= solution.t_max_Rplus(r, *current_vertex, current_w) && current_load + r.demand <= veh_type.getCapacity())) {					// IF it is satisfeasible
						if (verbose || debug) out << "\tACCEPTED !  " << "\tcurrent_vertex=" << current_vertex->toString();	
						current_time = max(current_time, solution.t_min_Rplus(r, *current_vertex, current_w));															// wait until t_min_Rplus 
						if (verbose || debug) out << "\t\t\twaiting for t_min_Rplus; current_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;						
						
						if (verbose || debug) out << "t=" << current_time;
						current_load += r.demand;	
						double tt = veh_type.travelTime(*current_vertex, r.getVertex(), current_time, true);		
						current_time += tt + veh_type.totalServiceTime(r);																		// travel to it and satisfy it
						current_vertex = & r.getVertex();																													// it becomes the current location
						last_request_id = r.id;
						if (verbose || debug) out << "\t\t\tsatisfy " << r.toString() << "\tcurrent_time=" << current_time << "\tcurrent_vertex=" << current_vertex->toString() << endl;
					}			
					else {																																			// ELSE (if not satisfeasible)
						nb_rejects ++;	
						if (verbose || debug) out << "\tREJECTED !" << endl;																																// reject
					}	
				} 

				if ((it+1) != req_vector.cend()) {																			// IF not the last request of this waiting vertex
					if (current_time < (it+1)->revealTime) {																	// IF next request not sampled
						if (verbose || debug) out << "t=" << current_time;														
						double tt = veh_type.travelTime(*current_vertex, waiting_vertex, current_time, true);																									
						if (verbose || debug) out << "[" << current_time << "]\ttravel from " << current_vertex->toString() << " to " << waiting_vertex.toString() << "(" << tt << "); current_time=" << current_time + tt << endl;		// travel back to waiting location
						current_time += tt;				
						current_vertex = & waiting_vertex;	
					}			
				}	

				// if (pool_volatile) {
				// 	pool_volatile->hProbas_Q[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
				// 	if (current_vertex == & waiting_vertex)
				// 		pool_volatile->h_Qplus_w[r.id][current_time][current_load] += 1 / (double) pool_volatile->numberScenarios;
				// 	else
				// 		pool_volatile->h_Qplus[r.id][current_time][current_load][last_request_id] += 1 / (double) pool_volatile->numberScenarios;
				// }
			}
		}
		ASSERT(accept_all or current_time <= I.getHorizon(), "current_time=" << current_time << "   horizon=" << I.getHorizon());
		ASSERT(current_load <= veh_type.getCapacity(), "current_load=" << current_load << "   capacity=" << veh_type.getCapacity());
	}

	int n_unassigned_sampled = 0;
	for (auto& r : solution.getUnassignedRequests()) {
		if (sampled(r)) {
			if (verbose || debug) out << "unassigned request " << r.toString(true) << " sampled" << endl;
			nb_rejects ++;
			n_unassigned_sampled ++;
		}
	}
	if (verbose || debug) out << "Sampled unassigned requests: " << n_unassigned_sampled << "/" << solution.getUnassignedRequests().size() << endl;

	if (verbose) cout << out.str();

	// return nb_rejects;
	SimulationResults r;
	r.nb_rejected = nb_rejects;
	// r.nb_serviced = n_serviced;
	// r.nb_appeared = n_sampled;
	// r.sum_delay = sum_delay;

	return r;
}



const VRP_vertex& Scenario_SS_DS_VRPTW_CR::getVertex_atTime(const Solution_SS_VRPTW_CR& solution, int route, double t) const {
	
	for (int pos=1; pos < solution.getRouteSize(route); pos++) 
		if (solution.getDepartureTimeAtPosition(route, pos-1) <= t and t < solution.getDepartureTimeAtPosition(route, pos)) 
			return solution.getVertexAtPosition(route, pos);
		
	return solution.getVertexAtPosition(route, 0);
}


SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution_R_SSVRP_R(const Solution_SS_VRPTW_CR& solution, bool relocate, bool debug, bool verbose, int scenarioHorizon) const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);

	if (verbose) cout << toString() << endl;

	int H = I.getHorizon();
	int K = solution.getNumberRoutes();
	// int K = 1;

	double* available_time_vehicle = new double[K+1];
	for (int k = 1; k<= K; k++)
		available_time_vehicle[k] = 0.0;


	Solution_SS_VRPTW_CR::SolutionElement depot;
	solution.getSolutionElementAtPosition(&depot, 1, 0);
	const VRP_vertex** position_vehicle = new const VRP_vertex*[K+1];
	for (int k = 1; k<= K; k++)
		position_vehicle[k] = & solution.getVertexAtElement(depot);

	double sum_delay = 0.0;
	for (int tu = 1; tu <= H; tu++) {

		/* fetch the requests revealed at beginning of time unit tu */
		vector<const VRP_request*> revealed_requests;
		for (VRP_instance::RequestAttributes r : getRequestSequence()) {
			if (r.reveal_time == tu)
				revealed_requests.push_back(r.request);
			if (r.reveal_time > tu)
				break;
		}

		for (const VRP_request* r : revealed_requests) {

			if (verbose) cout << "[TU " << tu << "] : \t" << r->toStringRealVertexNumbers() << " appeared! ";

			for (int k = 1; k<= K; k++)
				available_time_vehicle[k] = max((double) tu - 1, available_time_vehicle[k]);

			/* select the vehicle that will handle the request: the one that is first available, and the closest (time) to break ties */
			vector<int> first_available_vehicles;
			argmin(available_time_vehicle, 1, K, &first_available_vehicles);
			double min_tt = numeric_limits<double>::max();
			int closest_veh = -1;
			for (int veh : first_available_vehicles) {
				double tt = solution.getVehicleTypeAtRoute(veh).travelTime(*position_vehicle[veh], r->getVertex(), available_time_vehicle[veh], true);
				// cout << "t of veh " << veh << ": " << t << ";  ";
				if (tt < min_tt) {
					min_tt = tt;
					closest_veh = veh;
				} 
				// cout << endl;
			}
			_ASSERT_(closest_veh > 0, closest_veh);
			double tt = solution.getVehicleTypeAtRoute(closest_veh).travelTime(*position_vehicle[closest_veh], r->getVertex(), available_time_vehicle[closest_veh], true);

			if (verbose) cout << "\t\t -> Vehicle " << closest_veh << endl
				<< "\t curr. pos: " << position_vehicle[closest_veh]->toStringRealVertexNumbers() << "(id:" << position_vehicle[closest_veh]->getRegion() << ")"
				<< "   curr. avail. time: " << available_time_vehicle[closest_veh] 
				<< ", ttime of " << tt;

			available_time_vehicle[closest_veh] += tt;
			double delay = available_time_vehicle[closest_veh] - (tu-1);
			if (verbose) cout << "\t ----  delay: " << delay; 
			sum_delay += delay;

			// int service_duration = 120 / I.getScale();
			// available_time_vehicle[closest_veh] += service_duration;    					// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			available_time_vehicle[closest_veh] += r->add_service_time; 
			position_vehicle[closest_veh] = & r->getVertex();
			

			if (verbose) cout << endl << "\t ==> " 
				<< "  new pos: " << position_vehicle[closest_veh]->toStringRealVertexNumbers() 
				<< "   new avail. time: " << available_time_vehicle[closest_veh] << " (service duration: " << r->add_service_time << ")" << endl << endl ;

		}

		if (relocate) {
			// _ASSERT_(false, "not implemented yet");
			for (int k = 1; k<= K; k++) {
				if (available_time_vehicle[k] <= tu-1) {		// if the vehicle is IDLE
					const VRP_vertex& v = getVertex_atTime(solution, k, tu);
					if (v != *position_vehicle[k]) {
						if (verbose) cout << "[TU " << tu << "] : \t" << "vehicle " << k << " is IDLE -> relocating to " << v.toStringRealVertexNumbers();
						available_time_vehicle[k] = max((double) tu - 1, available_time_vehicle[k]);
						double tt = solution.getVehicleTypeAtRoute(k).travelTime(*position_vehicle[k], v, available_time_vehicle[k], true);
						if (verbose) cout << " \t\t tt: " << tt << endl;
						available_time_vehicle[k] += tt;
						position_vehicle[k] = & v;
						if (verbose) cout << endl << "\t ==> " 
							<< "  new pos: " << position_vehicle[k]->toStringRealVertexNumbers()
							<< "   new avail. time: " << available_time_vehicle[k] << endl << endl ;
					}
				}
			}
		}

	}

	// return nb_rejects;
	SimulationResults r;
	r.nb_rejected = 0;
	r.nb_serviced = getRequestSequence().size();
	r.nb_appeared = getRequestSequence().size();
	r.sum_delay = sum_delay;
	if (getRequestSequence().size() > 0)
		r.avg_delay = (double) sum_delay / (double) getRequestSequence().size();

	return r;
}



Solution_DS_VRPTW* s_dsvrptw_copy = nullptr;


SimulationResults Scenario_SS_DS_VRPTW_CR::evalSolution_R_GSA(const Solution_DS_VRPTW& solution, bool verbose, int scenarioHorizon) {
	// _ASSERT_(solution.getCurrentTime() <= this->sampled_time + 1, "");
	if (s_dsvrptw_copy == nullptr) {
		s_dsvrptw_copy = new Solution_DS_VRPTW(solution);
		// cout << endl << "new called" << endl;
	}
	else
		*s_dsvrptw_copy = solution;

	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet"); UNUSED(scenarioHorizon);
	
	if (verbose) cout << endl << "Scenario: evalSolution_R_GSA() called (current time: " << solution.getCurrentTime() <<")" << endl;
	int nb_rejects = 0, nb_accepts = 0;

	map<int, int> revealTime;
	for (const VRP_instance::RequestAttributes& req_attr : sampled_requests_sequence) {
		revealTime[req_attr.request->id] = req_attr.reveal_time;
		// cout << req_attr.request->toStringRealVertexNumbers() << " : " << req_attr.reveal_time << endl;

		if (s_dsvrptw_copy->getCurrentTime() < req_attr.reveal_time) 
			s_dsvrptw_copy->setCurrentTime(req_attr.reveal_time);

		if (verbose) cout << "Time " << req_attr.reveal_time << ": ";
		bool success = s_dsvrptw_copy->tryInsertOnlineRequest(*req_attr.request, false);
		if (! success) nb_rejects++;
		else nb_accepts++;
		if (verbose) cout << outputMisc::boolColorExpr(success) << req_attr.request->toString() << outputMisc::resetColor() << "  " << endl;
	}


	if (verbose) cout << "Total: " << nb_rejects << " rejects." << endl;


	int total_service_delay = 0.0;
	int number_serviced_requests = 0;
	for (int route_=1; route_ <= s_dsvrptw_copy->getNumberRoutes(); route_++) {		// for each route
		for (int pos=1; pos < s_dsvrptw_copy->getRouteSize(route_); pos++) {		// for each vertex in the route, including the ending depot vertex
			const VRP_request& req = s_dsvrptw_copy->getRequestAtPosition(route_, pos);
			ASSERT(req.getVertex().getType() != VRP_vertex::DEPOT, "");
			if (req.getVertex().getType() != VRP_vertex::WAITING) {
				total_service_delay += max(0.0, s_dsvrptw_copy->getServiceTimeAtPosition(route_, pos) - max(req.e, (double)revealTime[req.id]));
				number_serviced_requests ++;
				// cout << req << " --> req.revealTime = " << req.revealTime << "   :  " << total_service_delay << endl;
			}
		}
	}
	// cout << s_dsvrptw_copy->toStringRealVertexNumbers(true) << endl << "totservicedelay = " << total_service_delay << endl << endl;

	
	SimulationResults r;
	r.nb_rejected = nb_rejects;
	r.nb_serviced = nb_accepts;

	// r.nb_appeared = n_sampled;
	r.sum_delay = total_service_delay;
	// r.sum_delay = s_dsvrptw_copy->getTotalServiceDelay();
	r.avg_delay = 0;
	if (number_serviced_requests > 0)
		r.avg_delay = total_service_delay / number_serviced_requests;
	// r.avg_delay = s_dsvrptw_copy->getAVGserviceDelay();
	// cout << r.avg_delay << "  ";



	return r;
}


std::string& Scenario_SS_DS_VRPTW_CR::toString(bool realVertexNumbers) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);

	out << "Scenario: " << endl;
	// for (int t = 0; t <= I.getHorizon(); t++)
	// 	for (int region = 1; region < I.getNumberRegions(); region++)
	// 		if (sampled_requests[region][t]) 
	// 			out << "\t" << "Time " << setw(4) << t << " (time slot " << setw(3) << sampled_requests[region][t]->revealTS << ") ~> Request at vertex " << sampled_requests[region][t]->getVertex().toString() << "  req. infos: " << sampled_requests[region][t]->toString(true) << "  (appeared flag =" << sampled_requests[region][t]->hasAppeared() << ")" << endl;
	for (const VRP_instance::RequestAttributes& req_attr : sampled_requests_sequence)  {
		string vertex_info = req_attr.vertex->toString(), req_info = req_attr.request->toString(true);
		if (realVertexNumbers) {
			vertex_info = req_attr.vertex->toStringRealVertexNumbers();
			req_info = req_attr.request->toStringRealVertexNumbers(true);
		}

		out << "\t" << "Time " << setw(4) << req_attr.reveal_time << " (time slot " << setw(3) << req_attr.time_slot << ") ~> Request at vertex " << vertex_info << "  req. infos: " << req_info << "  (appeared flag =" << req_attr.request->hasAppeared() << ")" << endl;
	}

	static string str = ""; str = out.str(); return (str);
}

std::string& Scenario_SS_DS_VRPTW_CR::toString_perRegion() const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);

	out << "Scenario: " << endl;
	for (int region = 1; region < I.getNumberRegions(); region++)
		for (int t = 0; t <= I.getHorizon(); t++)
			if (sampled_requests[region][t]) 
				out << "\tRequest at vertex " << sampled_requests[region][t]->getVertex().toString() << " sampled for time " << setw(3) << t << " (time slot " << setw(2) << sampled_requests[region][t]->revealTS << ")  req. infos: " << sampled_requests[region][t]->toString(true) << "  (appeared flag =" << sampled_requests[region][t]->hasAppeared() << ")" << endl;

	static string str = ""; str = out.str(); return (str);
}














Scenario_SS_VRP_CD::Scenario_SS_VRP_CD(const VRP_instance& instance, VRP_type vrp_type, int currentTime) : I(instance) {
	UNUSED(currentTime);
	sol_type = vrp_type;
	sampled_demands = new int[I.getNumberRegions()+1];
	for (int r = 0; r <= I.getNumberRegions(); r++)
		sampled_demands[r] = 0;
	reSampleScenario();
}
Scenario_SS_VRP_CD::~Scenario_SS_VRP_CD() {
}


double Scenario_SS_VRP_CD::evalSolution_Bertsimas_A(const Solution_SS_VRP_CD& solution, bool debug, bool verbose) const {
	if (verbose || debug) cout << "Scenario: evalSolution_Bertsimas_A() called" << endl << endl << toString() << endl;
	
	double current_cost = 0;

	// ITERATING OVER ROUTES
	for (int route=1; route <= solution.getNumberRoutes(); route++) {
		const VRP_VehicleType& veh_type = solution.getVehicleTypeAtRoute(route);

		if (verbose || debug) cout << "<-- ROUTE " << route << ", capacity:" << veh_type.getCapacity() << " -->" << endl;
		int current_load = 0;

		const VRP_vertex& depot = solution.getVertexAtPosition(route, 0);
		const VRP_vertex* prev_v = & depot;
		for (int pos=1; pos < solution.getRouteSize(route) + 1; pos++) {	// +1 includes the return to the depot
			const VRP_vertex& curr_v = solution.getVertexAtPosition(route, pos);
			current_cost += veh_type.travelCost(*prev_v, curr_v);
			if (verbose || debug) cout << "\tMoving from " << prev_v->toString() << " to " << curr_v.toString() << " : \tcurrent_cost = " << current_cost << " \t current_load = " << current_load << endl;
			if (verbose || debug) cout << "\t\t[revealed demand : " << sampled_demands[curr_v.getRegion()] << "]";			
			current_load += sampled_demands[curr_v.getRegion()];
			if (veh_type.getCapacity() < current_load) {
				current_cost += veh_type.travelCost(curr_v, depot) + veh_type.travelCost(depot, curr_v);
				current_load = sampled_demands[curr_v.getRegion()];
				if (verbose || debug) cout << " ==> round trip to the depot; \tcurrent_cost = " << current_cost << " \t current_load = " << current_load;
			}
			if (verbose || debug) cout << endl;
			prev_v = & curr_v;
		}
	}
	return current_cost;
}

	

double Scenario_SS_VRP_CD::evalSolution(const Solution_SS_VRP_CD& solution, RecourseStrategy strategy, bool debug, bool verbose, int scenarioHorizon) {
	UNUSED(scenarioHorizon);
	switch (strategy) {
		case BERTSIMAS_A:
			return evalSolution_Bertsimas_A(solution, debug, verbose);
		default: _ASSERT_(false, "");
	}
	return 0.0;
}

void Scenario_SS_VRP_CD::reSampleScenario() {		// Applies the Alias method for sampling from arbitrary discrete probability distributions
	for (const VRP_vertex* v : I.getRandomDemandVertices()) {
		double x = (rand() % 10000 +1) / 10000.0; 
		double sum = 0.0;
		for (int q = 0; q <= I.getMaxRandomDemand(); q++) {
			if (x <= sum + v->getRequest().getDiscreteDemandProb(q)) {
				sampled_demands[v->getRegion()] = q;
				break;
			}
			sum += v->getRequest().getDiscreteDemandProb(q);
		}
		// cout << "q(" << v->getRegion() << ")=" << sampled_demands[v->getRegion()] << "   ";
	}
	// cout << endl;
}

int Scenario_SS_VRP_CD::getSampledDemandAtRegion(int r) const {
	ASSERT(r >= 0 && r <= I.getNumberRegions(), r);
	return sampled_demands[r];
}



std::string& Scenario_SS_VRP_CD::toString(bool realVertexNumbers) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	_ASSERT_(not realVertexNumbers, "not implemented yet");

	out << "Region:   ";
	for (int region = 1; region < I.getNumberRegions(); region++)
		out << setw(4) << region;
	out << endl;
	out << "Scenario: " << endl;
	for (int region = 1; region < I.getNumberRegions(); region++)
		out << setw(4) << sampled_demands[region];
	out << endl;

	static string str = ""; str = out.str(); return (str);
}












