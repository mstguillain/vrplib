# README #

This is a C++ library for Vehicle Routing Problems, oriented to Dynamic and/or Stochastic VRPs.

Whereas there exist other libraries being definitely more efficient at solving classical deterministic VRPs, 
this one is unique in the fact that it proposes state-of-the-art algorithms for Dynamic and/or Stochastic VRPs.
It allows elegant object oriented representations of VRP solutions and constraints, 
complex evaluation functions Dyanmic and/or Stochastic VRPs, real time and online management, and so on.

In particular, solutions for the DS-VRPTW (see **[1]**), SS-VRP-CD (see **4**) and the SS-VRTPW-CR (see **[2]**, **[3]**) 
can be modeled, manipulated and finally optimized using state-of-the-art methods implemented in the present library. 
Thanks to APIs that translate the library inner representation of (SS/DS-)VRPTW solutions into 2 or 3-index binary IP flow 
formulations, the library can be easily interfaced with modern MIP solvers such as *Gurobi* and *CPLEX*.

Pratical implementations of (SS/DS-)VRPs solvers can be found in separated repositories: 

* [SS-VRPTW-CR](https://bitbucket.org/mstguillain/ss-vrptw-cr): the Static and Stochastic VRPTW with both random Customers and Reveal Times

* [DS-VRPTW](https://bitbucket.org/mstguillain/ds-vrptw): the Dynamic and Stochastic VRPTW

* SS-VRP-CD: the Static and Stochastic VRP with random Customers and Demands; *work in progress*

Check the [**Wiki**](https://bitbucket.org/mstguillain/vrplib/wiki/Home).


---


Copyright 2017 Michael Saint-Guillain.

The library is under GNU Lesser General Public License (LGPL) v3.

**Contact**: Michael Saint-Guillain *<m dot stguillain at famous google mail dot com>*

---


**[1]** Saint-Guillain, M., Deville, Y., Solnon, C., 2015. *A Multistage Stochastic Programming 
Approach to the Dynamic and Stochastic VRPTW.* In: CPAIOR 2015. Springer International Publishing, pp. 357-374.    
[_Download link_](http://becool.info.ucl.ac.be/biblio/multistage-stochastic-programming-approach-dynamic-and-stochastic-vrptw)

**[2]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRP 
with Time Windows and both random Customers and Reveal Times.* In: EvoSTOC 2017. Springer International Publishing, pp. 110-127.   
[_Download link_](http://becool.info.ucl.ac.be/biblio/static-and-stochastic-vrp-time-windows-and-both-random-customers-and-reveal-times)

**[3]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRPTW both random Customers and Reveal Times: algorithms and recourse strategies.*   
_To be published_

**[4]** Bertsimas D.J., 1992. *A vehicle routing problem with stochastic demand*. Operations Research, vol. 40, no 3, p. 574-585.