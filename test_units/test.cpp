#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>
#include <vector>
#include <set>
#include <algorithm> // next_permutation, sort
#include <ctime>	// clock()

using namespace std;

int n_sol = 0;

bool request_lt(const VRP_request& r1, const VRP_request& r2) {
	if (r1 == r2) return false;

	if (r1.revealTime < r2.revealTime)							// reveal time first
		return true;
	if (r1.revealTime == r2.revealTime) {
		if (r1.l < r2.l)										// end of time window second)
			return true;
		if (r1.l == r2.l) {
			ASSERT(r1.getVertex().getRegion() != r2.getVertex().getRegion(), "no tie breaking !");
			return r1.getVertex().getRegion() < r2.getVertex().getRegion();		// region number to break ties
		}
	}

	return false;
}


int main(int argc, char **argv) {
	VRP_request::request_comparison_lt = &request_lt;
	
	if (argc < 4) {
		cout << "Usage: test carrier_file graph_file customers_file" << endl;
		return 1;
	}
	const char* carrier_file = argv[1];
	const char* graph_file = argv[2];
	const char* customers_file = argv[3];
	// const char* scenario_file = argv[4];

	// srand(time(NULL));
	// VRP_request::request_comparison_lt = &request_lt;

	// RecourseStrategy strategy = R_CAPA;

	// VRP_instance * I = nullptr;
	// I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, 0);
	
	// cout << endl << endl << "Instance files: " << carrier_file << " " << graph_file << " " << customers_file << endl;
	
	// cout << I->toString() << endl;
	// // // cout << I->toStringCoords(true) << endl;
	// // cout << I->getVehicleType().toString_TravelTimes() << endl;
	// // cout << I->getVehicleType().toString_Distances() << endl;
	// // cout << I->toStringProbasTS() << endl << endl;
	// // // cout << I->toStringInstanceMetrics() << endl;



	// /* Write a solution to a JSON file */
	// Solution_SS_VRPTW_CR s(*I, strategy, 120);
	// // cout << I->toString_AppearingOnlineRequests() << endl;
	// // cout << I->toStringVehicleTypes() << endl;
	// // cout << I->toStringPotentialOnlineRequests() << endl;
	// s.addRoutes(3);
	// s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	// cout << "Initial solution:" << endl << s.toString(false);
	// // cout << "Initial solution:" << endl << s.toString(true);
	// cout << "Cost:" << s.getCost() << endl;
	// // cout << s.toString(true) << endl;
	// json j = s.json_desc();
	// cout << endl << j.dump(2) << endl;



	// /* Read a solution from a JSON file */
	// Solution_SS_VRPTW_CR s2(*I, strategy, 120);
	// s2.addRoutes(3);
	// s2.generateInitialSolution(true);

	// std::ifstream file;
	// file.open("solution.json");
	// json j2;
	// file >> j2;
	// s2.copyFromJSON(j2);
	// cout << "Solution:" << endl << s2.toString(false);
	// // cout << "Solution:" << endl << s2.toString(true);
	// cout << "Cost:" << s2.getCost() << endl;


	int vehicle_capacity = 0;
	int number_vehicles = 2;
	int waiting_time_multiple = 60;
	RecourseStrategy strategy = R_CAPA;
	int scale = 1;

	VRP_instance& I = VRP_instance_file::readInstanceFile_SSVRPTW_CR__Candaele(carrier_file, graph_file, customers_file, vehicle_capacity);


	
	srand(time(NULL));


	Solution_SS_VRPTW_CR s480(I, strategy);
	s480.addRoutes(number_vehicles);
	s480.generateInitialSolution(true);



	int initialWaitingTime = max(waiting_time_multiple, (int)floor(60 / scale));

	Solution_SS_VRPTW_CR s(I, strategy);
	s.setMinimumWaitingTime(1.0);
	s.setInitialWaitingTime(initialWaitingTime);
	s.setWaitingTimeMultiple(waiting_time_multiple);
	s.addRoutes(number_vehicles);

	// NEIGHBORHOOD MANAGER
	int minIncrement = waiting_time_multiple;
	int maxIncrement;
	if (waiting_time_multiple == 1) 
		maxIncrement = ceil(I.getHorizon() / 5);
	else 
		maxIncrement = I.getHorizon()/5;
	NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I.getHorizon(), minIncrement, maxIncrement);


	// MAIN
	s.generateInitialSolution(true);

	LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
	lsprog.setTimeOut(1);
	lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)



	cout << endl << "Solution (scale " << s.getScale() << "):" << endl ;
	cout << s.toString(false) << s.getCost() <<  endl;




	s480.copyFromSol_scaleWaitingTimes(s);
	cout << "Cost under scale 1 (R_CAPA)" << endl << s480.toString(false) << s480.getCost() <<  endl;

}













