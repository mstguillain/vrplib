/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_instance +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#include <iostream>
#include <vector>
// #include <pair>
#include <set>
#include <cmath>
#include "VRP_instance.h"
#include "VRP_routes.h"
#include "tools.h"

using namespace std;



ostream &operator<<(ostream &os, const VRP_instance &i) { return os << i.toString(); }
ostream &operator<<(ostream &os, const VRP_VehicleType &veht) { return os << veht.toString(); }
ostream &operator<<(ostream &os, const VRP_request &r) { return os << r.toString(); }
ostream &operator<<(ostream &os, const VRP_vertex &v) { return os << v.toString(); }




// VRP_instance::VRP_instance(int nRegions, int nVeh, int Q, double vehVelocity, int H, int nTimeSlots, int orig_horizon, double scale) {
VRP_instance::VRP_instance(int nRegions, int H, int nTimeSlots, int orig_horizon, double scale) {
	ASSERT(nRegions > 0, "argh");	// cout << "Instance " << this << " created    nRegions=" << nRegions << endl;
	this->nRegions 		= nRegions +1;  // +1 because we do not assign any vertex to region 0 (but we keep it, just in case)
	// this->nVeh 			= nVeh;
	// this->Q 			= Q;
	// this->vehVelocity 	= vehVelocity;
	this->H 			= H;
	this->nTimeSlots	= nTimeSlots;
	this->orig_horizon	= orig_horizon;
	this->scale			= scale;

	for (int i=0; i<MAX_ID; i++) id_instance_file_to_region[i] = -42;
	for (int i=0; i<MAX_ID; i++) region_to_id_instance_file[i] = -42;

	// initialize distance related vectors
	coordX.assign(this->nRegions, 0.0);
	coordY.assign(this->nRegions, 0.0);

	for (int v1=0; v1 < this->nRegions; v1++) 
		online_requests_appearing_times.push_back(vector<int>(nTimeSlots+1, numeric_limits<int>::max()));

}

VRP_VehicleType& VRP_instance::addVehicleType(string name, int capacity, double velocity, double costPerDistanceUnit) {
	VRP_VehicleType* new_type = new VRP_VehicleType(nRegions, H, name, capacity, velocity, costPerDistanceUnit);
	addVehicleType(new_type);
	return *new_type;
}


void VRP_instance::addVehicleType(VRP_VehicleType * veh_type) {
	ASSERT(veh_type, "");
	vehicle_types.insert(veh_type);
}


const VRP_VehicleType& VRP_instance::getVehicleType(string name) const {
	for (const VRP_VehicleType* veh_type : getVehicleTypes()) {
		if (veh_type->getName() == name) return *veh_type;		
	}	
	_ASSERT_(false, "Vehicle type \"" << name << "\" does not exists");
}

const VRP_VehicleType& VRP_instance::getVehicleType() const {
	ASSERT(getVehicleTypes().size() == 1, getVehicleTypes().size());
	return ** getVehicleTypes().rbegin();
}

int VRP_instance::getMaxVehicleTypeCapacity() const {
	int max = 0;
	for (VRP_VehicleType* veh_type : getVehicleTypes())
		if (veh_type->getCapacity() > max)
			max = veh_type->getCapacity();
	return max;
}


int VRP_instance::getMaxRandomDemand() const {
	ASSERT(max_random_demand > 0, max_random_demand);
	return max_random_demand;
}


VRP_vertex& VRP_instance::addVertex(VRP_vertex::VertexType type, int instance_id, int max_demand) {
	_ASSERT_(instance_id >= 0, "");
	int region  = id_instance_file_to_region[instance_id];
	if (region == -42)
		region = nbAddedVerticesAtDifferentRegions + 1;		// we never assign the region id 0

	return addVertex(region, type, instance_id, max_demand);
}

VRP_vertex& VRP_instance::addVertex(int region, VRP_vertex::VertexType type, int instance_id, int max_demand) {
	_ASSERT_(type != VRP_vertex::NO_TYPE, "argh");
	_ASSERT_(instance_id < MAX_ID, "");
	_ASSERT_(max_demand <= 0 || type == VRP_vertex::RANDOM_DEMAND, "");
	_ASSERT_(type != VRP_vertex::RANDOM_DEMAND || max_demand > 0, "");

	VRP_vertex* pvertex = new VRP_vertex(region, type, nTimeSlots, instance_id, max_demand);

	addVertex(pvertex);

	if (instance_id >= 0) {
		if(id_instance_file_to_region[instance_id] == -42)
			nbAddedVerticesAtDifferentRegions ++;
		
		id_instance_file_to_region[instance_id] = region;
		if (region_to_id_instance_file[region] == -42)
			region_to_id_instance_file[region] = instance_id;
		else _ASSERT_(region_to_id_instance_file[region] == instance_id, region_to_id_instance_file[region] << " ≠ " << instance_id);
	}

	return *pvertex;
}

void VRP_instance::addVertex(VRP_vertex * pvertex) {
	switch(pvertex->getType()) {
		case VRP_vertex::DEPOT :
			depot_vertices.insert(pvertex);
			break;
		// case VRP_vertex::UNLOAD_DEPOT :
		// 	u_depot_vertices.insert(pvertex);
		// 	break;
		case VRP_vertex::REGULAR :
			reg_vertices.insert(pvertex);
			break;
		case VRP_vertex::RANDOM_CUSTOMER :
			random_cust_vertices.insert(pvertex);
			break;
		case VRP_vertex::RANDOM_DEMAND :
			random_demand_vertices.insert(pvertex);
			if (pvertex->getMaxDemand() > max_random_demand)
				max_random_demand = pvertex->getMaxDemand();
			break;
		case VRP_vertex::WAITING :
			wait_vertices.insert(pvertex);
			break;
		case VRP_vertex::REGULAR_ONLINE :
			reg_online_vertices.insert(pvertex);
			for (int ts = 0; ts <= nTimeSlots; ts++) 
				pvertex->getRequest_mutable(ts).appeared = false;
			break;

		// case VRP_vertex::SCHED_JOB :
		// 	break;


		default :
			_ASSERT_(false, "wrong vertex type " << pvertex->getType());
	}
}


void VRP_instance::setCoordinatesVertex(VRP_vertex& v, double lat, double lon) { 
	v.coordinates[0] = lat; 
	v.coordinates[1] = lon; 
	coordX[v.getRegion()] = lat;
	coordY[v.getRegion()] = lon;
}

int  VRP_instance::getNumberRegions() const {
	return nRegions;
}
int  VRP_instance::_getNumberVehicles() const {
	ASSERT(_nVeh > 0, "");
	return _nVeh;
}
int  VRP_instance::getHorizon() const {
	return H;
}
int  VRP_instance::getNumberTimeSlots() const {
	return nTimeSlots;
}
int  VRP_instance::getNumberPotentialRequests() const {
	return nPotentialRequests;
}
double VRP_instance::getExpectedNumber_appearedRequests(double fromCurrentTime) const {
	double expectedNumber_appearedRequests = 0.0;
	
	for (const VRP_vertex* v : getRandomCustomerVertices()) {
		for (int ts = 1; ts <= nTimeSlots; ts++)
			expectedNumber_appearedRequests += v->getRequest(ts).p;
			// expectedNumber_appearedRequests += v->getRequest(ts).p / 100;
	}
	for (const VRP_vertex* v : getOnlineVertices()) {
		for (int ts = 1; ts <= nTimeSlots; ts++)
			if (fromCurrentTime < v->getRequest(ts).revealTime_max and not v->getRequest(ts).hasAppeared())
				expectedNumber_appearedRequests += v->getRequest(ts).p;
				// expectedNumber_appearedRequests += v->getRequest(ts).p / 100;
	}
	
	return expectedNumber_appearedRequests;
}



int VRP_instance::getNumberVertices(VRP_vertex::VertexType type) const {

	switch(type) {
		case VRP_vertex::DEPOT :
			return depot_vertices.size();
		case VRP_vertex::REGULAR :
			return reg_vertices.size();
		case VRP_vertex::RANDOM_CUSTOMER :
			return random_cust_vertices.size();
		case VRP_vertex::RANDOM_DEMAND :
			return random_demand_vertices.size();
		case VRP_vertex::WAITING :
			return wait_vertices.size();
		default :
			_ASSERT_(false, "wrong vertex type " << type);
	}
	return 0;
}


VRP_vertex* VRP_instance::getVertexFromInstanceRegion_mutable(VRP_vertex::VertexType type, int realRegion) const { 
	return getVertexFromRegion_mutable(type, id_instance_file_to_region[realRegion]);
}


VRP_vertex* VRP_instance::getVertexFromRegion_mutable(VRP_vertex::VertexType type, int region) const { 
	ASSERT(region < nRegions, "region " << region << " does not exist (nRegions=" << nRegions << ") instance:" << this);

	const set<VRP_vertex*> * pset = nullptr;
	switch(type) {
		case VRP_vertex::DEPOT :
			pset = & depot_vertices;
			break;
		// case VRP_vertex::UNLOAD_DEPOT :
		// 	pset = & u_depot_vertices;
		// 	break;
		case VRP_vertex::REGULAR :
			pset = & reg_vertices;
			break;
		case VRP_vertex::RANDOM_CUSTOMER :
			pset = & random_cust_vertices;
			break;
		case VRP_vertex::RANDOM_DEMAND :
			pset = & random_demand_vertices;
			break;
		case VRP_vertex::WAITING :
			pset = & wait_vertices;
			break;
		case VRP_vertex::REGULAR_ONLINE :
			pset = & reg_online_vertices;
			// for (VRP_vertex* v : reg_online_vertices[ts]) {
			// 	// cout << "found a vertex of type " << v->type << " at region " << v->region << endl;
			// 	if (v->region == region)
			// 		return v;
			// }
			// _ASSERT_(false, "vertex not found !");
			break;
		default :
			_ASSERT_(false, "wrong vertex type " << type);
	}

	for (VRP_vertex* v : *pset) {
		// cout << "found a vertex of type " << v->type << " at region " << v->region << endl;
		if (v->region == region)
			return v;
	}

	// for (const VRP_vertex* v : *pset) cout << v->toString() << " ";
	_ASSERT_(false, "vertex from region " << region << " not found ! ");
	return NULL;
}

const VRP_vertex* VRP_instance::getVertexFromRegion(VRP_vertex::VertexType type, int region) const { 
	return getVertexFromRegion_mutable(type, region);
}

const VRP_vertex* VRP_instance::getVertexFromInstanceFileID(VRP_vertex::VertexType type, int instance_id) const {
	// cout << "id_instance_file_to_region[" << instance_id << "] =" << id_instance_file_to_region[instance_id] << endl;
	return getVertexFromRegion(type, id_instance_file_to_region[instance_id]);
}


void VRP_instance::set_OnlineRequest_asAppeared(int region, int timeSlot, int time_unit) {
	ASSERT(0 <= timeSlot && timeSlot <= nTimeSlots, "");		// offline requests, i.e. at TS 0, must also be added using this method by the user
	getVertexFromRegion_mutable(VRP_vertex::REGULAR_ONLINE, region)->getRequest_mutable(timeSlot).setAppeared();
	if (time_unit != -42)
		getVertexFromRegion_mutable(VRP_vertex::REGULAR_ONLINE, region)->getRequest_mutable(timeSlot).revealTime = time_unit;
}
void VRP_instance::set_OnlineRequest_asAppeared(const VRP_request& r, int time_unit) {
	set_OnlineRequest_asAppeared(r.getVertex().getRegion(), r.revealTS, time_unit);
}


// void VRP_instance::clearAppearedOnlineRequests() {
// 	for (VRP_vertex* pvertex : reg_online_vertices)
// 		for (int ts=1; ts < nTimeSlots+1; ts++)
// 			pvertex->getRequest_mutable(ts).appeared = false;
// }


void VRP_instance::addAppearingOnlineRequest(int region, int timeSlot, int revealTime) {
	const VRP_vertex & vertex = * getVertexFromRegion(VRP_vertex::REGULAR_ONLINE, region);
	_ASSERT_(revealTime >= 0 and revealTime < H, "");
	_ASSERT_(vertex.getRequest(timeSlot).revealTime_min <= revealTime and revealTime <= vertex.getRequest(timeSlot).revealTime_max, endl << "revealTime_min=" << vertex.getRequest(timeSlot).revealTime_min << "  revealTime=" << revealTime << "   revealTime_max=" << vertex.getRequest(timeSlot).revealTime_max << endl << "timeSlot=" << timeSlot << endl << vertex.getRequest(timeSlot).toString(true));
	_ASSERT_(region <= nRegions-1, region << " >= " << nRegions);
	_ASSERT_(0 < timeSlot && timeSlot <= nTimeSlots, timeSlot << " > " << nTimeSlots);
	
	bool already_there = false;
	for (auto& req : online_requests_appeared) {
		if (req.vertex->getRegion() == region && req.request->revealTS == timeSlot) {
			cout << "Request at region " << region << " (vertex instance_id: " << region-1 << ") duplicated !! Ignored..." << endl;
			already_there = true;
		}
	}
	_ASSERT_(! already_there, "request already in the set of appeared requests !");

	RequestAttributes req;
	req.vertex = & vertex;
	req.request = & vertex.getRequest(timeSlot);
	req.reveal_time = revealTime;
	req.time_slot = timeSlot;
	online_requests_appeared.push_back(req);

	_ASSERT_(region < (int) online_requests_appearing_times.size() && timeSlot < (int) online_requests_appearing_times[0].size(), "size: " << online_requests_appearing_times.size() << " x " << online_requests_appearing_times[0].size());
	online_requests_appearing_times[region][timeSlot] = revealTime;
}

bool VRP_instance::revealedAtTime(const VRP_request& r, int time) const {
	if (r.revealTime_max <= time) return true;
	if (time < r.revealTime_min)  return false;
	// cout << "online_requests_appearing_times[" << r.getVertex().getRegion() << "][" << r.revealTS << "] =" << flush << online_requests_appearing_times[r.getVertex().getRegion()][r.revealTS] << endl;
	return online_requests_appearing_times[r.getVertex().getRegion()][r.revealTS] <= time;
}


// void VRP_instance::updateRevealTimes_potentialRequests(double curr_time) {

// 	for (VRP_vertex* pvertex : getRandomCustomerVertices()) {
// 		for (int ts=1; ts <= getNumberTimeSlots(); ts++) {
// 			VRP_request& req = pvertex->getRequest_mutable(ts);
// 			if (curr_time <= req.revealTime_max and not req.hasAppeared() and req.p > 0) 
// 				req.revealTime = ceil(max(curr_time, req.revealTime_min) + (req.revealTime_max - max(curr_time, req.revealTime_min)) / 2.0);
// 		}
// 	}

// 	for (VRP_vertex* pvertex : getOnlineVertices()) {
// 		for (int ts=1; ts <= getNumberTimeSlots(); ts++) {
// 			VRP_request& req = pvertex->getRequest_mutable(ts);
// 			if (curr_time <= req.revealTime_max and not req.hasAppeared() and req.p > 0) 
// 				req.revealTime = ceil(max(curr_time, req.revealTime_min) + (req.revealTime_max - max(curr_time, req.revealTime_min)) / 2.0);
// 		}
// 	}

// }


void VRP_instance::setIntegerTravelTimes() {
	integer_travel_times = true;
	for (VRP_VehicleType* veh_type : vehicle_types) {
		// cout << "Setting integer_travel_times for vehicle type [" << veh_type->getName() << "]" << endl;
		veh_type->integer_travel_times = true;
	}
}

void VRP_instance::setTimeDependent() {
	time_dependent_travel_times = true;
	for (VRP_VehicleType* veh_type : vehicle_types) {
		// cout << "Setting integer_travel_times for vehicle type [" << veh_type->getName() << "]" << endl;
		veh_type->time_dependent_travel_times = true;
	}
}


void VRP_instance::print_TD_bins() const {
	_ASSERT_(time_dependent_travel_times, "");
	for (VRP_VehicleType* veh_type : vehicle_types) {
		cout << "Vehicle: " << veh_type->getName() << endl;
		veh_type->print_TD_bins();
		cout << endl << endl;
	}
}


double VRP_instance::getMaxReqProb() const {
	double max_p = 0.0;
	for (const VRP_vertex* pvertex : getRandomCustomerVertices()) {
		for (int ts=0; ts <= getNumberTimeSlots(); ts++) {
			const VRP_request& req = pvertex->getRequest(ts);
			max_p = max(req.p, max_p);
		}
	}

	for (const VRP_vertex* pvertex : getOnlineVertices()) {
		for (int ts=1; ts <= getNumberTimeSlots(); ts++) {
			const VRP_request& req = pvertex->getRequest(ts);
			max_p = max(req.p, max_p);
		}
	}
	return max_p;
}




string& VRP_instance::toString(bool verbose) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	
	out << "Instance:"
		<< "   nRegions: " << nRegions << "(";
	if (depot_vertices.size()) out << "#depot:" << depot_vertices.size();
	if (reg_vertices.size()) out << ", #regular:" << reg_vertices.size();
	if (random_cust_vertices.size()) out << ", #customer:" << random_cust_vertices.size();
	if (random_demand_vertices.size()) out << ", #random_demand:" << random_demand_vertices.size();
	if (reg_online_vertices.size()) out << ", #online:" << reg_online_vertices.size();
	if (wait_vertices.size()) out << ", #waiting:" << wait_vertices.size();
	out	<< ")   nVeh: " << _nVeh;
	if (H == numeric_limits<int>::max())
		out << 	"   H: +inf";
	else	
		out	<< "   H: " << H;
	out	<< "   nTimeSlots: " << nTimeSlots 
		<< "   (orig. H: " << orig_horizon << ", scale: " << scale << ")";

	if (verbose) {}

	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringCoords(bool and_attributes) const {
	ostringstream out, inf("inf");
	out.setf(std::ios::fixed);
	out.precision(2);

	out << outputMisc::greenExpr(true) << "         x     y";
	if (and_attributes)
			out << "     q     s     e     l";
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(4) << i << "   " << outputMisc::resetColor();
		out << setw(4) << (int) coordX[i] << "  " << setw(4) << (int) coordY[i] << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringProbasTS(bool instance_ids) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	out << outputMisc::greenExpr(true) << "Probas (%)" << endl << "TS:  "; 
	for (int ts=0; ts < nTimeSlots+1; ts++) 
		out << setw(3) << ts << "  "; 

	out << endl << "V:" << endl << outputMisc::resetColor() ;
	for (const VRP_vertex* pvertex : getRandomCustomerVertices()) {
		int n;
		if (instance_ids) n = pvertex->getIdInstance();
		else n = pvertex->region;
		out << outputMisc::greenExpr(true) << setw(3) << n << "   " << outputMisc::resetColor();
		for (int ts=0; ts < nTimeSlots+1; ts++) {
			const VRP_request& req = pvertex->getRequest(ts);
			out << outputMisc::redExpr(req.p > 0) << setw(2) << setprecision(1) << req.p * 100 << outputMisc::resetColor() << setw(3) << "  ";
		}
		out << endl;
	}

	out << endl << "V:" << endl << outputMisc::resetColor() ;
	for (const VRP_vertex* pvertex : getOnlineVertices()) {
		int n;
		if (instance_ids) n = pvertex->getIdInstance();
		else n = pvertex->region;
		out << outputMisc::greenExpr(true) << setw(3) << n << "   " << outputMisc::resetColor();
		for (int ts=0; ts < nTimeSlots+1; ts++) {
			const VRP_request& req = pvertex->getRequest(ts);
			out << outputMisc::redExpr(req.p > 0) << setw(2) << setprecision(1) << req.p * 100 << outputMisc::resetColor() << setw(3) << "  ";
		}
		out << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toStringDiscreteDemandsProbas() const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	_ASSERT_(getMaxRandomDemand() > 0, "argh");

	out << outputMisc::greenExpr(true) << " Q:  "; 
	for (int q = 0; q <= getMaxRandomDemand(); q++) 
		out << setw(3) << q << "  "; 
	out << endl << " V:    \%" << endl << outputMisc::resetColor() ;

	for (const VRP_vertex* pvertex : getRandomDemandVertices()) {
		out << outputMisc::greenExpr(true) << setw(3) << pvertex->region << "   " << outputMisc::resetColor();
		const VRP_request& req = pvertex->getRequest();
		for (int q = 0; q <= getMaxRandomDemand(); q++) 
			out << outputMisc::redExpr(req.getDiscreteDemandProb(q) > 0) << setw(2) << (int) (req.getDiscreteDemandProb(q)*100) << outputMisc::resetColor() << setw(3) << "  ";
		out << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toStringPotentialRequests(bool instance_ids) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	ASSERT(getRandomCustomerVertices().size() > 0, "");
	ASSERT(nTimeSlots > 0, "");

	for (const VRP_vertex* pvertex : getRandomCustomerVertices()) {
		for (int ts=1; ts < nTimeSlots+1; ts++) {
			VRP_request& req = pvertex->requests_ts[ts];
			if (req.p > 0)
				out << req.toString(true, instance_ids) << endl;
		}
	}

	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringPotentialOnlineRequests() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	int n = 0;
	double expected = 0.0;
	vector<double> exp_ts(nTimeSlots+1, 0.0);
	out << "Online requests probabilities (\%):";
	for (int ts=1; ts <= nTimeSlots; ts++) out << "     TS" << setw(2) << ts;
	out << endl;
	for (const VRP_vertex* pvertex : getOnlineVertices()) {
		double exp_nb_req = 0.0;
		for (int ts=1; ts <= nTimeSlots; ts++) {
			VRP_request& req = pvertex->requests_ts[ts];
			// exp_nb_req += req.p / 100.0;
			// exp_ts[ts] += req.p / 100.0;
			exp_nb_req += req.p;
			exp_ts[ts] += req.p;
		}
		if (exp_nb_req > 0) {
			n++;
			out << "                      " << pvertex->toString() << "(" << setw(5) << pvertex->getIdInstance() << "):    ";
			for (int ts=1; ts < nTimeSlots+1; ts++) {
				VRP_request& req = pvertex->requests_ts[ts];
				// out << setw(6) << setprecision(2) << req.p << "   ";
				if (req.p > 0)
					out << req.toString(true,true) << "    ";
			} out << endl;
		}
		expected += exp_nb_req;
	}
	out << "E[#requests] = " << expected << "   (" << n << " potential locations, out of " << getOnlineVertices().size();
	for (int ts=1; ts <= nTimeSlots; ts++) out << "   TS" << setw(2) << ts << ":" << exp_ts[ts];
	out << endl;
	static string str = ""; str = out.str();
	return (str);
}



string& VRP_instance::toStringInstanceMetrics() const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	// out << get_customer_vertices().size() << endl;
	for (auto veh_type : getVehicleTypes()) {
		out << "Metrics for vehicle type 1 " << veh_type->toString() << ":" << endl; 
		double avg_dist = 0.0;
		for (const VRP_vertex* pv_1 : getRandomCustomerVertices())
			for (const VRP_vertex* pv_2 : getRandomCustomerVertices())
				avg_dist += veh_type->travelTime(*pv_1, *pv_2) / (getRandomCustomerVertices().size() * getRandomCustomerVertices().size());
		out << "Average travel time between customer regions: " << avg_dist << endl;
		
		avg_dist = 0.0;
		for (const VRP_vertex* pw : getWaitingVertices())
			for (const VRP_vertex* pv : getRandomCustomerVertices())
				avg_dist += veh_type->travelTime(*pw, *pv) / (getWaitingVertices().size() * getRandomCustomerVertices().size());
		out << "Average travel time between customer regions and waiting locations: " << avg_dist << endl;


		double exp_tw = 0.0;
		for (const VRP_vertex* pv : getRandomCustomerVertices()) {
			double exp_cust_tw = 0.0;
			for (int ts =1; ts <= nTimeSlots; ts++) {
				// cout << "ts:" << ts << "\t" << pv->getRequest(ts).e << " \t" << pv->getRequest(ts).l << " \t" << pv->getRequest(ts).p << endl;
				exp_cust_tw += (pv->getRequest(ts).l - pv->getRequest(ts).e +1) * pv->getRequest(ts).p/100 / 2;
			}
			exp_tw += exp_cust_tw / getRandomCustomerVertices().size();
		}
		out << "Expected time window size: " << exp_tw << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toStringVehicleTypes(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	for (auto* veh_type : getVehicleTypes())
		out << veh_type->toString(verbose) << endl;

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toString_AppearingOnlineRequests(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	for (auto& req_attr : online_requests_appeared)
		out << req_attr.toString(verbose) << endl;

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::RequestAttributes::toString(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	out << "\tRequest at vertex " << vertex->toString(verbose) << " (instance_id: " << vertex->getIdInstance() << ", time slot " << setw(3) << time_slot << ") appears at time " << reveal_time; 

	static string str = ""; str = out.str();
	return (str);
}



void VRP_instance::setDiscreteDemandProb(int region, int demand, double p) {
	const VRP_vertex* v = getVertexFromRegion(VRP_vertex::RANDOM_DEMAND, region);
	_ASSERT_(demand >= 0 && demand <= v->max_demand, "demand=" << demand);
	v->getRequest().demand_prob[demand] = p;
}



string& VRP_instance::toString_TravelTimes(bool real_ids, double curr_time) const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);



	for (auto veh_type : getVehicleTypes()) {
		if (veh_type->integer_travel_times)
			out << "[" << veh_type->getName() << "] Integer travel times !" << endl;
		out << outputMisc::greenExpr(true) << setw(6) << veh_type->getName(); 
		for (int j=0; j<nRegions; j++) {
			int region = j;
			if (real_ids) region = region_to_id_instance_file[j];
			out << setw(8) << region; 
		}
		out << endl << endl << outputMisc::resetColor() ;
		for (int i=0; i<nRegions; i++) {	
			int region = i;
			if (real_ids) region = region_to_id_instance_file[i];
			out << outputMisc::greenExpr(true) << setw(4) << region << "    " << outputMisc::resetColor();
			for (int j=0; j<nRegions; j++) {
				double tt = veh_type->travelTime(i,j, curr_time);
				out << setw(6) << tt << "  ";
			}
			out << endl;
		}
	}

	static string str = ""; str = out.str();
	return (str);
}








/* VRP_vertex +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
VRP_vertex::VRP_vertex(int region, VertexType type, int nTimeSlots, int instance_id, int max_demand) {
	this->region = region;
	this->id_instance_file = instance_id;
	this->type = type;
	this->nTimeSlots = nTimeSlots;
	this->max_demand = max_demand;

	if (type == VRP_vertex::RANDOM_CUSTOMER || type == VRP_vertex::REGULAR_ONLINE) {
		this->requests_ts = new VRP_request[nTimeSlots+1]; 
		for (int ts=0; ts <= nTimeSlots; ts++) 
			requests_ts[ts].vertex = this;
	}
	else if (type == VRP_vertex::RANDOM_DEMAND) {
		_ASSERT_(max_demand > 0, "");
		this->requests_ts = new VRP_request(this); 
	}
	else
		this->requests_ts = new VRP_request(this); 

	// cout << "Creating vertex at region " << region << "(instance id: " << instance_id << ") of type " << type << endl;
}

bool VRP_vertex::operator== (const VRP_vertex& v) const {
	return region == v.region && id_instance_file == v.id_instance_file && type == v.type && nTimeSlots == v.nTimeSlots;
}


const VRP_request& VRP_vertex::getRequest(int timeSlot) const {
	ASSERT(timeSlot >= 0, "argh");
	ASSERT(!(timeSlot > 0) || (type == VRP_vertex::RANDOM_CUSTOMER || type == VRP_vertex::REGULAR_ONLINE), "only CUSTOMER or REGULAR_ONLINE vertex type has time slots !");
	if (timeSlot > 0) ASSERT(timeSlot <= nTimeSlots, "time slot=" << timeSlot << "), type=" << type);
	return requests_ts[timeSlot];
}
const VRP_request& VRP_vertex::getRequest() const {
	ASSERT(type != VRP_vertex::RANDOM_CUSTOMER && type != VRP_vertex::REGULAR_ONLINE, "REGULAR_ONLINE (or CUSTOMER) vertex must take a timeslot argument !");
	return getRequest(0);
}

VRP_request& VRP_vertex::getRequest_mutable(int timeSlot) {
	ASSERT(timeSlot >= 0, "argh");
	if (timeSlot > 0)
		ASSERT(timeSlot <= nTimeSlots && (type == VRP_vertex::RANDOM_CUSTOMER || type == VRP_vertex::REGULAR_ONLINE) , "time slot=" << timeSlot << "), type=" << type);
	return requests_ts[timeSlot];
}
VRP_request& VRP_vertex::getRequest_mutable() {
	ASSERT(type != VRP_vertex::RANDOM_CUSTOMER, "argh");
	return requests_ts[0];
}

string& VRP_vertex::toStringRealVertexNumbers(bool verbose) const {
	return toString(verbose, true);
}

string& VRP_vertex::toString(bool verbose) const {
	return toString(verbose, false);
}

string& VRP_vertex::toString(bool verbose, bool instance_numbering) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	if (! verbose || type == VRP_vertex::WAITING || type == VRP_vertex::RANDOM_CUSTOMER || type == VRP_vertex::RANDOM_DEMAND) {
		switch(type) {
			case VRP_vertex::DEPOT: out << "D"; break;
			case VRP_vertex::REGULAR: out << "R"; break;
			case VRP_vertex::RANDOM_CUSTOMER: out << "C"; break;
			case VRP_vertex::RANDOM_DEMAND: out << "\u03A9"; break;		// the Ω (Omega) letter
			case VRP_vertex::WAITING: out << "W.."; break;
			case VRP_vertex::REGULAR_ONLINE: out << "O"; break;
			default: out << "?";
		}

		// if (type == VRP_vertex::REGULAR_ONLINE && )
		if (instance_numbering)
			out << setfill('0') << setw(3) << id_instance_file; 
		else 
			out << setfill('0') << setw(3) << region; 
	}
	if (verbose && (type == VRP_vertex::REGULAR || type == VRP_vertex::REGULAR_ONLINE || type == VRP_vertex::DEPOT)) {
		out << getRequest(0).toString(true);
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_vertex::toStringType() const {
	ostringstream out;
	switch(type) {
		case VRP_vertex::DEPOT: out << "DEPOT"; break;
		case VRP_vertex::REGULAR: out << "REGULAR"; break;
		case VRP_vertex::RANDOM_CUSTOMER: out << "RANDOM_CUSTOMER"; break;
		case VRP_vertex::RANDOM_DEMAND: out << "RANDOM_DEMAND"; break;		// the Ω (Omega) letter
		case VRP_vertex::WAITING: out << "WAITING"; break;
		case VRP_vertex::REGULAR_ONLINE: out << "REGULAR_ONLINE"; break;
		default: out << "?";
	}
	static string str = ""; str = out.str();
	return (str);
}

bool VRP_vertex::checkConsistency() const {
	// TODO
	return true;
}
















/* VRP_request +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

VRP_request::VRP_request(const VRP_vertex* v) {
	vertex = v;
	// id = -1;
	// id_instance_file = -1;
	// e = -1;
	// l = numeric_limits<int>::max();
	// demand = 0;
	// revealTime = -1;		
	// revealTime_min = -1;	
	// revealTime_max = -1;	
	// revealTS = -1;
	// p = 0;
	if (vertex && vertex->type == VRP_vertex::RANDOM_DEMAND) {
		_ASSERT_(vertex->max_demand > 0, "");
		demand_prob = new double[vertex->max_demand+1]; 
		for (int q=0; q <= vertex->max_demand; q++) 
			demand_prob[q] = 0.0;
	}
}

bool (*VRP_request::request_comparison_lt)(const VRP_request &, const VRP_request &) = NULL;

bool VRP_request::operator==(const VRP_request& other) const {
	return id_instance_file == other.id_instance_file && id == other.id;
}

bool VRP_request::operator<(const VRP_request& other) const {
	if (request_comparison_lt == NULL)
		return id_instance_file < other.id_instance_file;
	else 
		return request_comparison_lt(*this, other);
}

bool VRP_request::checkConsistency() const {
	ASSERT(e <= l, "");
	return true;
}

void VRP_request::setAppeared() {
	ASSERT(vertex->getType() == VRP_vertex::REGULAR_ONLINE, "argh");
	ASSERT(!appeared, "request" << toString(true) << "was already set as appeared !");
	appeared = true;
}


bool VRP_request::hasAppeared() const {
	ASSERT(vertex->getType() == VRP_vertex::REGULAR_ONLINE, "argh");
	return appeared;
}


void VRP_request::setRevealTime(double rt) {
	revealTime = rt;
}

int VRP_request::getRevealTimeSS(double curr_time) const {
	_ASSERT_(curr_time <= revealTime_max, curr_time << " > " << revealTime_max);
	// return max((int) revealTime, (int) ceil(curr_time));
	return ceil(max(revealTime_min, curr_time) + (revealTime_max - max(revealTime_min, curr_time)) / 2.0);
}






double VRP_request::getDiscreteDemandProb(int q) const {
	ASSERT(vertex, "");
	ASSERT(q >= 0 && q <= vertex->max_demand, "q=" << q);
	return demand_prob[q];
}

string& VRP_request::toStringRealVertexNumbers(bool verbose) const {
	return toString(verbose, true);
}

string& VRP_request::toString(bool verbose) const {
	return toString(verbose, false);
}

string& VRP_request::toString(bool verbose, bool instance_numbering) const {
	ostringstream out, tmp, inf;
	out.setf(std::ios::fixed);
	out.precision(2);
	if (!verbose) {
		int reg = vertex->region;
		if (instance_numbering) reg = vertex->id_instance_file;

		if (vertex->type == VRP_vertex::RANDOM_CUSTOMER)
			out << setw(3) << id << "[" << setw(3) << reg << ":" << setfill('0') << setw(2) << revealTS << "]"; // << "-" << setfill('0') << setw(3) << (int)revealTime << "]";
		if (vertex->type == VRP_vertex::REGULAR_ONLINE) 
			out << "O" << revealTS << "_" << setfill('0') << setw(3) << reg;
		else
			out << "v:" << vertex->toString();
	}
	else {
		tmp << (int) l; inf << "inf";
		out 
			<< "[v:" << vertex->id_instance_file
			// << "id:" << setw(4) << id_instance_file 
			// << ", " << vertex->toString() 
			<< ",e:" << setw(4) << (int) e 
			<< ",l:" << setw(4) << ( (l < numeric_limits<int>::max()) ? tmp.str() : inf.str())
			<< ",s-:" << setw(3) << (int) add_service_time
			<< ",q:" << setw(3) << (int) demand
			;
		if (vertex->getType() == VRP_vertex::REGULAR_ONLINE || vertex->getType() == VRP_vertex::RANDOM_CUSTOMER)
			out 
				<< ",rt: [" << setw(5) << (int) revealTime_min << " ≤ " << (int) getRevealTimeSS(-1) << " ≤ " << (int) revealTime_max
				<< ",ts:"<< setw(3) << (int) revealTS 
				<< ",p:" << setw(3) << (double) p; 
		out << "]";
	} 

	

	static string str = ""; str = out.str();
	return (str);
}





























VRP_VehicleType::VRP_VehicleType(int nRegions, int H, string name, int capacity, double velocity, double costPerDistanceUnit) {
	ASSERT(nRegions > 0, "");
	this->nRegions = nRegions;
	this->H = H;

	this->name = name; 
	this->capacity = capacity; 
	this->velocity = velocity; 
	this->costPerDistanceUnit = costPerDistanceUnit;

	for (int v1=0; v1 < nRegions; v1++) {
		dist.push_back(vector<double>(nRegions, 0.0));
		travel_times.push_back(vector<double>(nRegions, 0.0));

		// time-dependent matrices
		dist_TD.push_back(vector<vector<double>>(nRegions, (vector<double>(H, -1))));
		travel_times_TD.push_back(vector<vector<double>>(nRegions, vector<double>(H, -1)));
	}
	service_times.assign(nRegions, 0.0);
	TD_curr_bin.assign(H, -1);
	TD_next_bin.assign(H, -1);
}

bool VRP_VehicleType::operator == (const VRP_VehicleType& other) const {
	return nRegions == other.nRegions && name == other.name && capacity == other.capacity && velocity == other.velocity && costPerDistanceUnit == other.costPerDistanceUnit;
}



void VRP_VehicleType::setServiceTime(int region, double t) {
	_ASSERT_(region < nRegions && region >= 0, "argh");
	_ASSERT_(0 <= t, "argh");
	service_times[region] = t;
}



void VRP_VehicleType::preComputeEuclideanDistances(const vector<double>& coordX, const vector<double>& coordY, bool setTravelTimes) {
	// cout << nRegions << endl;
	for (int v1=0; v1 < nRegions; v1++) {
		for (int v2=0; v2 < nRegions; v2++) {
			double x = ( sqrt(  pow(static_cast<double>(coordX[v1] - coordX[v2]),2) 
						 		  + pow(static_cast<double>(coordY[v1] - coordY[v2]),2) ) 
						);
			// cout << v1 << ":" << v2 << " : " << coordX[v1] << "-" << coordY[v1] << " = " << x << endl;
			dist[v1][v2] = x;
			if (setTravelTimes)
				travel_times[v1][v2] = x;
		}
	}	
	if (setTravelTimes)
		travel_times_NE_distances = true;
}

void VRP_VehicleType::setTravelDistance(int region1, int region2, double d, int fromTU, int toTU) {
	_ASSERT_(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	_ASSERT_(0 <= d, "argh");
	bool time_dependent = (fromTU >= 0 or toTU >= 0);

	if (time_dependent) { 
		for (int tu = fromTU; tu <= toTU; tu++) 
			dist_TD[region1][region2][tu] = d;
	}
	else
		dist[region1][region2] = d;
}



void VRP_VehicleType::computeAverageDistances() {
	for (int v1=0; v1 < nRegions; v1++) 
		for (int v2=0; v2 < nRegions; v2++) {
			double sum = 0.0;
			for (int tu = 0; tu < H; tu++) sum += dist_TD[v1][v2][tu];
			dist[v1][v2] = sum / H;
		}		
}


void VRP_VehicleType::setTravelTime(int region1, int region2, double t, int fromTU, int toTU) {
	_ASSERT_(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	_ASSERT_(0 <= t, "argh");
	_ASSERT_(fromTU < H and toTU < H, fromTU << "  " << toTU);
	travel_times_NE_distances = true;
	bool time_dependent = (fromTU >= 0 or toTU >= 0);
	
	if (region1 == 0 and region2 == 1) cout << "0 -> 1: " << fromTU << " to " << toTU << " : " << t << endl;

	if (time_dependent) {
		for (int tu = fromTU; tu <= toTU; tu++) 
			travel_times_TD[region1][region2][tu] = t;

		if (TD_curr_bin[fromTU] == -1) {
			for (int tu = fromTU; tu <= toTU; tu++) {
				TD_curr_bin[tu] = fromTU;
				TD_next_bin[tu] = toTU+1;
			}
		}
	}
	else
		travel_times[region1][region2] = t;
	
}

void VRP_VehicleType::print_TD_bins() const {
	for (int tu = 0; tu < H; tu++)
		cout << "TU: " << tu << " \tTD_curr_bin[tu] = " << TD_curr_bin[tu] << "  \tTD_next_bin[tu]" << TD_next_bin[tu] << endl;
}


double 	VRP_VehicleType::distance_TD(int region1, int region2, double curr_time) const { 
	ASSERT(curr_time >= 0, curr_time);
	return dist_TD[region1][region2][max(H-1, (int) floor(curr_time))];
}


// Computes the travel time required along the arc (region1, region2), when current time is start_time and the remaining distance to travel is remain_dist
// adapted from algorithm from Eglese, Maden, Slater, 2006, "A road timetable to aid vehicle routing and scheduling"
// simpler version (because recursive)
double VRP_VehicleType::tt_TD(int region1, int region2, double start_time, double remain_dist) const {
	ASSERT(start_time >= 0, start_time);
	ASSERT(remain_dist >= -0.000001, remain_dist);

	if (remain_dist <= 0) return 0.0;

	int TU_start_time = min(H-1, (int) floor(start_time));

	// double v = dist_TD[region1][region2][TU_start_time] / travel_times_TD[region1][region2][TU_start_time];
	double v = dist[region1][region2] / travel_times_TD[region1][region2][TU_start_time];


	// cout << "tt_TD called: " << "  " << region1 << "  " << region2 << " :  start_time=" << start_time << "  dist=" << dist[region1][region2] << "  remain_dist=" << remain_dist << "   v:" << v << "  tt=" << travel_times_TD[region1][region2][TU_start_time] << "  TU=" << TU_start_time << endl;

	double arrival_time = start_time + remain_dist / v;
	if (arrival_time >= H or arrival_time <= TD_next_bin[TU_start_time]) {
		// cout << " \t tt_TD DONE; arrival_time= " << arrival_time << "   remain_dist=" << remain_dist << "   v:" << v << "   tt=" << travel_times_TD[region1][region2][TU_start_time] << endl;
		return remain_dist / v;
	}

	int new_start_time 	=  TD_next_bin[TU_start_time];
	double traveled 	= v * (new_start_time - start_time);
	remain_dist    		-= traveled;

	ASSERT(new_start_time > start_time, "");
	ASSERT(new_start_time > 0, "");

	// cout << "\t RECURSIVE CALL !!!   --> new_start_time=" << new_start_time << "  traveled: " << traveled << "   remain_dist=" << remain_dist << endl;
	double tt = new_start_time - start_time + tt_TD(region1, region2, new_start_time, remain_dist);
	ASSERT(tt > 0, tt);

	// cout << "\t tt_TD DONE; tt = " << tt << endl;
	return tt;
}


double VRP_VehicleType::dep_TD(int region1, int region2, double arrival_time, double remain_dist) const {
	// ASSERT(arrival_time >= 0, arrival_time);
	ASSERT(remain_dist >= -0.00001, remain_dist << " " << region1 << " " << region2);

	if (remain_dist <= 0) return arrival_time;

	int TU_arrival_time = min(H-1, (int) floor(arrival_time));	

	// double v = dist_TD[region1][region2][TU_arrival_time] / travel_times_TD[region1][region2][TU_arrival_time];
	double v = dist[region1][region2] / travel_times_TD[region1][region2][TU_arrival_time];
	double start_time = arrival_time - remain_dist / v;

	// cout << "dep_TD called: " << "  " << region1 << "  " << region2 << " :  arrival_time=" << arrival_time << "  dist=" << dist[region1][region2] << "  remain_dist=" << remain_dist << "   v:" << v << "  tt=" << travel_times_TD[region1][region2][TU_arrival_time] << "  TU=" << TU_arrival_time << endl;

	if (start_time <= 0 or start_time >= H or start_time >= TD_curr_bin[TU_arrival_time]) {
		// cout << " \t dep_TD DONE; start_time= " << start_time << "   remain_dist=" << remain_dist << "   v:" << v << "   tt=" << travel_times_TD[region1][region2][TU_arrival_time] << endl;
		return start_time;
	}

	double new_arrival_time = max((double) TD_curr_bin[TU_arrival_time]-1, start_time);
	double traveled 	= v * (arrival_time - new_arrival_time);
	remain_dist -= traveled;

	ASSERT(remain_dist >= -0.0000001, remain_dist << " " << region1 << " " << region2);

	// cout << "\t RECURSIVE CALL !!!   -->  start_time=" << start_time << "  new_arrival_time=" << new_arrival_time << "  traveled: " << traveled << "   remain_dist=" << remain_dist << endl;
	double dep_t = dep_TD(region1, region2, new_arrival_time, remain_dist);

	// cout << "\t dep_TD DONE; dep_t = " << dep_t << endl;
	return dep_t;
}



// double 	VRP_VehicleType::travelTime_TD(int region1, int region2, double curr_time) const { 
// 	// ASSERT(curr_time >= 0 and curr_time < H, curr_time);
// 	// ASSERT(curr_time >= 0, curr_time);
// 	if (region1 == region2) return 0.0;

// 	return tt_TD(region1, region2, curr_time, dist_TD[region1][region2][min(H-1, (int) floor(curr_time))]);

// }



double 	VRP_VehicleType::departureTime_TD(int region1, int region2, double arr_time) const { 
	// ASSERT(arr_time >= 0 and arr_time < H, arr_time);
	if (region1 == region2) return arr_time;

	// return dep_TD(region1, region2, arr_time, dist_TD[region1][region2][min(H-1, (int) floor(arr_time))]);
	return dep_TD(region1, region2, arr_time, dist[region1][region2]);

}



double 	VRP_VehicleType::distance(int region1, int region2, double curr_time) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	if (time_dependent_travel_times) ASSERT(curr_time >= 0, curr_time << ", time_dependent: " << time_dependent_travel_times);
	
	if (time_dependent_travel_times) {
		return distance_TD(region1, region2, curr_time);
	}

	return dist[region1][region2]; 
}
double 	VRP_VehicleType::distance(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time) const { 
	if (time_dependent_travel_times) {
		return distance_TD(v1.getRegion(), v2.getRegion(), curr_time);
	}

	return dist[v1.getRegion()][v2.getRegion()]; 
}






double 	VRP_VehicleType::travelTime(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time, bool force_nonInteger) const { 
	return travelTime(v1.getRegion(), v2.getRegion(), curr_time, force_nonInteger);
}	
double 	VRP_VehicleType::travelTime(int region1, int region2, double curr_time, bool force_nonInteger) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	if (time_dependent_travel_times) ASSERT(curr_time >= 0, curr_time << ", time_dependent: " << time_dependent_travel_times);
	ASSERT(travel_times_NE_distances, "");

	// cout << "VRP_VehicleType::travelTime  called  " << region1 << " " << region2 << " " << curr_time << endl;
	if (region1 == region2) return 0.0;

	

	double tt;
	if (time_dependent_travel_times) {
		// tt = travelTime_TD(region1, region2, curr_time);
		tt = tt_TD(region1, region2, curr_time, dist[region1][region2]); 
		// tt = tt_TD(region1, region2, curr_time, dist_TD[region1][region2][min(H-1, (int) floor(curr_time))]); 
		ASSERT(tt > 0, tt << "  " << dist[region1][region2] << "  " << curr_time);
		// ASSERT(tt > 0, tt << "  " << dist_TD[region1][region2][min(H-1, (int) floor(curr_time))] << "  " << curr_time);
	}
	else
		tt = travel_times[region1][region2]; 

	if (integer_travel_times and not force_nonInteger)
		tt = ceil(tt); 
	
	// double d = dist_TD[region1][region2][min(H-1, (int) floor(curr_time))];
	// cout << curr_time << " travel time " << region1 << " -> " << region2 << ": " << tt << "    d= " << d << endl;
	return tt;
		
}	



double 	VRP_VehicleType::departureTime(const VRP_vertex& v1, const VRP_vertex& v2, double arrival_time) const { 
	return departureTime(v1.getRegion(), v2.getRegion(), arrival_time);
}	
double 	VRP_VehicleType::departureTime(int region1, int region2, double arrival_time) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	if (time_dependent_travel_times) ASSERT(arrival_time >= 0, arrival_time << ", time_dependent: " << time_dependent_travel_times);
	ASSERT(travel_times_NE_distances, "");


	double dep_t;
	if (time_dependent_travel_times)
		dep_t = departureTime_TD(region1, region2, arrival_time);
	else
		dep_t = arrival_time - travel_times[region1][region2]; 

	if (integer_travel_times)
		dep_t = floor(dep_t); 
	
	// double d = dist_TD[region1][region2][min(H-1, (int) floor(arrival_time))];
	// cout << arrival_time << " departure time " << region1 << " -> " << region2 << ": " << dep_t << "    d= " << d << endl;
	return dep_t;

}	


double 	VRP_VehicleType::travelCost(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time) const { 
	return travelCost(v1.getRegion(), v2.getRegion(), curr_time);
}	
double 	VRP_VehicleType::travelCost(int region1, int region2, double curr_time) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");

	if (time_dependent_travel_times)
		return distance_TD(region1, region2, curr_time) * costPerDistanceUnit;

	return dist[region1][region2] * costPerDistanceUnit; 
}	


double 	VRP_VehicleType::totalServiceTime(const VRP_request& r) const { 
	return serviceTime(r.getVertex().getRegion()) + r.add_service_time;
}	
double 	VRP_VehicleType::totalServiceTime(const VRP_vertex& v) const { 
	return serviceTime(v) + v.getRequest().add_service_time;
}	
double 	VRP_VehicleType::serviceTime(const VRP_vertex& v) const { 
	return serviceTime(v.getRegion());
}	
double 	VRP_VehicleType::serviceTime(int region) const { 
	ASSERT(region < nRegions && region >= 0, "region=" << region << "  nRegions=" << nRegions);
	return service_times[region]; 
}	


string& VRP_VehicleType::toString(bool verbose) const {
	UNUSED(verbose);
	ostringstream out; 
	// out.setf(std::ios::fixed);
	// out.precision(2);
	out << "\"" << name << "\"[Q=" << capacity << ",v=" << velocity << ",c=" << costPerDistanceUnit << "]";
	if (integer_travel_times) out << " IntTT";
	static string str = ""; str = out.str();
	return (str);
}


string& VRP_VehicleType::toString_Distances() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << distance(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}

string& VRP_VehicleType::toString_TravelTimes() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << travelTime(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}
string& VRP_VehicleType::toString_TravelCosts() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << travelCost(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}









