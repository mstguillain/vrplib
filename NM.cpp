/* NeighborhoodManager +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See NM.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#include <iostream>
#include <cmath>
#include "VRP_solution.h"
#include "NM.h"
#include "tools.h"


template <class S>
NeighborhoodManager<S>::NeighborhoodManager(S& solution, VRP_type vrp_type, int horizon, int min_increment, int max_increment) : solution(solution) {
	ASSERT(min_increment <= max_increment, min_increment << " > " << max_increment);
	this->horizon 		= horizon;
	this->min_increment = min_increment;
	this->max_increment = max_increment;

	current_sequence_idx 	= -1;
	current_operator_size 	= 1;
	for(int i=0; i < 1000; i++)
		success[i] = -1;

	sol_vrp_type = vrp_type;
}
// template <class S>
// void NeighborhoodManager<S>::setWaitingTimeMultiple(double multiple) {
// 	ASSERT(multiple > 0, "");
// 	waiting_time_multiple = multiple;
// }

template <class S>
bool NeighborhoodManager<S>::shakeSolution(bool verbose){
	// first check whether there is something to do, i.e. if there is at least one customer we can try to move
	if (solution.getNumberNonFixedRoutes() == 0) {
		if (verbose) cout << "no free route anymore ! abording shakeSolution()" << endl;
		return 0;
	}
	
	int operator_ = -1;
	if (sol_vrp_type == SS_VRPTW_CR) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(SSVRPTW_CR_SEQUENCE) / sizeof(SSVRPTW_CR_SEQUENCE[0]));
		operator_ = SSVRPTW_CR_SEQUENCE[current_sequence_idx];
	}
	else if (sol_vrp_type == DS_VRPTW_GSA) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(DSVRP_GSA_SEQUENCE) / sizeof(DSVRP_GSA_SEQUENCE[0]));
		operator_ = DSVRP_GSA_SEQUENCE[current_sequence_idx];
	}

	else if (sol_vrp_type == DS_VRPTW_GSA_SIMPLE) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(DSVRP_GSA_SEQUENCE_SIMPLE) / sizeof(DSVRP_GSA_SEQUENCE_SIMPLE[0]));
		operator_ = DSVRP_GSA_SEQUENCE_SIMPLE[current_sequence_idx];
	}
	
	else if (sol_vrp_type == DS_VRPTW) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(VRP_SEQUENCE_NODIFF) / sizeof(VRP_SEQUENCE_NODIFF[0]));
		operator_ = VRP_SEQUENCE_NODIFF[current_sequence_idx];
	}
	else if (sol_vrp_type == VRPTW || sol_vrp_type == VRP || sol_vrp_type == MTSP || sol_vrp_type == TSP || sol_vrp_type == SS_VRP_CD) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(VRP_SEQUENCE) / sizeof(VRP_SEQUENCE[0]));
		operator_ = VRP_SEQUENCE[current_sequence_idx];
	}
	else if (sol_vrp_type == JOBSHOP || sol_vrp_type == JOBSHOP_SHARED) {
		current_sequence_idx = (current_sequence_idx +1) % (sizeof(SCHED_SEQUENCE) / sizeof(SCHED_SEQUENCE[0]));
		operator_ = SCHED_SEQUENCE[current_sequence_idx];
	}
	else _ASSERT_(false, "VRP type " << sol_vrp_type << " not supported in NM");

	// cout << "operator_ = " << operator_ << endl;
	// last_e1.type = -1; last_e2.type = -1;
	// last_pos1.type = -1; last_pos2.type = -1;
	// last_waiting_time_modif = 0;
	last_route_1.clear(), last_route_2.clear();

	if (success[operator_] < 0) 
		success[operator_] = 0;
	last_operator_number_called = operator_;
	if(applyOperator(operator_, verbose)) {
		it++;
		return true;
	} else 
		return false;
}

template <class S>
bool NeighborhoodManager<S>::applyOperator(int operator_, bool verbose) {
	switch (operator_) {
		case RELOCATE_RANDOM: 			return relocate_random(verbose); 							
		case RELOCATE_WORST_TO_BEST: 	return relocate_worst_to_best(verbose); 					
		case RELOCATE_BEST_IMPROVEMENT: return relocate_best_improvement(verbose);					
		case TWO_EXCHANGE:				return two_exchange(verbose); 								
		case TWO_OPT_INVERSION:			return two_opt_inversion(current_operator_size, verbose); 
		case CROSS_EXCHANGE:			return cross_exchange(current_operator_size, verbose); 		
		case INSERT_WAITING_VERTEX:		return insert_waiting_vertex(verbose); 						
		case REMOVE_WAITING_VERTEX:		return remove_waiting_vertex(verbose); 						
		case ADD_WAITING_TIME: 			return add_waiting_time(false, verbose); 							
		case REMOVE_WAITING_TIME: 		return remove_waiting_time(false, verbose); 						
		case TRANSFER_WAITING_TIME: 	return transfer_waiting_time(false, verbose); 						
		case ADD_WAITING_TIME_ANY: 		return add_waiting_time(true, verbose); 							
		case ADD_WAITING_TIME_VIOLATED: return add_waiting_time_violated(verbose); 							
		case REMOVE_WAITING_TIME_ANY: 	return remove_waiting_time(true, verbose); 						
		case TRANSFER_WAITING_TIME_ANY: return transfer_waiting_time(true, verbose); 						
		default: _ASSERT_(false, "invalid operator");
	}
	return false;
}


template <class S>
void NeighborhoodManager<S>::restorePreviousSolution(S& from_solution, bool verbose) {
	if (verbose) cout << "[NM:RESTORE] routes ";
	for (SolutionElement route : modifiedRoutes) {
		if (verbose) cout << route.toString() << ", ";
		
		solution.copyRouteFrom(from_solution, route);
	} 
	solution.copyGlobalVariablesFrom(from_solution);
	if (verbose) cout << endl;

	modifiedRoutes.clear();

	// int operator_;
	// if (sequence.compare("SS_VRP_CR") == 0)
	// 	operator_ = SSVRPTW_CR_SEQUENCE[current_sequence_idx];
	// if (sequence.compare("VRP") == 0)
	// 	operator_ = VRP_SEQUENCE[current_sequence_idx];
	// if (sequence.compare("VRP_basic") == 0)
	// 	operator_ = VRP_SEQUENCE_BASIC[current_sequence_idx];

	// // if (verbose) cout << "restoring solution from previous neighborhood operation: ";
	// switch (operator_) {
	// 	// case RELOCATE_RANDOM || RELOCATE_WORST_TO_BEST || RELOCATE_BEST_IMPROVEMENT:
	// 	// 	if (verbose) cout << "[RESTORE] moving vertex " << last_e1.toString() << " back to pos " << last_pos1.toString() << endl;
	// 	// 	solution.moveVertex(last_e1, last_pos1);
	// 	// 	break;
	// 	// case TWO_EXCHANGE:
	// 	// 	if (verbose) cout << "[RESTORE] swapping back vertices " << last_e1.toString() << " and " << last_e2.toString() << endl;  
	// 	// 	solution.swapVertices(last_e1, last_e2);
	// 	// 	break;
	// 	// case TWO_OPT_INVERSION:
	// 	// 	if (verbose) cout << "[RESTORE] inverting back segment " << last_e1.toString() << endl;
	// 	// 	solution.invertSegment(last_e1);
	// 	// 	break;
	// 	// case CROSS_EXCHANGE:
	// 	// 	if (last_e2.type > 0) {
	// 	// 		if (verbose) cout << "[RESTORE] swapping back segments " << last_e1.toString() << " and " << last_e2.toString() << endl;
	// 	// 		solution.swapSegments(last_e1, last_e2);
	// 	// 	} else {
	// 	// 		if (verbose) cout << "[RESTORE] moving back segment " << last_e1.toString() << " to pos " << last_pos1.toString() << endl;
	// 	// 		solution.moveSegment(last_e1, last_pos1);
	// 	// 	}
	// 	// 	break;
	// 	// case INSERT_WAITING_VERTEX:
	// 	// 	if (verbose) cout << "[RESTORE] removing inserted waiting vertex " << last_e1.toString() << endl;
	// 	// 	solution.removeWaitingVertex(last_e1);
	// 	// 	break;
	// 	// case REMOVE_WAITING_VERTEX:
	// 	// 	if (verbose) 
	// 	// 		cout << "[RESTORE] inserting back removed waiting vertex (region: " << last_e1.toString() << ") at pos " << last_pos1.toString() << " with w_time=" << last_waiting_time_modif << endl;
	// 	// 	solution.insertWaitingVertex(last_e1, last_pos1);
	// 	// 	solution.setWaitingTime(last_e2, last_waiting_time_modif);
	// 	// 	break;
	// 	// case ADD_WAITING_TIME:
	// 	// 	if (verbose) 
	// 	// 		cout << "[RESTORE] removing added waiting time (qtty: " << last_waiting_time_modif <<  ") at waiting vertex " << last_e1.toString() << endl;
	// 	// 	solution.decreaseWaitingTime(last_e1, last_waiting_time_modif);
	// 	// 	break;
	// 	// case REMOVE_WAITING_TIME:
	// 	// 	if (verbose) 
	// 	// 		cout << "[RESTORE] putting back removed waiting time (qtty: " << last_waiting_time_modif <<  ") at waiting vertex " << last_e1.toString() << endl;
	// 	// 	solution.increaseWaitingTime(last_e1, last_waiting_time_modif);
	// 	// 	break;
	// 	case RELOCATE_RANDOM :
	// 	case RELOCATE_WORST_TO_BEST :
	// 	case RELOCATE_BEST_IMPROVEMENT :
	// 	case TWO_EXCHANGE :
	// 	case CROSS_EXCHANGE :
	// 		if (verbose) cout << "[RESTORE] routes " << last_route_1.toString() << " and (" << ((last_route_1 == last_route_2) == false) << ") " << last_route_2.toString() << endl;
	// 		solution.copyRouteFrom(from_solution, last_route_1);
	// 		if ((last_route_1 == last_route_2) == false)
	// 			solution.copyRouteFrom(from_solution, last_route_2); 
	// 		break;
	// 	case TWO_OPT_INVERSION :
	// 	case INSERT_WAITING_VERTEX :
	// 	case REMOVE_WAITING_VERTEX :
	// 	case ADD_WAITING_TIME :
	// 	case REMOVE_WAITING_TIME:
	// 		if (verbose) cout << "[RESTORE] route " << last_route_1.toString() << endl;
	// 		solution.copyRouteFrom(from_solution, last_route_1); 
	// 		break;
	// 	default: _ASSERT_(false, "invalid operator");
	// }
}


template <class S>
void NeighborhoodManager<S>::intensify() {
	// k = 1; 
	current_sequence_idx  = 0;
	current_operator_size = 1;
}

template <class S>
void NeighborhoodManager<S>::diversify() {
	// k = k%K + 1;
	if(current_sequence_idx == 0) 
		current_operator_size = current_operator_size % MAX_OPERATOR_SIZE + 1;
}

template <class S>
void NeighborhoodManager<S>::neighborAccepted() {
	// cout << "neighbor accepted: ";
	// switch (last_operator_number_called) {
	// 	case RELOCATE_RANDOM: 			cout << "relocate_random";				break;
	// 	case RELOCATE_WORST_TO_BEST: 	cout << "relocate_worst_to_best";		break;
	// 	case RELOCATE_BEST_IMPROVEMENT: cout << "relocate_best_improvement";	break;
	// 	case TWO_EXCHANGE:				cout << "two_exchange";					break;
	// 	case TWO_OPT_INVERSION:			cout << "two_opt_inversion";			break;
	// 	case CROSS_EXCHANGE:			cout << "cross_exchange";				break;
	// 	case INSERT_WAITING_VERTEX:		cout << "insert_waiting_vertex";		break;
	// 	case REMOVE_WAITING_VERTEX:		cout << "remove_waiting_vertex";		break;
	// 	case ADD_WAITING_TIME: 			cout << "add_waiting_time"; 			break;
	// 	case REMOVE_WAITING_TIME: 		cout << "remove_waiting_time";			break;
	// 	default: _ASSERT_(false,"");
	// }
	// cout << endl << flush;
	success[last_operator_number_called]++;
}

template <class S>
void NeighborhoodManager<S>::printNeighborhoodScores() const {
	for (int i=0; i < 1000; i++) {
		if (success[i] < 0) continue;
		cout << endl << "success[";
		switch (i) {
			case RELOCATE_RANDOM: 			cout << "relocate_random";				break;
			case RELOCATE_WORST_TO_BEST: 	cout << "relocate_worst_to_best";		break;
			case RELOCATE_BEST_IMPROVEMENT: cout << "relocate_best_improvement";	break;
			case TWO_EXCHANGE:				cout << "two_exchange";					break;
			case TWO_OPT_INVERSION:			cout << "two_opt_inversion";			break;
			case CROSS_EXCHANGE:			cout << "cross_exchange";				break;
			case INSERT_WAITING_VERTEX:		cout << "insert_waiting_vertex";		break;
			case REMOVE_WAITING_VERTEX:		cout << "remove_waiting_vertex";		break;
			case ADD_WAITING_TIME: 			cout << "add_waiting_time"; 			break;
			case REMOVE_WAITING_TIME: 		cout << "remove_waiting_time";			break;
			case TRANSFER_WAITING_TIME: 	cout << "transfer_waiting_time";		break;
			case ADD_WAITING_TIME_ANY: 		cout << "add_waiting_time(any)"; 		break;
			case REMOVE_WAITING_TIME_ANY: 	cout << "remove_waiting_time(any)";		break;
			case TRANSFER_WAITING_TIME_ANY: cout << "transfer_waiting_time(any)";	break;
			default: _ASSERT_(false, "");
		}
		cout << "] = " << success[i];
		cout << endl;
	}
}



/* 	Relocate random: the simpliest neighborhood operator
	Moves one RANDOM vertex from its position to a random different position
*/
template <class S>
bool NeighborhoodManager<S>::relocate_random(bool verbose) {
	if (verbose) cout << "relocate_random called : ";	
	if (solution.getNumberMoveableVertices() == 0) { // abort if there is no moveable vertex in the solution
		if (verbose) cout << "no moveable vertices" << endl;
		return 0;
	}
	if (solution.getNumberFreePositions() < 3) { // abort if there is not enough free (= non fixed) positions in the solution (2 positions for the moveable vertex to be moved, plus one extra free position where to move it)
		if (verbose) cout << "not enough free positions" << endl;
		return 0;
	}

	// SELECT a vertex
	SolutionElement v;
	solution.selectRandomMoveableVertex(&v);
	if (verbose) cout << "relocating vertex " << v.toString();

	// MOVE it
	SolutionElement pos;
	solution.selectRandomInsertionPosition(v, &pos);	

	if (verbose) cout << " at position " << pos.toString() << endl;

	if (sol_vrp_type == SS_VRPTW_CR && solution.getRouteSize(v.route) == 2 && solution.getRouteSize(pos.route) == 1) {
		if (verbose) cout << "\tdoes not change anything in SS-VRPTW-CR solution; abort" << endl;
		return 0;
	}

	solution.moveVertex(v, pos);

	// pos.getVertex(&last_e1);
	// v.getPos(&last_pos1);
	// if (v.route == pos.route && v.pos < pos.pos)
	// 	last_e1.pos--;
	// if (v.route == pos.route && v.pos > pos.pos)
	// 	last_pos1.pos++;

	v.getRoute(&last_route_1);
	pos.getRoute(&last_route_2);

	modifiedRoutes.insert(last_route_1);
	modifiedRoutes.insert(last_route_2);

	return 1;
}



/* 	Relocate Worst to Best: 
	1) SELECT the vertex that 
		a) is implied in the most violations
		b) contributes the most in the overall solution cost
	2) MOVE that vertex to the place that:
		a) minimizes the number of violations
		b) minimizes the cost
*/
template <class S>
bool NeighborhoodManager<S>::relocate_worst_to_best(bool verbose) {
	if (verbose) cout << "relocate_worst_to_best called : ";	
	if (solution.getNumberMoveableVertices() == 0) { // abort if there is no free (= non fixed) vertex in the solution
		if (verbose) cout << "no moveable vertices" << endl;
		return 0;
	}
	if (solution.getNumberFreePositions() < 3) { // abort if there is not enough free (= non fixed) positions in the solution (2 positions for the moveable vertex to be moved, plus one extra free position where to move it)
		if (verbose) cout << "not enough free positions" << endl;
		return 0;
	}

	// SELECT the vertex involving the most violations and the worst cost
	SolutionElement v; 
	solution.selectWorstMoveableVertex(&v);
	if (verbose) 
		cout << "relocating vertex " << v.toString();

	// MOVE it
	SolutionElement pos;
	solution.selectBestRelocationPosition(v, &pos);
	if (verbose)
		cout << " at position " << pos.toString() << endl;
	solution.moveVertex(v, pos);

	// pos.getVertex(&last_e1);
	// v.getPos(&last_pos1);


	v.getRoute(&last_route_1);
	pos.getRoute(&last_route_2);

	modifiedRoutes.insert(last_route_1);
	modifiedRoutes.insert(last_route_2);

	return 1;
}


/* 	Relocate Best Improvement: 
	Moves one RANDOM vertex from its position to the best possible position
*/
template <class S>
bool NeighborhoodManager<S>::relocate_best_improvement(bool verbose) {
	if (verbose) cout << "relocate_best_improvement called : ";	
	if (solution.getNumberMoveableVertices() == 0) { // abort if there is no free (= non fixed) vertex in the solution
		if (verbose) cout << "no moveable vertices" << endl;
		return 0;
	}
	if (solution.getNumberFreePositions() < 3) { // abort if there is not enough free (= non fixed) positions in the solution (2 positions for the moveable vertex to be moved, plus one extra free position where to move it)
		if (verbose) cout << "not enough free positions" << endl;
		return 0;
	}

	// SELECT a vertex
	SolutionElement v;
	solution.selectRandomMoveableVertex(&v);
	if (verbose) 
		cout << "relocating vertex " << v.toString();
	
	// MOVE it
	SolutionElement pos;
	solution.selectBestRelocationPosition(v, &pos);	
	if (verbose)
		cout << " at position " << pos.toString() << endl;
	solution.moveVertex(v, pos);

	// pos.getVertex(&last_e1);
	// v.getPos(&last_pos1);


	v.getRoute(&last_route_1);
	pos.getRoute(&last_route_2);

	modifiedRoutes.insert(last_route_1);
	modifiedRoutes.insert(last_route_2);

	return 1;
}



/* 	Two-Exchange : from paper "A two-stage hybrid algorithm for the VRPTW", Bent & Van Hentenryck 
	exchanges the positions of two vertices picked at random (a random "swap")
*/
template <class S>
bool NeighborhoodManager<S>::two_exchange(bool verbose) {
	if (verbose) cout << "two_exchange called : ";	
	if (solution.getNumberMoveableVertices() < 2) {	// abort if there is no enough moveable (= non fixed and non-depot) vertices in the solution
		if (verbose) cout << "not enough moveable vertices" << endl;
		return 0; 
	}

	// SELECT two different vertices
	SolutionElement v1, v2;
	solution.selectRandomMoveableVertex(&v1);
	int count = 0;
	do {
		solution.selectRandomMoveableVertex(&v2);
		count++;
	} while (v1 == v2 || (!solution.exchangeable(v1, v2) && count < 100));
	
	if (verbose) cout << "swapping vertices " << v1.toString() << " and " << v2.toString() << endl;

	if (sol_vrp_type == SS_VRPTW_CR && solution.getRouteSize(v1.route) == 2 && solution.getRouteSize(v2.route) == 2) {
		if (verbose) cout << "\tdoes not change anything in SS-VRPTW-CR solution; abort" << endl;
		return 0;
	}
	solution.swapVertices(v2, v1);	

	// last_e1 = v1;
	// last_e2 = v2;


	v1.getRoute(&last_route_1);
	v2.getRoute(&last_route_2);

	modifiedRoutes.insert(last_route_1);
	modifiedRoutes.insert(last_route_2);

	return 1;
}

/* 	
	2-opt with sequence inversion : from paper "A two-stage hybrid algorithm for the VRPTW", Bent & Van Hentenryck 
*/
template <class S>
bool NeighborhoodManager<S>::two_opt_inversion(int max_size, bool verbose) {
	max_size = max(2, max_size);
	if (verbose) cout << "two_opt_inversion called (max_size=" << max_size << ") : ";	
	
	// check whether there are still at least one route with minimum two non-fixed customer vertices
	int n = 0;
	for (int k=1; k <= solution.getNumberRoutes(); k++)	
		if (solution.getNumberMoveableVertices(k) >= 2)
			n++;
	if (n == 0) {
		if (verbose) cout << "no route with at least 2 moveable vertices" << endl;
		return 0;
	}

	SolutionElement s;
	solution.selectRandomMoveableSegment(&s, 2, max_size);
	if (verbose) 
		cout << "invert segment " << s.toString() << endl;
	solution.invertSegment(s);

	// last_e1 = s;


	s.getRoute(&last_route_1);

	modifiedRoutes.insert(last_route_1);

	return 1;
}


/* 	Cross-exchange : based on paper "A variable neighborhood search for the MDVRPTW", Polacek, Hartl and Doerner
	Exchanges two segments of maximum sizes 'size' between two different routes; one of the two sizes can be zero (20% chance); 
	Todo: move of one segment within the same route (the so-called OR-EXCHANGE)
*/
template <class S>
bool NeighborhoodManager<S>::cross_exchange(int max_size, bool verbose) {
	if (verbose) cout << "cross_exchange called (max_size=" << max_size << ") : ";	

	if (solution.getNumberNonFixedRoutes() < 2)	{ // check whether there are still at least two available vehicles
		if (verbose) cout << "not enough free route" << endl;
		return 0;	
	}
	if (solution.getNumberMoveableVertices() < 1) { // and at least one moveable vertex
		if (verbose) cout << "no available moveable vertices" << endl;
		return 0;	
	}

	SolutionElement s1, s2, pos;
	solution.selectRandomMoveableSegment(&s1, 1, max_size);	// choose the first segment (minimum segment length of 1)
	int chance = rand() % 100 + 1;
	if (chance <= 80 && solution.getNumberModifiableRoutes() >= 2) {		// 80% chance to swap two segments (only if there is still a second route with moveable vertices)
		solution.selectRandomMoveableSegment(&s2, 1, max_size, -42, s1.route);	// choose the second segment (minimum segment length of 1, and not in s1.route)
		if (verbose) 
			cout << "swap segments " << s1.toString() << " and " << s2.toString() << endl;
		if (sol_vrp_type == SS_VRPTW_CR) {
			int s1_len = s1.pos_2 - s1.pos_1 + 1;	// number of vertices involved in the segment
			int s2_len = s2.pos_2 - s2.pos_1 + 1;
			if (s1_len + 1 == solution.getRouteSize(s1.route) && s2_len + 1 == solution.getRouteSize(s2.route)) {	// getRouteSize() returns the size of the route, depot included
				if (verbose) cout << "\tdoes not change anything in SS-VRPTW-CR solution; abort" << endl;
				return 0;
			}
		}
		solution.swapSegments(s1, s2);
		
		// last_e1 = s1;
		// last_e2 = s2;

		s1.getRoute(&last_route_1);
		s2.getRoute(&last_route_2);

		modifiedRoutes.insert(last_route_1);
		modifiedRoutes.insert(last_route_2);
	} 
	else {	// 20% of chance to just move one segment (s1) in another route
		solution.selectRandomInsertionPosition(s1, &pos, -42, s1.route);	// not_route = s1.route --> avoid the same route as s1
		if (verbose) 
			cout << "move segment " << s1.toString() << " to position " << pos.toString() << endl;
		
		if (sol_vrp_type == SS_VRPTW_CR) {
			int s1_len = s1.pos_2 - s1.pos_1 + 1;	// number of vertices involved in the segment
			if (s1_len + 1 == solution.getRouteSize(s1.route) && solution.getRouteSize(pos.route) == 1) {	// getRouteSize() returns the size of the route, depot included
				if (verbose) cout << "\tdoes not change anything in SS-VRPTW-CR solution; abort" << endl;
				return 0;
			}
		}
		solution.moveSegment(s1, pos);

		// s1.getPos(&last_pos1);							// start position of segment s1
		// pos.getSegment(s1.getLength(), &last_e1);		// segment of length obtained by starting at pos


		s1.getRoute(&last_route_1);
		pos.getRoute(&last_route_2);

		modifiedRoutes.insert(last_route_1);
		modifiedRoutes.insert(last_route_2);
	}



	return 1;
}

/* 	Insert Waiting Vertex: 
	Insert k_size random waiting vertices at :
		1) random position (50%)
		2) best insert (50%)
*/
template <class S>
bool NeighborhoodManager<S>::insert_waiting_vertex(bool verbose) {
	if (verbose) cout << "insert_waiting_vertex called : ";	
	if (solution.getNumberNonFixedRoutes() == 0) {
		if (verbose) cout << "no available free route" << endl;
		return 0;
	}
	if (solution.getNumberUnvisitedWaitingVertices() == 0) {
		if (verbose) cout << "no available unvisited waiting vertices" << endl;
		return 0;
	}

	SolutionElement waiting_location;
	solution.selectRandomWaitingLocation(&waiting_location);

	// int chance = rand() % 100 + 1;
	SolutionElement pos;

	// for(int i=0; i<k_size; i++) {
		// if (chance <= 50) 
			solution.selectRandomInsertionPosition(&pos);
		// else 
		// 	solution.selectBestInsertionPosition(waiting_location, &pos);

		if (verbose) 
			cout << "inserting waiting vertex " << waiting_location.toString() << " at position " << pos.toString() << endl;
		solution.insertWaitingVertex(waiting_location, pos);
	// }

	// pos.getVertex(&last_e1);
	pos.getRoute(&last_route_1);

	modifiedRoutes.insert(last_route_1);

	return 1;
}


/* 	Remove Waiting Vertex: 
	Remove k_size random *non fixed* waiting vertices from :
		1) random position (100%)
		2) worst violation position (0%)
	Todo: 2)
*/
template <class S>
bool NeighborhoodManager<S>::remove_waiting_vertex(bool verbose) {
	if (verbose) 
		cout << "remove_waiting_vertex called : ";	
	if (solution.getNumberFreeWaitingVertices() == 0) {
		if (verbose) cout << "no available FREE waiting vertices" << endl;
		return 0;
	}

	// for(int i=0; i< min(k_size, solution.getNumberFreeWaitingVertices()); i++) {
		// SELECT the vertex
		SolutionElement waiting_vertex;
		solution.selectRandomFreePlannedWaitingVertex(&waiting_vertex);


		if (verbose) 
			cout << "removing waiting vertex " << waiting_vertex.toString();
		int last_waiting_time_modif = solution.removeWaitingVertex(waiting_vertex);
		if (verbose) 
			cout << " having w_time=" << last_waiting_time_modif << endl;
	// }

	// waiting_vertex.getRegion(&last_e1);
	// waiting_vertex.getPos(&last_pos1);
	// last_e2 = waiting_vertex;
	waiting_vertex.getRoute(&last_route_1);
	modifiedRoutes.insert(last_route_1);

	return 1;
}



/* 	Increase Waiting Time at waiting vertices: 
	Increase waiting time at k_size random *unleft* waiting vertices from :
		1) random position (100%)
		2) worst violation position (0%)
	Todo: 2)
*/
template <class S>
bool NeighborhoodManager<S>::add_waiting_time(bool to_any_vertex_type, bool verbose) {
	if (verbose) cout << "add_waiting_time called : ";	
	

	if (not to_any_vertex_type && solution.getNumberUNLEFTWaitingVertices() == 0) {
		if (verbose) cout << "no available UNLEFT waiting vertices" << endl;
		return 0;
	}
	if (to_any_vertex_type && solution.getNumberUNLEFT_Vertices() == 0) {
		if (verbose) cout << "no available UNLEFT vertices" << endl;
		return 0;
	}

	// for(int i=0; i < min(k_size, solution.getNumberUNLEFTWaitingVertices()); i++) {
		// SELECT the vertex
		SolutionElement vertex;
		if (to_any_vertex_type)
			solution.selectRandomUNLEFT_Vertex(&vertex);
		else 
			solution.selectRandomUNLEFTWaitingVertex(&vertex);
		// double increment = (rand() % 100 + 1) / 10;
		double increment = (rand() % (min(horizon, max_increment) - min_increment +1) ) + min_increment;
		// double increment = waiting_time_multiple * ( (rand() % (int)(min(horizon, max_increment)/waiting_time_multiple - min_increment +1) ) + min_increment );
		// double increment = 1.0;

		if (verbose) 
			cout << "adding " << increment << " time units to wait. time at vertex " << vertex.toString() << endl;
		solution.increaseWaitingTime(vertex, increment);

	// }

	// last_e1 = vertex;
	vertex.getRoute(&last_route_1);
	modifiedRoutes.insert(last_route_1);

	return 1;
}

template <class S>
bool NeighborhoodManager<S>::add_waiting_time_violated(bool verbose) {
	if (verbose) cout << "add_waiting_time_violated called : ";	
	
	if (solution.getNumberViolatedVertices() == 0) {
		if (verbose) cout << "no available VIOLATED vertices" << endl;
		return 0;
	}

	SolutionElement vertex;
	solution.selectRandomViolatedVertex(&vertex);
	// double increment = (rand() % 100 + 1) / 10;
	double increment = (rand() % (min(horizon, max_increment) - min_increment +1) ) + min_increment;
	// double increment = waiting_time_multiple * ( (rand() % (int)(min(horizon, max_increment)/waiting_time_multiple - min_increment +1) ) + min_increment );
	// double increment = 1.0;

	if (verbose) 
		cout << "adding " << increment << " time units to wait. time at vertex " << vertex.toString() << endl;
	solution.increaseWaitingTime(vertex, increment);

	
	vertex.getRoute(&last_route_1);
	modifiedRoutes.insert(last_route_1);

	return 1;
}



/* 	
*/
template <class S>
bool NeighborhoodManager<S>::transfer_waiting_time(bool to_any_vertex_type, bool verbose) {
	if (verbose) cout << "transfer_waiting_time called : ";	

	if (not to_any_vertex_type && solution.getNumberUNLEFTWaitingVertices() < 2) {
		if (verbose) cout << "no enough UNLEFT waiting vertices" << endl;
		return 0;
	}
	if (to_any_vertex_type && solution.getNumberUNLEFT_Vertices() < 2) {
		if (verbose) cout << "no enough UNLEFT vertices" << endl;
		return 0;
	}

	// for(int i=0; i < min(k_size, solution.getNumberUNLEFTWaitingVertices()); i++) {
		// SELECT the vertices
		SolutionElement waiting_vertex_1, waiting_vertex_2;
		if (to_any_vertex_type) {
			solution.selectRandomUNLEFT_Vertex(&waiting_vertex_1);
			do solution.selectRandomUNLEFT_Vertex(&waiting_vertex_2); 
			while (waiting_vertex_1 == waiting_vertex_2);
		}
		else {
			solution.selectRandomUNLEFTWaitingVertex(&waiting_vertex_1);
			do solution.selectRandomUNLEFTWaitingVertex(&waiting_vertex_2); 
			while (waiting_vertex_1 == waiting_vertex_2);
		} 

		


		double wait_time = (rand() % (max_increment - min_increment +1) ) + min_increment;
		// double wait_time = waiting_time_multiple * ( (rand() % (int)(min(horizon, max_increment)/waiting_time_multiple - min_increment +1) ) + min_increment );

		double actual_wait_time = solution.decreaseWaitingTime(waiting_vertex_1, wait_time, true);
		if (actual_wait_time > 0) {
			if (verbose) cout << "transfering " << actual_wait_time << " waiting time units from vertex " << waiting_vertex_1.toString() << " to vertex " << waiting_vertex_2.toString() << endl;
			solution.increaseWaitingTime(waiting_vertex_2, actual_wait_time);
		} else {
			if (verbose) cout << "cannot remove any further waiting time from " << waiting_vertex_1.toString() << "; abort" << endl;
			return 0;
		}
	// }

	// last_e1 = waiting_vertex;
	waiting_vertex_1.getRoute(&last_route_1);
	modifiedRoutes.insert(last_route_1);

	waiting_vertex_2.getRoute(&last_route_2);
	modifiedRoutes.insert(last_route_2);

	return 1;
}


/* 	DECREASE Waiting Time at waiting vertices: 
	Decrease waiting time at k_size random *non fixed* waiting vertices 
*/
template <class S>
bool NeighborhoodManager<S>::remove_waiting_time(bool to_any_vertex_type, bool verbose) {
	if (verbose) cout << "remove_waiting_time called : ";	

	if (not to_any_vertex_type && solution.getNumberFreeWaitingVertices() == 0) {
		if (verbose) cout << "no available FREE waiting vertices" << endl;
		return 0;
	}
	if (to_any_vertex_type && solution.getNumberUNSERVED_Vertices() == 0) {
		if (verbose) cout << "no available FREE vertices" << endl;
		return 0;
	}

	// for(int i=0; i < min(k_size, solution.getNumberFreeWaitingVertices()); i++) {
		// SELECT the vertex


		SolutionElement vertex;
		if (to_any_vertex_type)
			solution.selectRandomUNSERVED_Vertex(&vertex);
		else 
			solution.selectRandomFreePlannedWaitingVertex(&vertex);

		// double decrement = (rand() % 100 + 1) / 10;
		double decrement = (rand() % (max_increment - min_increment +1) ) + min_increment;
		// double decrement = waiting_time_multiple * ( (rand() % (int)(min(horizon, max_increment)/waiting_time_multiple - min_increment +1) ) + min_increment );
		// double decrement = 1.0;
		if (verbose) 
			cout << "removing " << decrement << " time units from wait. time at vertex " << vertex.toString() << endl;
		int removed = solution.decreaseWaitingTime(vertex, decrement);
		if (removed == 0) {
			if (verbose) cout << "\tnothing removed !" << endl;
			return 0;
		}
	// }

	// last_e1 = vertex;
	vertex.getRoute(&last_route_1);
	modifiedRoutes.insert(last_route_1);

	return 1;
}













