/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_scenario +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	This class is used to represent a VRP scenario / scenario pool, in a dynamic context.

	The constructors usually take an object of type VRP_instance, describing the current problem.
	Concerning VRP_ScenarioPool, one also precise the desired number of scenari in the pool. 

	The goal of a VRP_ScenarioPool object is to compute expected cost, based on random event (which 
	are stochastically described in the instance). 
	In order to compute an expected cost, one provides a solution object of template type Solution, 
	and the VRP_ScenarioPool then assess the operational actions described in that solution to each
	and every scenario of its pool, in order to compute the expectation. 
	How a solution is confronted to a scenario is directly defined in the evalSolution() method of
	the corresponding Scenario class.

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#ifndef VRP_SCENARIO_H
#define	VRP_SCENARIO_H

#include <iostream>
#include <vector>
#include <set>
#include <cmath>
#include <limits>
#include "VRP_instance.h"
#include "VRP_solution.h"
// #include "types.h"
#include "tools.h"
#include "json.hpp"




struct SimulationResults {
	double 	avg_rejected 	= 0.0;
	double 	avg_delay 		= 0.0;
	double 	avg_serviced 	= 0.0;
	double	avg_appeared	= 0.0;

	int   	nb_rejected		= 0;
	double 	sum_delay 		= 0.0;
	int   	nb_serviced		= 0;
	int 	nb_appeared		= 0;
};




template <class Scenario, class Solution> 
class VRP_ScenarioPool {
protected:
	const VRP_instance& I;
	vector<Scenario> scenarioPool;	
	int numberScenarios;
	VRP_type sol_type;
private:
	// vector<vector<vector<double>>> hProbas_Q;		// for debugging

public:

	VRP_ScenarioPool(const VRP_instance& instance, int poolSize, VRP_type vrp_type, int currentTime = -1) : I(instance) {
		ASSERT(poolSize > 0, "argh");
		ASSERT(vrp_type != VRPTYPE_NO_SET, "");
		sol_type = vrp_type;
		numberScenarios = poolSize;
		for (int i=0; i<poolSize; i++)
			scenarioPool.push_back(Scenario(instance, sol_type, currentTime));
	}

	VRP_ScenarioPool(const VRP_instance& instance, json j_scenarios, VRP_type vrp_type, int currentTime = -1) : I(instance) {
		ASSERT(vrp_type != VRPTYPE_NO_SET, "");
		sol_type = vrp_type;
		for (json j_scenario : j_scenarios){
			scenarioPool.push_back(Scenario(instance, j_scenario, sol_type, currentTime));
		}
	}

	const vector<Scenario> & getScenarios() const { return scenarioPool; }

	void updatePool(double currentTime) {
		for (auto& s : scenarioPool)
			s.updateScenario(currentTime);
	}
	int reSamplePool(double currentTime, float proportion = 1.0) {
		
		if(proportion <= 0.0)
			return 0;

		if(proportion == 1.0) {
			for (auto& s : scenarioPool)
				s.reSampleScenario(currentTime);
			return scenarioPool.size();
		}

		double nReSample = (double) scenarioPool.size() * proportion;
		int nReS = floor(nReSample) + (rand() % 100000 + 1 <= (nReSample-floor(nReSample))*100000); // decide probabilistically for the remaining fraction
		if(nReS > 0) {
			int fromPos = rand() % (scenarioPool.size()-nReS+1);
			for(int i=fromPos; i<fromPos+nReS; i++) {
				scenarioPool[i].reSampleScenario(currentTime);
			}		
		}
		return nReS;
	}

	SimulationResults computeExperimentalExpectedCost(const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
		_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");

		SimulationResults r_avg;
		// double eval = 0.0;
		for (auto& s : scenarioPool) {
			SimulationResults r = s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);
			// eval += s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);
			r_avg.nb_rejected 	+= r.nb_rejected;
			r_avg.nb_serviced 	+= r.nb_serviced;
			r_avg.nb_appeared 	+= r.nb_appeared;
			r_avg.sum_delay 	+= r.sum_delay;
		}
		r_avg.avg_serviced	= (double) r_avg.nb_serviced / scenarioPool.size();
		r_avg.avg_rejected	= (double) r_avg.nb_rejected / scenarioPool.size();
		r_avg.avg_appeared	= (double) r_avg.nb_appeared / scenarioPool.size();
		if (r_avg.nb_serviced > 0)
			r_avg.avg_delay		= r_avg.sum_delay / r_avg.nb_serviced;
		// return eval / scenarioPool.size();
		
		return r_avg;
	}

	// double computeExperimentalAverageDelay(const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
	// 	_ASSERT_(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");

	// 	double avg_delay = 0.0;
	// 	for (auto& s : scenarioPool)
	// 		avg_delay += s.avgDelay(solution, strategy, debug, verbose, scenarioHorizon);

	// 	return avg_delay / scenarioPool.size();
	// }

	std::string& toString(bool realVertexNumbers = true) const {
		ostringstream out;
		for (auto& s : scenarioPool)
			out << s.toString(realVertexNumbers) << endl << endl;
		static string str = ""; str = out.str(); return (str);
	}
	std::string& toString_perRegion(bool realVertexNumbers = true) const {
		ostringstream out;
		for (auto& s : scenarioPool)
			out << s.toString_perRegion(realVertexNumbers) << endl << endl;
		static string str = ""; str = out.str(); return (str);
	}


	// SimulationResults evalSolution_GSA(const Solution& solution, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
	// 	ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");

	// 	// double eval = 0.0;
	// 	SimulationResults r_avg;
	// 	for (auto& s : scenarioPool) {
	// 		SimulationResults r = s.evalSolution_R_GSA(solution, verbose, scenarioHorizon);
	// 		r_avg.nb_rejected 	+= r.nb_rejected;
	// 		r_avg.nb_serviced 	+= r.nb_serviced;
	// 		r_avg.nb_appeared 	+= r.nb_appeared;
	// 		r_avg.sum_delay 	+= r.sum_delay;
	// 	}
	// 	r_avg.avg_serviced	= (double) r_avg.nb_serviced / scenarioPool.size();
	// 	r_avg.avg_rejected	= (double) r_avg.nb_rejected / scenarioPool.size();
	// 	r_avg.avg_appeared	= (double) r_avg.nb_appeared / scenarioPool.size();
	// 	if (r_avg.nb_serviced > 0)
	// 		r_avg.avg_delay		= r_avg.sum_delay / r_avg.nb_serviced;
	// }

	std::string& toString_avgRandomDemands() const {
		ostringstream out;
		_ASSERT_(sol_type == SS_VRP_CD, "");
		vector<vector<double>> avg;
		avg.resize(I.getNumberRegions());	// not +1 because nRegions already counts the depot	which has number 0
		for (auto& vec : avg) 
			vec.assign(I.getMaxRandomDemand() +1, 0.0);
		

		for (int r = 0; r < I.getNumberRegions(); r++) 
			for (int q = 0; q <= I.getMaxRandomDemand(); q++)
				for (auto& s : scenarioPool) 
					if (s.getSampledDemandAtRegion(r) == q) 
						avg[r][q] += (double) 1 / numberScenarios;

		out << outputMisc::greenExpr(true) << " Q:  "; 
		for (int q = 0; q <= I.getMaxRandomDemand(); q++) 
			out << setw(3) << q << "  "; 
		out << endl << " V:    \%" << endl << outputMisc::resetColor() ;

		// for (int r = 1; r < I.getNumberRegions(); r++) {
		for (const VRP_vertex* pvertex : I.getRandomDemandVertices()) {
			// const VRP_vertex* pvertex = I.getVertexFromRegion(VRP_vertex::RANDOM_DEMAND, r);
			out << outputMisc::greenExpr(true) << setw(3) << pvertex->getRegion() << "   " << outputMisc::resetColor();
			// const VRP_request& req = pvertex->getRequest();
			for (int q = 0; q <= I.getMaxRandomDemand(); q++) 
				out << outputMisc::redExpr(avg[pvertex->getRegion()][q] > 0) << setw(2) << (int) (avg[pvertex->getRegion()][q]*100) << outputMisc::resetColor() << setw(3) << "  ";
			out << endl;
		}


		static string str = ""; str = out.str(); return (str);
	}
};






template <class Scenario, class Solution> 
class VRP_ScenarioPool_Volatile {
protected:
	const VRP_instance& I;
	int numberScenarios;
	VRP_type sol_type;
	int currentTime = -1;
private:
	// const Solution* solution_hProbas;
	// vector<vector<vector<double>>> hProbas_Q;		// for debugging

	// vector<vector<vector<double>>> h_Qplus_w, g1_Qplus_w, g2_Qplus_w;
	// vector<vector<vector<vector<double>>>> h_Qplus, g1_Qplus, g2_Qplus;

public:
	friend class Scenario_SS_VRPTW_CR;

	VRP_ScenarioPool_Volatile(const VRP_instance& instance, int nb_scenarios, VRP_type vrp_type, int currentTime = -1) : I(instance) {
		numberScenarios = nb_scenarios;
		sol_type = vrp_type;
		this->currentTime = currentTime;
	}

	SimulationResults computeExperimentalExpectedCost(const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) {
		ASSERT(scenarioHorizon == numeric_limits<int>::max(), "not implemented yet");
		// double eval = 0.0;
		SimulationResults r_avg;

		// Scenario s(I, this);
		Scenario s(I, sol_type, currentTime);
		double sum_avg_delay = 0.0;
		int n_nonempty_scnenarios = 0;
		for (int i=0; i<numberScenarios; i++) {
			SimulationResults r = s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);
			// eval += s.evalSolution(solution, strategy, debug, verbose, scenarioHorizon);
			r_avg.nb_rejected 	+= r.nb_rejected;
			r_avg.nb_serviced 	+= r.nb_serviced;
			r_avg.nb_appeared 	+= r.nb_appeared;
			r_avg.sum_delay 	+= r.sum_delay;

			// cout << "#### Scenario " << i << " (" << r.nb_serviced << " requests) has total delay of " << r.sum_delay << endl;

			sum_avg_delay		+= r.avg_delay;
			n_nonempty_scnenarios += (r.nb_serviced > 0);

			s.reSampleScenario(currentTime);
		}
		r_avg.avg_serviced	= (double) r_avg.nb_serviced / numberScenarios;
		r_avg.avg_rejected	= (double) r_avg.nb_rejected / numberScenarios;
		r_avg.avg_appeared	= (double) r_avg.nb_appeared / numberScenarios;
		
		r_avg.avg_delay		= r_avg.sum_delay / r_avg.nb_serviced;
		// r_avg.avg_delay		= sum_avg_delay / n_nonempty_scnenarios;
		
		return r_avg;
	}



	// string& toString_hProbas() {
	// 	ostringstream out; 
	// 	out.setf(std::ios::fixed);
	// 	out.precision(2);

	// 	const Solution& solution = *solution_hProbas;
	// 	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment(); 
		
	// 	for (int route=1; route <= solution.getNumberRoutes(); route++) {
	// 		out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

	// 		out << outputMisc::greenExpr(true) << "H:                                                                                                "; 
	// 		for (int t=1; t <= I.getHorizon(); t++) 
	// 			out << setw(3) << t << "   "; 
	// 		out << endl << "Req:" << endl << outputMisc::resetColor() ;

	// 		for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {

	// 			VRP_solution::SolutionElement w; solution.getSolutionElementAtPosition(&w, route, pos);
	// 			const VRP_vertex& w_vertex = solution.getVertexAtElement(w);
	// 			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 				auto& r = *it;
					
	// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
	// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
	// 				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) solution.t_min(r,w) << " tmax=" << setw(3) << (int) solution.t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
	// 				for (int t=1; t <= I.getHorizon(); t++) {
	// 					double h_t = 0.0;
						
	// 					// switch (strategy) {
	// 					// 	case R_INFTY:
	// 					// 	case R_CAPA:
	// 					// 		ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					// 		for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 					// 			h_t += hProbas_Q[r.id][t][q];
	// 					// 		}
	// 					// 		break;
	// 					// 	case R_CAPA_PLUS:
	// 					// 		for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 					// 			h_t += h_Qplus_w[r.id][t][q];
	// 					// 			for (auto& r_ : requests_ordered_per_waiting_locations[route][pos])	
	// 					// 				h_t += h_Qplus[r.id][t][q][r_.id];
	// 					// 		}
	// 					// 		break;
								
	// 					// 	default: _ASSERT_(false, "");
	// 					// }
	// 					// (void)strategy; // so it shuts up with unused warning
	// 					ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++) {
	// 						h_t += hProbas_Q[r.id][t][q];
	// 					}
						
	// 					out << " "; if (h_t < 0.995) out << " ";
	// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";
						
	// 				}
	// 				out << endl;
	// 			}
	// 		} 
	// 		out << endl;
	// 	}
	// 	static string str = ""; str = out.str();
	// 	return (str);
	// }

	// string& toString_h_v(const Solution& solution, int route, int request_id, int n) {
	// 	ostringstream out; 
	// 	out.setf(std::ios::fixed);
	// 	out.precision(2);

	// 	const vector< vector< vector<VRP_request>>> & requests_ordered_per_waiting_locations = solution.getRequestAssignment(); 
		
	
	// 	out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

	// 	out << outputMisc::greenExpr(true) << "H:                                                                               "; 
	// 	for (int t=1; t <= I.getHorizon(); t++) 
	// 		out << setw(3) << t << "   "; 
	// 	out << endl << "Req:" << endl << outputMisc::resetColor() ;

	// 	int n_ = 0;

	// 	for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {

	// 		VRP_solution::SolutionElement w; solution.getSolutionElementAtPosition(&w, route, pos);
	// 		const VRP_vertex& w_vertex = solution.getVertexAtElement(w);
	// 		auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 		for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 			auto& r = *it;
				
	// 			if (r.id == request_id) 
	// 				n_ = n;

	// 			if (n_ > 0) {
	// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
	// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
	// 				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) solution.t_min(r,w) << " tmax=" << setw(3) << (int) solution.t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
	// 				for (int t=1; t <= I.getHorizon(); t++) {
	// 					double h_t = 0.0;
						
	// 					ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++) 
	// 						h_t += hProbas_Q[r.id][t][q];	
						
	// 					out << " "; if (h_t < 0.995) out << " ";
	// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";	
	// 				}
	// 				out << endl;

	// 				double h_t_w = 0.0;
	// 				for (int t=1; t <= I.getHorizon(); t++) 
	// 					for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 						h_t_w += h_Qplus_w[r.id][t][q];
	// 				if (h_t_w > 0) {
	// 					out << outputMisc::magentaExpr(true);
	// 					out << "\tFrom waiting vertex: \t\t\t\t\t\t\t";
	// 					for (int t=1; t <= I.getHorizon(); t++) {
	// 						h_t_w = 0.0;
	// 						for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 							h_t_w += h_Qplus_w[r.id][t][q];
	// 						out << " "; if (h_t_w < 0.995) out << " ";
	// 						if (h_t_w > 0)
	// 							out << setfill('0') << setw(2) << (int)round(h_t_w * 100) << setfill(' ') << "  ";
	// 						else out << "    ";
	// 					}
	// 					out << outputMisc::resetColor() <<  endl;
	// 				}

	// 				for (int pos=1; pos <= solution.getRouteSize(route)-1; pos++) {
	// 					auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
	// 					for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
	// 						auto& r_ = *it;

	// 						double h_t_rid = 0.0;
	// 						for (int t=1; t <= I.getHorizon(); t++) 
	// 							for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 								h_t_rid += h_Qplus[r.id][t][q][r_.id];
	// 						if (h_t_rid > 0) {
	// 							out << outputMisc::magentaExpr(true);
	// 							out << "\tFrom request " << r_.id << ": \t\t\t\t\t\t\t";
	// 							for (int t=1; t <= I.getHorizon(); t++) {
	// 								h_t_rid = 0.0;
	// 								for (int q=0; q <= I.getVehicleCapacity(); q++)
	// 									h_t_rid += h_Qplus[r.id][t][q][r_.id];
	// 								out << " "; if (h_t_rid < 0.995) out << " ";
	// 								if (h_t_rid > 0)
	// 									out << setfill('0') << setw(2) << (int)round(h_t_rid * 100) << setfill(' ') << "  ";
	// 								else out << "    ";
	// 							}
	// 							out << outputMisc::resetColor() <<  endl;
	// 						}
	// 					}
	// 				}

	// 				n_--;
	// 			}
	// 		}
	// 	}
	// 	static string str = ""; str = out.str();
	// 	return (str);
	// }

};


extern Solution_DS_VRPTW* s_dsvrptw_copy;

class Scenario_SS_DS_VRPTW_CR {
private:
	const VRP_instance& I;
	VRP_type sol_type;

	vector<vector<const VRP_request *>> sampled_requests;	// the sampled requests that constitute the scenario: sampled_requests[c][t]= r <==> the request *r appears at time unit t (not time slot !!) at customer vertex c ; otherwise = NULL
	vector<VRP_instance::RequestAttributes> sampled_requests_sequence;
	int sampled_time = -42;
	bool fixed_scenario = false;	// true iff the scenario cannot be resampled (i.e. it comes from a scenario file)
	bool sampled(const VRP_request& r) const;				// based on sampled_requests, tells whether a particular request is revealed as present
	const VRP_request* sampled(int region, int time_unit) const;	// based on sampled_requests, tells whether a request occurs at particular customer region and time unit (if so returns a pointer to the request, otherwise NULL)

	SimulationResults 	evalSolution_R_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;
	SimulationResults 	evalSolution_R_CAPA_PLUS(const Solution_SS_VRPTW_CR& solution, bool accept_all, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;
	SimulationResults 	evalSolution_R_BASIC_INFTY_CAPA(const Solution_SS_VRPTW_CR& solution, bool capacity, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;

	SimulationResults 	evalSolution_R_SSVRP_R(const Solution_SS_VRPTW_CR& solution, bool relocate, bool debug, bool verbose, int scenarioHorizon = numeric_limits<int>::max()) const;

	SimulationResults 	evalSolution_R_GSA(const Solution_DS_VRPTW& solution, bool verbose, int scenarioHorizon = numeric_limits<int>::max());

	

	// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_volatile_SS = NULL;
	// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_volatile_DS = NULL;
	const VRP_vertex& 	getVertex_atTime(const Solution_SS_VRPTW_CR& solution, int route, double t) const;

public:

	// Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>* pool_SS = NULL, VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW_CR>* pool_DS = NULL);	// constructor ***
	Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, VRP_type vrp_type, int currentTime);	// constructor ***
	Scenario_SS_DS_VRPTW_CR(const VRP_instance& instance, json j_scenario, VRP_type vrp_type, int currentTime);	// constructor ***
	~Scenario_SS_DS_VRPTW_CR();	

	SimulationResults 	evalSolution(const Solution_SS_VRPTW_CR& solution, RecourseStrategy strategy, bool debug = false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max());
	SimulationResults 	evalSolution(const Solution_DS_VRPTW& solution, RecourseStrategy strategy, bool debug = false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max());

	// double 	avgDelay(const Solution_SS_VRPTW_CR& solution, RecourseStrategy strategy, bool debug = false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max());

	const vector<VRP_instance::RequestAttributes> & getRequestSequence() const { return sampled_requests_sequence; }

	void 	updateScenario(double currentTime);			// Updates the scenario wrt the realizations in the instance file and the current time
	void 	reSampleScenario(double currentTime);		// Re-sample the scenario (for instance, in case it is not valable anymore), according to current time

	std::string& toString(bool realVertexNumbers = false) const;
	std::string& toString_perRegion() const;
};



class Scenario_SS_VRP_CD {
private:
	const VRP_instance& I;
	VRP_type sol_type;

	int* sampled_demands;	// the sampled requests that constitute the scenario: sampled_requests[r]= q <==> customer at region r reveals a demand of q
	
	double 	evalSolution_Bertsimas_A(const Solution_SS_VRP_CD& solution, bool debug, bool verbose) const;
public:
	Scenario_SS_VRP_CD(const VRP_instance& instance, VRP_type vrp_type, int currentTime = -1);	// constructor ***
	~Scenario_SS_VRP_CD();	

	double 	evalSolution(const Solution_SS_VRP_CD& solution, RecourseStrategy strategy, bool debug = false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max());

	void 	reSampleScenario();		// Re-sample the scenario (for instance, in case it is not valable anymore), according to current time
	int 	getSampledDemandAtRegion(int r) const;

	std::string& toString(bool realVertexNumbers = false) const;
	std::string& toString_perRegion() const;
};








#endif



