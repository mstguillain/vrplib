/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/* VRP_instance +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	Everything that you need to represent a VRP (dertermistic,dynamic,stochastic) instance
	Classes:
		- VRP_instance : general data of the instance and container for vertices and/or requests
		- VRP_vertex : represents a vertex in a VRP (could also be the depot)
		- VRP_request : represents a request in a dynamic (stochastic) context, associated to a vertex

	An instance of type VRP_instance namely represents a problem instance. 
	It contains datastructures in order to provide a representation of each geographical region in a graph.
	For each of these regions, 4 VRP_vertex instance is created, each representing a type of vertex that 
	could be associated to that region: depot vertex, regular VRP vertex, waiting vertex (*) and finally
	customer vertex (**). 

	*	In dynamic context (and also in static and stochastic one), waiting vertices -aka. relocation vertices-
		can be inserted in a VRPTW solution in order to indicate the vehicle that it should wait at a given 
		location, for a given amount of time; waiting and relocation strategies are basic approaches for anticipiation.

	** 	In static and stochastic context, a customer vertex -aka. customer region- hosts a number of different 
		potential requests, usually one per time slot at which the request could potentially appear.

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#ifndef VRP_INSTANCE_H
#define	VRP_INSTANCE_H

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cmath>
#include "tools.h"
#include "json.hpp"

using json = nlohmann::json;
using namespace std;

class VRP_request;
class VRP_vertex;
class VRP_instance;



/* VRP_vehicle    ------------------------------------------------------------------ */
class VRP_VehicleType {
public:
	VRP_VehicleType(int nRegions, int H, string name = string(), int capacity = 0, double velocity = 1.0, double costPerDistanceUnit = 1.0);
	bool operator == (const VRP_VehicleType& other) const;
	bool operator != (const VRP_VehicleType& other) const { return ! operator==(other); }
	// ~VRP_VehicleType();

	/* accessors */
	string 	getName() const {return name;}
	int 	getCapacity() const {return capacity;}
	double 	getVelocity() const {return velocity;}
	double 	getCostPerDistUnit() const {return costPerDistanceUnit;}

	/* distances */
	double 	distance(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time =-1) const;			// returns the distance between two vertices
	double 	distance(int region1, int region2, double curr_time =-1) const;								// idem between regions
	double 	travelTime(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time =-1, bool force_nonInteger = false) const;			// returns the travel time between two vertices, based on the distance and the vehicle velocity; curr_time is the time at which the trip is started (for time-dependent travel times)
	double 	travelTime(int region1, int region2, double curr_time =-1, bool force_nonInteger = false) const;							// idem between regions
	double 	departureTime(const VRP_vertex& v1, const VRP_vertex& v2, double arrival_time =-1) const;	// time at which one needs to leave v1, in order to reach v2 at time arrival_time
	double 	departureTime(int region1, int region2, double arrival_time =-1) const;
	double 	travelCost(const VRP_vertex& v1, const VRP_vertex& v2, double curr_time =-1) const;			// returns the travel cost between two vertices, based on the distance and the cost per distance unit
	double 	travelCost(int region1, int region2, double curr_time =-1) const;							// idem between regions

	/* other */
	double 	totalServiceTime(const VRP_request& r) const;						// returns the TOTAL service time required for some request (TOTAL = service time required at the vertex [specific to the vehicle] + additional time needed for that request [specific to the request])
	double 	totalServiceTime(const VRP_vertex& v) const;						
	double 	serviceTime(const VRP_vertex& v) const;								// returns the service time (in time units) at some vertex
	double 	serviceTime(int region) const;										// idem with region number

	/* comm */
	string& toString(bool verbose=false) const;
	string& toString_Distances() const;
	string& toString_TravelTimes() const;
	string& toString_TravelCosts() const;
	void 	print_TD_bins() const;

	friend ostream& operator<<(ostream& os, const VRP_VehicleType& veht); 

protected:
	friend class VRP_instance;
	friend class VRP_instance_file;
	
	string name;
	int capacity; 
	double velocity;
	double costPerDistanceUnit;

	int nRegions, H;
	vector< vector<double> > 			dist;			// stores the  distance matrix (in km)						==> initialized at 0.0 for each couple
	vector< vector<double> > 			travel_times;	// stores the  travel times matrix (in seconds)		==> initialized at 0.0 for each couple
	vector< vector<vector<double>>> 	dist_TD;		// time-dependent
	vector< vector<vector<double>>> 	travel_times_TD;// time-dependent
	vector< int > 						TD_curr_bin;	// time-dependent
	vector< int > 						TD_next_bin;	// time-dependent
	vector< double > 					service_times;	// stores the  service times 						==> initialized at 0.0 for each vertex
		
	bool integer_travel_times 			= false;
	bool travel_times_NE_distances 		= false;		// true iff travel_time(i,j) ≠ dist(i,j)*velocity
	bool time_dependent_travel_times	= false;

	void 	setServiceTime(int region, double t);							// set the service time required at some region
	void 	setTravelDistance(int region1, int region2, double d, int fromTU = -1, int toTU = -1);			// set the travel DISTANCE between two vertices
	void 	setTravelTime(int region1, int region2, double t, int fromTU = -1, int toTU = -1);				// set the travel TIME and put travel_times_NE_distances to TRUE

	void 	preComputeEuclideanDistances(const vector<double>& coordX, const vector<double>& coordY, bool setTravelTimes = false);		// compute the Euclidean distance matrix dist (typically called after parsed instance file)
	void 	computeAverageDistances();

private:
	double 	distance_TD(int region1, int region2, double curr_time) const;
	// double 	travelTime_TD(int region1, int region2, double curr_time) const;
	double 	tt_TD(int region1, int region2, double start_time, double remain_dist) const;
	double 	departureTime_TD(int region1, int region2, double arr_time) const;
	double 	dep_TD(int region1, int region2, double arrival_time, double remain_dist) const;
	
};



/* VRP_request    ------------------------------------------------------------------ */

class VRP_request {
protected:
	const VRP_vertex* vertex;	// vertex hosting the request
	double* demand_prob = nullptr;	// demand_prob[q] is the probability that the request has a demand load of q
	friend class VRP_instance;

public:
	static bool (*request_comparison_lt)	(const VRP_request &, const VRP_request &);

	friend class VRP_instance;
	friend class VRP_vertex;

	int 	id_instance_file = -1;	// id as told in the instance file
	int 	id = -1;				// id for internal use

	double 	e = -1;    				// beginning of request's time window 	
	double 	l = numeric_limits<int>::max();    	// end of request's time window 		
	double 	add_service_time = 0;  	// additional service time for the request (in addition to the service time at the vertex (which is a VehicleType attribute))
	double 	demand = 0; 			// deterministic demand of request 	

	int 	revealTS;				// reveal time slot of potential request 	

	double 	revealTime = -1;		// reveal time of potential request  [!! used only in DS/SS-VRPTW-CR]	(time, not time slot !)	===> used when there is a precise reveal time on the potential request 
	double 	revealTime_min = -1; 	// used when the request's reveal time is comprised in an interval (e.g. the potential request can appear between t=161 and t'=320)
	double  revealTime_max = -1;	// ^^^

	double 	p = 0;					// probability of presence of the potential request (in %)
	bool 	appeared = true;		// false if it is a sampled request; can only be false for type REGULAR_ONLINE

	const VRP_vertex& getVertex() const { ASSERT(vertex != NULL, ""); return *vertex; }
	int getID() const { return id;}
	int getInstanceID() const { return id_instance_file;}
	int getRevealTS() const { return revealTS;}
	double getDiscreteDemandProb(int q) const;

	VRP_request(const VRP_vertex* v = NULL);
	void setAppeared();
	bool hasAppeared() const;
	void setRevealTime(double rt);
	int getRevealTimeSS(double curr_time) const;

	bool operator==(const VRP_request& other) const;
	bool operator!=(const VRP_request& other) const { return !operator==(other);}
	bool operator<(const VRP_request& other) const;		// "less than" comparison operator; used for ordering the requests; must be user defined ! 
														// if req_a < req_b, then req_a will be before req_b in the request assignment and ordering
	bool operator>=(const VRP_request& other) const { return ! operator<(other);}
	/* debug */
	bool checkConsistency() const;

	/* comm */
	string& toString(bool verbose, bool instance_numbering) const;
	string& toString(bool verbose = false) const;
	string& toStringRealVertexNumbers(bool verbose = false) const;
	
	friend ostream& operator<<(ostream& os, const VRP_request& r); 
};


/* VRP_vertex    ------------------------------------------------------------------ */

class VRP_vertex {

public:

	enum VertexType {	// types of vertices
		DEPOT		,		// a vertex associated to the depot; usually does not host any request (there is one, but initialized with null values)
		REGULAR		,		// a vertex associated to a regular customer; used in the classical VRP(TW); a regular vertex hosts exactly one request
		RANDOM_CUSTOMER,	// a vertex associated to a customer region with random customers and possibly random reveal times; used in SS-VRP-C and SS-VRPTW-CR; a customer location hosts a list of potential requests
		WAITING		,		// a vertex associated to a waiting location; used in DS-VRPs and SS-VRPs; usually a waiting vertex does not normally host any request (there is one, but initialized with null values)
		REGULAR_ONLINE, 	// a vertex associated to a customer potential request; usually used in DS-VRPs in order to allow multiple requests in the at the same region
		RANDOM_DEMAND,		// a vertex associated to a customer region with random demands; used in SS-VRP-D

		// SCHED_JOB,			// a vertex associated to a job in a scheduling problem
		NO_TYPE		
	};
		

	VRP_vertex(int region, VertexType type, int nTimeSlots, int instance_id=-1, int max_demand = -1);
	bool operator == (const VRP_vertex& v) const;
	bool operator != (const VRP_vertex& v) const { return ! operator==(v); }
	// ~VRP_vertex();

	/* accessors */
	VertexType 			getType() const { return type; }
	int 				getIdInstance() const { return id_instance_file; }
	const double*		getCoordinates() const { return coordinates; }
	int 				getRegion() const { return region; }
	const VRP_request&	getRequest(int timeSlot) const;
	const VRP_request&	getRequest() const;
	int 				getMaxDemand() const { return max_demand; }

	/* debug */
	bool 	checkConsistency() const;

	/* comm */
	string& toString(bool verbose, bool instance_numbering) const;
	string& toString(bool verbose = false) const;
	string& toStringType() const;
	string& toStringRealVertexNumbers(bool verbose = false) const;
	friend ostream& operator<<(ostream& os, const VRP_vertex& v);  


protected:
	friend class VRP_instance;
	friend class VRP_instance_file;
	friend class SCHED_instance_file;
	friend class VRP_request;
	// bool active;
	int region;				// region to which the vertex is associated, in the graph representing the problem instance
	int id_instance_file;	// identification number in the test instance file 
	double coordinates[2];
	int nTimeSlots;			// # of timeslots in the instance
	int max_demand;			// max demand load that a customer may ask on that vertex, according to (potential) requests

	VertexType type;		// type of the vertex

	/* Requests */
	VRP_request* requests_ts;	// one request per time slot (including those with zero probability, and timeslot 0)		
	void setRegion(int reg) { region = reg; }
	void setType(VertexType typ) { type = typ; }

	VRP_request&	getRequest_mutable(int timeSlot);
	VRP_request&	getRequest_mutable();

};













/* VRP_instance    ------------------------------------------------------------------ */
#define MAX_ID	100000

class VRP_instance {
public:
	struct RequestAttributes {
		const VRP_vertex* vertex = nullptr;
		const VRP_request* request = nullptr;
		int reveal_time = -1;
		int time_slot = -1;
		string& toString(bool verbose=false) const;
	};
protected:
	friend class VRP_instance_file;

	/* Vertices */
	int nRegions;	// = total number of different regions (including the one associated with the depot) in the graph representing the problem instance; 
	int nVehicles = -1;	// if stated in the instances files
	int id_instance_file_to_region[MAX_ID];
	int region_to_id_instance_file[MAX_ID];
	int nbAddedVerticesAtDifferentRegions = 0;	// keep track of the number of regions for which a vertex has been added, so far

	set<VRP_vertex*>	depot_vertices;		// set of vertices associated to a depot vertex, according to the instance
	set<VRP_vertex*>	reg_vertices;		// set of vertices associated to a regular vertex, according to the instance
	set<VRP_vertex*>	random_cust_vertices;		// set of vertices associated to a customer with random presence and reveal times (SS-VRP only), according to the instance; each vertex host a set of potential requests
	set<VRP_vertex*>	random_demand_vertices;		// set of vertices associated to a customer with random demand region (SS-VRP only), according to the instance;
	set<VRP_vertex*>	wait_vertices;		// set of vertices associated to a waiting vertex, according to the instance
	set<VRP_vertex*> 	reg_online_vertices;// set of subsets: reg_online_vertices[ts] = set of vertices associated to a customer regions (in DS-VRP) at time slot ts, according to the instance; each vertex hosts one potential request
	vector<double> 		coordX;	// stores the coordinates of each region
	vector<double> 		coordY;			// they must coincide with the coordinates of the vertices hosted in each region

	bool 				no_TW_horizon = false;


	VRP_vertex&			addVertex(int region, VRP_vertex::VertexType type, int instance_id = -42, int max_demand = -1);	// DEPRECATED add a new vertex in the instance and returns a reference to it (typically called when parsing instance file); 
	VRP_vertex&			addVertex(VRP_vertex::VertexType type, int instance_id = -42, int max_demand = -1);				// same; but let the region number (internal representation, not the one given in the instance file) be assigned automatically
	void				addVertex(VRP_vertex * pvertex);																// add a vertex that has already been instanciated (used by SCHED_instance, when it creates a new job)
	// void				setCoordRegion(int region, double x, double y) { coordX[region] = x; coordY[region] = y; }					// sets the coordinates of a particular region of the graph
	// void				setCoordRegion(int region, int x, int y) { setCoordRegion(region, (double) x, (double) y); }
	void				setCoordinatesVertex(VRP_vertex& v, double lat, double lon);
	VRP_vertex* 		getVertexFromRegion_mutable(VRP_vertex::VertexType type, int region) const; 	// returns a MUTABLE reference pointer to a particular type of vertex, for a given region (internal region id, not the one from the instance)
	VRP_vertex* 		getVertexFromInstanceRegion_mutable(VRP_vertex::VertexType type, int realRegion) const; 	// idem, but using the region ID from the instance file


	/* Time */
	int 	H;     			// time horizon (= number of time units) [0,1,...,H], maximum vehicle capacity amongst all vehicle types, according to the instance
	int 	nTimeSlots;		// number of time slots (≤H) in the horizon (one request per time slot for each customer region); timeslot 0 corresponds to deterministic requests 	
	double 	scale;			// scale of the horizon size (if original -i.e. from instance file- horizon size is 480, and here H is 240, then scale is 2)
	int 	orig_horizon;


	/* Vehicles */
	int 					_nVeh = -1;    						// number of vehicles (this shouldn't be part of the instance, deprecated)
	set<VRP_VehicleType *> 	vehicle_types;
	VRP_VehicleType& 		addVehicleType(string name = string(), int capacity = 0, double velocity = 1.0, double costPerDistanceUnit = 1.0);	// adds new vehicle type to the set vehicle_types
	void 			 		addVehicleType(VRP_VehicleType* veh_type);	// add a vehicle type that has already been instanciated (used by SCHED_instance, when it creates a new "worker")
	
	/* Customer potential requests (dynamic/stochastic context), if any */
	int 						nPotentialRequests = 0;
	vector<RequestAttributes> 	online_requests_appeared;			// vector containing the sequence of appeared requests, according to the instance; ordered according to the instance (primarily by reveal time, obviously)
	vector<vector<int>> 		online_requests_appearing_times;	// appearing_times[region][ts] == t iff an online request (at region for time slot ts) appears at to time t (according the the real scenario revealed by the instance)
	int 						max_random_demand = -1;				// maximum demand load that a customer may ask (in SS-VRP-D, or SS-VRP-CD, only)

	VRP_vertex&					addOnlineVertex(int region, VRP_vertex::VertexType type, int time_slot, int min_reveal_time, int max_reveal_time, int instance_id = -42);	// for online requests, associates a vertex to a region and one time slot; min and max reveal times define should bound the time slot in the horizon
	void 						addAppearingOnlineRequest(int region, int timeSlot, int revealTime);			// adds an entry to vector online_requests_appeared
	void 						setDiscreteDemandProb(int region, int demand, double p);				// set the demand probability of a customer vertex (of type VRP_vertex::RANDOM_DEMAND, in SS-VRP-C/D only)
	
	bool 			integer_travel_times 		= false;
	bool 			time_dependent_travel_times = false;
public:
	VRP_instance(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double precision=1);		// 

	/* Time */
	int 			getHorizon() const;
	int 			getNumberTimeSlots() const;				// DS/SS VRPTWs only
	double 			getScale() const { return scale;}		// SS-VRPTW-CR only (for the moment, should be extended to DS-VRPTW)


	void 			setNoHorizonTW() { no_TW_horizon = true; }
	bool 			isHorizonTW() const { return no_TW_horizon; }
	
	/* Graph */
	int 						getNumberRegions() const;
	int 						getNumberVehicles() const { return nVehicles; }
	const set<VRP_vertex *>& 	getDepotVertices() const { return depot_vertices; }
	const set<VRP_vertex *>& 	getRegularVertices() const { return reg_vertices; }
	const set<VRP_vertex *>& 	getRandomCustomerVertices() const {return random_cust_vertices; }
	const set<VRP_vertex *>&	getRandomDemandVertices() const {return random_demand_vertices; }
	const set<VRP_vertex *>&	getWaitingVertices() const { return wait_vertices; }
	const set<VRP_vertex *>& 	getOnlineVertices() const { return reg_online_vertices; }
	int 					 	getNumberVertices(VRP_vertex::VertexType type) const;	// returns the number of vertices associated to a particular type 
	const VRP_vertex* 			getVertexFromRegion(VRP_vertex::VertexType type, int region) const; 	// returns a reference pointer to a particular type of vertex, for a given region
	const VRP_vertex* 			getVertexFromInstanceFileID(VRP_vertex::VertexType type, int instance_id) const; 	// returns a reference pointer to a particular type of vertex, based on the ID specified in the instance file (VRP_vertex::id_instance_file)
	int 						instanceRegionIDtoRegion(int instance_region) const { return id_instance_file_to_region[instance_region];}
	string& 					toStringCoords(bool and_attributes=false) const;

	/* Vehicles */
	const set<VRP_VehicleType *>& 	getVehicleTypes() const { return vehicle_types; }	// returns the set of vehicle types
	const VRP_VehicleType& 			getVehicleType(string name) const;					// returns the vehicle type having the corresponding name
	const VRP_VehicleType& 			getVehicleType() const;								// returns the unique vehicle type (crashes is more than one type recorded)
	int 							getMaxVehicleTypeCapacity() const;
	void 							setIntegerTravelTimes();				// no matter the vehicle type, the travelTime() function will return integer times (rounded up)
	void 							setTimeDependent();						// no matter the vehicle type, the travelTime() and distance() functions will be time dependent
	bool 							isIntegerTravelTimes() const { return integer_travel_times; }
	bool 							isTimeDependent() const { return time_dependent_travel_times; }
	int 							_getNumberVehicles() const; 			// deprecated; the number of vehicles shouldn't be part of the instance, but instead a parameter of the solving method
	string& 						toStringVehicleTypes(bool verbose=false) const;

	/* Dynamic context */
	/* the term "appeared" means that a potential request has revealed	to be appearing in the real scenario; whether a request is appeared may vary along the online execution, since it depends on the moments at which stochastic events occur */
	/* the term "revealed" means that the stochastic information about a potential request (that is, if it appears or not) is know; again, that property depends on the time at which the question is asked */
	// void 								clearAppearedOnlineRequests();		// not working, apparently I miss something in addition to setting appeared=false to every online requests
	void								set_OnlineRequest_asAppeared(int region, int timeSlot, int time_unit = -42);	// tells the instance that, from now, the potential request must be considered as appeared (in the real scenario)
	void								set_OnlineRequest_asAppeared(const VRP_request& r, int time_unit = -42);						//
	bool  								revealedAtTime(const VRP_request& r, int time) const; 					// tells whether, for a given potential request r, and according to the real scenario that realized until current time, the request is revealed or not (if the request associated time slot is past, then returns always true; if r's TS is not started yet, returns always false; otherwise, returns true iff r appeared during the part of the TS until current time)
	const vector<RequestAttributes> & 	getAppearingOnlineRequests() const { return online_requests_appeared; }	// for DS-VRPTW simulation only ! returns the sequence of requests that actually appear
	string& 							toStringPotentialOnlineRequests() const;		// DS-VRPTW
	string& 							toString_AppearingOnlineRequests(bool verbose=false) const;
	double 								getMaxReqProb() const;

	/* Stochastic context */
	int 		getMaxRandomDemand() const;					// SS-VRP-C/D only, returns maximum demand load that a customer may ask (in SS-VRP-D, or SS-VRP-CD, only)
	int 		getNumberPotentialRequests() const; 		// SS-VRPTW-CR only (for the moment), returns the number of potential requests having positive probability to appear
	double 		getExpectedNumber_appearedRequests(double fromCurrentTime = -1) const;	// SS-VRPTW-CR only (for the moment)
	string& 	toStringDiscreteDemandsProbas() const;		// SS-VRP-D
	string& 	toStringProbasTS(bool instance_ids = false) const;	// SS-VRPTW-CR 	(if instance_ids = true, then print real instance vertex ids instead of internal ids (from 1 to ..., with depot 0))
	string& 	toStringPotentialRequests(bool instance_ids = false) const;			// SS-VRPTW-CR
	string& 	toStringInstanceMetrics() const;			// SS-VRPTW-CR
	// void 		updateRevealTimes_potentialRequests(double curr_time);

	/* debug */
	bool 	checkConsistency() const;	// performs some basic ckecks related to the data stored (e.g. are all requests such that demand ≤ Q ?)

	/* misc */
	string& toString(bool verbose=false) const;
	string& toString_Distances(bool real_ids = false) const;	// print values for each vehicle type
	string& toString_TravelTimes(bool real_ids = false, double curr_time =-1) const;
	string& toString_TravelCosts(bool real_ids = false) const;

	void 	print_TD_bins() const;
};






class VRP_instance_file {
/* INSTANCE FILES ------------------------------------------------------------------ 

The following functions read a given type of instance file and return a reference to an object VRP_instance, filled with the instance data */
public:

	/****** VRPTW ******/
	// Handles instance files of Cordeau-Laporte in benchmarks/cordeau/vrptw/old 	-	static instances 
	static VRP_instance& readInstanceFile_CordeauLaporte_VRPTW_old(const char* filename);


	/****** SS/DS-VRPTW ******/
	/* Instance files used for the DS-VRPTW, from Bent & Van Hentenryck */
	static VRP_instance& readInstanceFile_DS_VRPTW__Bent_PVH(const char *filename);
	/* Instance files used for the DS-VRPTW, from Rizzo  */
	static VRP_instance& readInstanceFile_DS_VRPTW__Rizzo(const char *oc_directory, int scenario_number, bool use_lockers);

	// SS-VRPTW-CR instance files in benchmarks/ss-vrptw-cr/paper_conf - given a scale ≥ 1 (e.g. 2 -> means 1 time unit of horizon represents 2 instance real time units), all the temporal data are scaled appropriately
	static VRP_instance& readInstanceFile_SSVRPTW_CR(const char *filename, double scale = 1.0);

	/* Instance files used for the DS-VRPTW, from Candaele */
	// static VRP_instance& readInstanceFile_DS_VRPTW__Candaele(const char *carrier_filename, const char *graph_filename, const char *customers_filename, const char *scenario_filename);
	static VRP_instance& readInstanceFile_DS_VRPTW__Candaele(string carrier_filename, string graph_filename, string customers_filename, string scenario_filename, int vehicle_capacity = -42);
	static VRP_instance& readInstanceFile_DS_VRPTW__Candaele(string carrier_filename, string graph_filename, string customers_filename, int vehicle_capacity = -42);
	// Instance files used for the SS-VRPTW-CR in benchmarks/ss-vrptw-cr/realdata_optimod_lyon (journal paper, part 1: models)
	// static VRP_instance& readInstanceFile_SSVRPTW_CR_journal_part_1(const char *filename_distances, const char *filename_instance, double scale = 1.0);
	static VRP_instance& readInstanceFile_SSVRPTW_CR__Candaele(string carrier_filename, string graph_filename, string customers_filename, int vehicle_capacity = -42, double scale = 1.0, string type = "SS");
	static VRP_instance& readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(string carrier_filename, string graph_filename, string customers_filename, string scenario_filename, int vehicle_capacity = -42, double scale = 1.0);
	static VRP_instance& readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(const char *filename_distances, const char *filename_instance, int vehicle_capacity, double scale = 1.0);


	/****** SS-VRP-D ******/
	// SS-VRP-D instance files in benchmarks/ss-vrp-d/discrete distrib/local 
	static VRP_instance& readInstanceFile_SSVRP_D__1(const char *filename, int vehicle_capacity);

private:
	static VRP_instance& readInstanceFile_DS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, json j_scenario, int vehicle_capacity);
	static VRP_instance& readInstanceFile_DS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity);
	
	static VRP_instance& readInstanceFile_Hybrid_DS_VRPTW_CR__Candaele(json j_carrier, json j_graph, json j_customers, json j_scenario, int vehicle_capacity, double scale);
	static VRP_instance& readInstanceFile_SSVRPTW_CR__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity, double scale, string type);
	
	/* Core implementation of CANDAELE parser for SS/DS VRPTW json files*/
	static VRP_instance& readInstanceFile_SSDS_VRPTW__Candaele(json j_carrier, json j_graph, json j_customers, int vehicle_capacity, string type, VRP_vertex::VertexType requestVertexType, double scale = 1.0);
	
};
	

#endif

